<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        if (0 === strpos($pathinfo, '/secure')) {
            if (0 === strpos($pathinfo, '/secure/project')) {
                // flow_add
                if (preg_match('#^/secure/project/(?P<projectId>[^/]++)/flow/add$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'flow_add')), array (  '_controller' => 'App\\Controller\\Admin\\FlowController::add',));
                }

                // flow_edit
                if (preg_match('#^/secure/project/(?P<projectId>[^/]++)/flow/(?P<flowId>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'flow_edit')), array (  '_controller' => 'App\\Controller\\Admin\\FlowController::edit',));
                }

            }

            // dashboard
            if ('/secure' === $trimmedPathinfo) {
                $ret = array (  '_controller' => 'App\\Controller\\Admin\\IndexController::dashboard',  '_route' => 'dashboard',);
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif ('GET' !== $canonicalMethod) {
                    goto not_dashboard;
                } else {
                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'dashboard'));
                }

                return $ret;
            }
            not_dashboard:

            if (0 === strpos($pathinfo, '/secure/project')) {
                // projects_list
                if ('/secure/projects' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\Admin\\ProjectController::list',  '_route' => 'projects_list',);
                }

                // project_add
                if ('/secure/project/add' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\Admin\\ProjectController::add',  '_route' => 'project_add',);
                }

                // project_edit
                if (preg_match('#^/secure/project/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'project_edit')), array (  '_controller' => 'App\\Controller\\Admin\\ProjectController::edit',));
                }

            }

        }

        // user_register
        if ('/signup' === $pathinfo) {
            return array (  '_controller' => 'App\\Controller\\UserController::register',  '_route' => 'user_register',);
        }

        // blog
        if ('/blog' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'App\\Controller\\BlogController::index',  '_route' => 'blog',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_blog;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'blog'));
            }

            return $ret;
        }
        not_blog:

        // contact_us
        if ('/contact_us' === $pathinfo) {
            return array (  '_controller' => 'App\\Controller\\ContactController::contactUs',  '_route' => 'contact_us',);
        }

        // user_confirm
        if (0 === strpos($pathinfo, '/confirm-registration') && preg_match('#^/confirm\\-registration/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_confirm')), array (  '_controller' => 'App\\Controller\\UserController::confirmRegistration',));
        }

        // login
        if ('/login' === $pathinfo) {
            return array (  '_controller' => 'App\\Controller\\SecurityController::login',  '_route' => 'login',);
        }

        // logout
        if ('/logout' === $pathinfo) {
            return array('_route' => 'logout');
        }

        if (0 === strpos($pathinfo, '/re')) {
            // remind_password
            if ('/remind-password' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\SecurityController::remindPassword',  '_route' => 'remind_password',);
            }

            // refund_policy
            if ('/refund-policy.html' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\TermsController::refundPolicy',  '_route' => 'refund_policy',);
            }

            // user_registration_success
            if ('/registered' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\UserController::registrationSuccess',  '_route' => 'user_registration_success',);
            }

        }

        // index
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'App\\Controller\\SiteController::index',  '_route' => 'index',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_index;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'index'));
            }

            return $ret;
        }
        not_index:

        // app_site_index
        if ('/index.html' === $pathinfo) {
            return array (  '_controller' => 'App\\Controller\\SiteController::index',  '_route' => 'app_site_index',);
        }

        // pricing
        if ('/pricing-and-plans.html' === $pathinfo) {
            return array (  '_controller' => 'App\\Controller\\SiteController::pricing',  '_route' => 'pricing',);
        }

        // privacy_policy
        if ('/privacy-policy.html' === $pathinfo) {
            return array (  '_controller' => 'App\\Controller\\TermsController::privacyPolicy',  '_route' => 'privacy_policy',);
        }

        // free_trial
        if ('/free-trial.html' === $pathinfo) {
            return array (  '_controller' => 'App\\Controller\\SiteController::freeTrial',  '_route' => 'free_trial',);
        }

        // geo_targeting
        if ('/geo-targeting.html' === $pathinfo) {
            return array (  '_controller' => 'App\\Controller\\SiteController::geoTargeting',  '_route' => 'geo_targeting',);
        }

        // how_it_works
        if ('/how-it-works.html' === $pathinfo) {
            return array (  '_controller' => 'App\\Controller\\SiteController::howItWorks',  '_route' => 'how_it_works',);
        }

        // traffic
        if ('/traffic.html' === $pathinfo) {
            return array (  '_controller' => 'App\\Controller\\SiteController::traffic',  '_route' => 'traffic',);
        }

        // terms_and_condititons
        if ('/terms-and-conditions.html' === $pathinfo) {
            return array (  '_controller' => 'App\\Controller\\TermsController::termsAndConditions',  '_route' => 'terms_and_condititons',);
        }

        // unsubscribe
        if (0 === strpos($pathinfo, '/unsubscribe') && preg_match('#^/unsubscribe/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'unsubscribe')), array (  '_controller' => 'App\\Controller\\SubscriptionController::unsubscribe',));
        }

        if (0 === strpos($pathinfo, '/_')) {
            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not__profiler_home;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', '_profiler_home'));
                    }

                    return $ret;
                }
                not__profiler_home:

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ('/_profiler/open' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

        }

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
