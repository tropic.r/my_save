<?php

/* admin/project/list.html.twig */
class __TwigTemplate_4677c87a70dcee79a16943944b66e6e29bef63645c612dc45106375653e7e82a extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("admin/layouts/admin_left_menu.html.twig", "admin/project/list.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layouts/admin_left_menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/project/list.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/project/list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "
    <div class=\"ibox\">
        <div class=\"ibox-title\">
            <h5>All projects assigned to this account</h5>
            <div class=\"ibox-tools\">
                <a href=\"";
        // line 9
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("project_add");
        echo "\" class=\"btn btn-primary btn-xs\">Create new project</a>
            </div>
        </div>
        <div class=\"ibox-content\">
            <div class=\"row m-b-sm m-t-sm\">
                <div class=\"col-md-12\">
                    <div class=\"input-group\">
                        <input type=\"text\" placeholder=\"Search\" class=\"input-sm form-control\">
                        <span class=\"input-group-btn\">
                              <button type=\"button\" class=\"btn btn-sm btn-primary\"> Go!</button>
                        </span>
                    </div>
                </div>
            </div>

            <div class=\"project-list\">

                <table class=\"table table-hover\">
                    <tbody>

                    ";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["projects"]) || array_key_exists("projects", $context) ? $context["projects"] : (function () { throw new Twig_Error_Runtime('Variable "projects" does not exist.', 29, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["project"]) {
            // line 30
            echo "
                        <tr>
                            <td class=\"project-status\">
                                ";
            // line 33
            if ((twig_get_attribute($this->env, $this->source, $context["project"], "state", array()) == "active")) {
                // line 34
                echo "                                    <span class=\"btn-primary btn-sm\">Active</span>
                                ";
            } else {
                // line 36
                echo "                                    <span class=\"btn-white btn-sm\">Unactive</span>
                                ";
            }
            // line 38
            echo "                            </td>
                            <td class=\"project-title\">
                                <a href=\"";
            // line 40
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("project_edit", array("id" => twig_get_attribute($this->env, $this->source, $context["project"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["project"], "name", array()), "html", null, true);
            echo "</a>
                            </td>
                            <td class=\"project-actions\">
                                <a href=\"";
            // line 43
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("project_edit", array("id" => twig_get_attribute($this->env, $this->source, $context["project"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-white btn-sm\"><i class=\"fa fa-pencil\"></i> Edit </a>
                            </td>
                        </tr>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['project'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "
                    </tbody>
                </table>
            </div>
        </div>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/project/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 48,  114 => 43,  106 => 40,  102 => 38,  98 => 36,  94 => 34,  92 => 33,  87 => 30,  83 => 29,  60 => 9,  53 => 4,  44 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'admin/layouts/admin_left_menu.html.twig' %}

{% block content %}

    <div class=\"ibox\">
        <div class=\"ibox-title\">
            <h5>All projects assigned to this account</h5>
            <div class=\"ibox-tools\">
                <a href=\"{{ path('project_add') }}\" class=\"btn btn-primary btn-xs\">Create new project</a>
            </div>
        </div>
        <div class=\"ibox-content\">
            <div class=\"row m-b-sm m-t-sm\">
                <div class=\"col-md-12\">
                    <div class=\"input-group\">
                        <input type=\"text\" placeholder=\"Search\" class=\"input-sm form-control\">
                        <span class=\"input-group-btn\">
                              <button type=\"button\" class=\"btn btn-sm btn-primary\"> Go!</button>
                        </span>
                    </div>
                </div>
            </div>

            <div class=\"project-list\">

                <table class=\"table table-hover\">
                    <tbody>

                    {% for project in projects %}

                        <tr>
                            <td class=\"project-status\">
                                {% if project.state == \"active\" %}
                                    <span class=\"btn-primary btn-sm\">Active</span>
                                {% else %}
                                    <span class=\"btn-white btn-sm\">Unactive</span>
                                {% endif %}
                            </td>
                            <td class=\"project-title\">
                                <a href=\"{{ path('project_edit', { 'id': project.id }) }}\">{{ project.name }}</a>
                            </td>
                            <td class=\"project-actions\">
                                <a href=\"{{ path('project_edit', { 'id': project.id }) }}\" class=\"btn btn-white btn-sm\"><i class=\"fa fa-pencil\"></i> Edit </a>
                            </td>
                        </tr>

                    {% endfor %}

                    </tbody>
                </table>
            </div>
        </div>
    </div>

{% endblock %}", "admin/project/list.html.twig", "/var/www/trafficbot.loc/templates/admin/project/list.html.twig");
    }
}
