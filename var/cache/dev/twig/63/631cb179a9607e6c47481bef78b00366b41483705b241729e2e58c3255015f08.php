<?php

/* site/layouts/main.html.twig */
class __TwigTemplate_e7cc5dd9cdef17dd2372a4fec27e256550f85d6abb649eb030c088b7b90c8aad extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("site/layouts/base.html.twig", "site/layouts/main.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'header' => array($this, 'block_header'),
            'header_logo' => array($this, 'block_header_logo'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "site/layouts/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "site/layouts/main.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "site/layouts/main.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        $this->displayBlock('header', $context, $blocks);
        // line 74
        echo "
    ";
        // line 75
        $this->displayBlock('content', $context, $blocks);
        // line 76
        echo "
    ";
        // line 77
        $this->displayBlock('footer', $context, $blocks);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_header($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 5
        echo "        <header class=\"header";
        if (array_key_exists("header_class", $context)) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["header_class"]) || array_key_exists("header_class", $context) ? $context["header_class"] : (function () { throw new Twig_Error_Runtime('Variable "header_class" does not exist.', 5, $this->source); })()), "html", null, true);
            echo "\"";
        }
        echo "\">
        <div class=\"container\">
            ";
        // line 7
        $this->displayBlock('header_logo', $context, $blocks);
        // line 10
        echo "            <nav class=\"primary-menu float-right\">
                <ul class=\"nav\">
                    <li>
                        <a href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pricing");
        echo "\">Pricing</a>
                    </li>
                    <li>
                        <a href=\"";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("free_trial");
        echo "\">Free Trial</a>
                    </li>
                    <li>
                        <a href=\"#contacts\">Contacts</a>
                    </li>
                    <li>
                        <a href=\"";
        // line 22
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("blog");
        echo "\">Blog</a>
                    </li>
                    <li>
                        <a href=\"#faq\">FAQ</a>
                    </li>
                    <li>
                        <a href=\"#register\">Register</a>
                    </li>
                    <li class=\"nav-login-item\">
                        <a href=\"";
        // line 31
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("login");
        echo "\" class=\"button\">Login</a>
                    </li>
                </ul>
            </nav><!--nav.primary-menu-->
            <div class=\"clearfix\"></div>
            <div class=\"mobile-header-visible\">
                <div class=\"dropdown-toggle float-left menu-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\"
                     aria-expanded=\"false\">
                    <span class=\"menu-toggle-icon\"></span>
                </div>
                <div class=\"dropdown-menu mobile-menu-wrapper\" aria-labelledby=\"dropdownMenuButton\">
                    <div class=\"scrollable\">
                        <nav class=\"mobile-primary-menu\">
                            <ul>
                                <li>
                                    <a href=\"";
        // line 46
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pricing");
        echo "\">Pricing</a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 49
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("free_trial");
        echo "\">Free Trial</a>
                                </li>
                                <li>
                                    <a href=\"#contacts\">Contacts</a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 55
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("blog");
        echo "\">Blog</a>
                                </li>
                                <li>
                                    <a href=\"#faq\">FAQ</a>
                                </li>
                                <li>
                                    <a href=\"#register\">Register</a>
                                </li>
                            </ul>
                        </nav><!--nav.mobile-primary-menu-->
                    </div>
                </div><!--.mobile-menu-wrapper-->
                <div class=\"nav-login-item float-right\">
                    <a href=\"";
        // line 68
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("login");
        echo "\" class=\"button\">Login</a>
                </div><!--.nav-login-item-->
            </div><!--.mobile-header-visible-->
        </div>
        </header>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_header_logo($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_logo"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_logo"));

        // line 8
        echo "                <a class=\"logo white\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("index");
        echo "\"></a>
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 75
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 77
    public function block_footer($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 78
        echo "        <footer>
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col\">
                        <figure class=\"main-logo footer-logo\">
                            <a class=\"logo white\" href=\"";
        // line 83
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("index");
        echo "\"></a>
                        </figure><!--.main-logo-->
                        <div class=\"short-site-desc\">Where you can buy website traffic<br>at the best prices ever!</div>
                        <ul class=\"policy-links\">
                            <li><a href=\"";
        // line 87
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("terms_and_condititons");
        echo "\">Terms & Conditions</a></li>
                            <li><a href=\"";
        // line 88
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("privacy_policy");
        echo "\">Privacy Policy</a></li>
                            <li><a href=\"";
        // line 89
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("refund_policy");
        echo "\">Refund Policy</a></li>
                        </ul>
                    </div>
                    <div class=\"col text-right\">
                        <div class=\"footer-col-title\">Recently posted in Blog</div>
                        <ul class=\"blog-links\">
                            <li><a href=\"#\">Post 1</a></li>
                            <li><a href=\"#\">Post 2</a></li>
                            <li><a href=\"#\">Post 3</a></li>
                            <li><a href=\"#\">Post 4</a></li>
                            <li><a href=\"#\">Post 5</a></li>
                        </ul>
                    </div>
                    <div class=\"col text-right\">
                        <div class=\"footer-col-title\">Other Links</div>
                        <ul class=\"seo-links\">
                            <li><a href=\"#\">Link 1</a></li>
                            <li><a href=\"#\">Link 2</a></li>
                            <li><a href=\"#\">Link 3</a></li>
                            <li><a href=\"#\">Link 4</a></li>
                            <li><a href=\"#\">Link 5</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "site/layouts/main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  266 => 89,  262 => 88,  258 => 87,  251 => 83,  244 => 78,  235 => 77,  218 => 75,  205 => 8,  196 => 7,  180 => 68,  164 => 55,  155 => 49,  149 => 46,  131 => 31,  119 => 22,  110 => 16,  104 => 13,  99 => 10,  97 => 7,  87 => 5,  78 => 4,  68 => 77,  65 => 76,  63 => 75,  60 => 74,  57 => 4,  48 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'site/layouts/base.html.twig' %}

{% block body %}
    {% block header %}
        <header class=\"header{% if header_class is defined %} {{ header_class }}\"{% endif %}\">
        <div class=\"container\">
            {% block header_logo %}
                <a class=\"logo white\" href=\"{{ path('index') }}\"></a>
            {% endblock %}
            <nav class=\"primary-menu float-right\">
                <ul class=\"nav\">
                    <li>
                        <a href=\"{{ path('pricing') }}\">Pricing</a>
                    </li>
                    <li>
                        <a href=\"{{ path('free_trial') }}\">Free Trial</a>
                    </li>
                    <li>
                        <a href=\"#contacts\">Contacts</a>
                    </li>
                    <li>
                        <a href=\"{{ path('blog') }}\">Blog</a>
                    </li>
                    <li>
                        <a href=\"#faq\">FAQ</a>
                    </li>
                    <li>
                        <a href=\"#register\">Register</a>
                    </li>
                    <li class=\"nav-login-item\">
                        <a href=\"{{ path('login') }}\" class=\"button\">Login</a>
                    </li>
                </ul>
            </nav><!--nav.primary-menu-->
            <div class=\"clearfix\"></div>
            <div class=\"mobile-header-visible\">
                <div class=\"dropdown-toggle float-left menu-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\"
                     aria-expanded=\"false\">
                    <span class=\"menu-toggle-icon\"></span>
                </div>
                <div class=\"dropdown-menu mobile-menu-wrapper\" aria-labelledby=\"dropdownMenuButton\">
                    <div class=\"scrollable\">
                        <nav class=\"mobile-primary-menu\">
                            <ul>
                                <li>
                                    <a href=\"{{ path('pricing') }}\">Pricing</a>
                                </li>
                                <li>
                                    <a href=\"{{ path('free_trial') }}\">Free Trial</a>
                                </li>
                                <li>
                                    <a href=\"#contacts\">Contacts</a>
                                </li>
                                <li>
                                    <a href=\"{{ path('blog') }}\">Blog</a>
                                </li>
                                <li>
                                    <a href=\"#faq\">FAQ</a>
                                </li>
                                <li>
                                    <a href=\"#register\">Register</a>
                                </li>
                            </ul>
                        </nav><!--nav.mobile-primary-menu-->
                    </div>
                </div><!--.mobile-menu-wrapper-->
                <div class=\"nav-login-item float-right\">
                    <a href=\"{{ path('login') }}\" class=\"button\">Login</a>
                </div><!--.nav-login-item-->
            </div><!--.mobile-header-visible-->
        </div>
        </header>
    {% endblock %}

    {% block content %}{% endblock %}

    {% block footer %}
        <footer>
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col\">
                        <figure class=\"main-logo footer-logo\">
                            <a class=\"logo white\" href=\"{{ path('index') }}\"></a>
                        </figure><!--.main-logo-->
                        <div class=\"short-site-desc\">Where you can buy website traffic<br>at the best prices ever!</div>
                        <ul class=\"policy-links\">
                            <li><a href=\"{{ path('terms_and_condititons') }}\">Terms & Conditions</a></li>
                            <li><a href=\"{{ path('privacy_policy') }}\">Privacy Policy</a></li>
                            <li><a href=\"{{ path('refund_policy') }}\">Refund Policy</a></li>
                        </ul>
                    </div>
                    <div class=\"col text-right\">
                        <div class=\"footer-col-title\">Recently posted in Blog</div>
                        <ul class=\"blog-links\">
                            <li><a href=\"#\">Post 1</a></li>
                            <li><a href=\"#\">Post 2</a></li>
                            <li><a href=\"#\">Post 3</a></li>
                            <li><a href=\"#\">Post 4</a></li>
                            <li><a href=\"#\">Post 5</a></li>
                        </ul>
                    </div>
                    <div class=\"col text-right\">
                        <div class=\"footer-col-title\">Other Links</div>
                        <ul class=\"seo-links\">
                            <li><a href=\"#\">Link 1</a></li>
                            <li><a href=\"#\">Link 2</a></li>
                            <li><a href=\"#\">Link 3</a></li>
                            <li><a href=\"#\">Link 4</a></li>
                            <li><a href=\"#\">Link 5</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    {% endblock %}
{% endblock %}", "site/layouts/main.html.twig", "/var/www/trafficbot.loc/templates/site/layouts/main.html.twig");
    }
}
