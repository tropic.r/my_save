<?php

/* site/pricing.html.twig */
class __TwigTemplate_ef44b2394bb80abec78270c4d655466c38266cc3b52644f51163786f9828c14e extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("site/layouts/main.html.twig", "site/pricing.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "site/layouts/main.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "site/pricing.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "site/pricing.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    <section class=\"economy-traffic-packages first-section\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col text-center\">
                    <h2 class=\"section-title\">Pricing & plans</h2>
                    <div class=\"section-description big gray\">
                        <p>You can have unlimited number of packages of different sizes linked to your account.<br>
                            * It is possible to purchase several packages and point them to the same URLs (10 x Large
                            will give you est. 6 Million hits per month).</p>
                    </div>
                </div>
            </div>
            <div class=\"row traffic-package-gallery\">
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Micro</div>
                        <div class=\"tp-price\">\$9.9</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 50k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Small</div>
                        <div class=\"tp-price\">\$29.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 200k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Medium</div>
                        <div class=\"tp-price\">\$59.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 500k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Large</div>
                        <div class=\"tp-price\">\$99.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 1M</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.economy-traffic-packages-->
    <section class=\"free-trial\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col text-center\">
                    <h1 class=\"page-title section-title\">Get 3000 Page Views For Free!</h1>
                    <div class=\"section-description big gray\">
                        <p>Feel short form below to get 3000 free page views to your website.</p>
                    </div>
                    <form id=\"free-trial-form\">
                        <label for=\"your-email\">
                            <input id=\"your-email\" type=\"email\" name=\"your_email\" placeholder=\"Your email here*\">
                        </label><br>
                        <label for=\"your-website\">
                            <input id=\"your-website\" type=\"url\" name=\"your_website\" placeholder=\"Your website URL*\">
                        </label><br>
                        <div class=\"modified-select\">
                            <select id=\"location-select\">
                                <option>Select something</option>
                                <option value=\"global\">Global(mixed)</option>
                                <option value=\"usa\">USA</option>
                                <option value=\"global\">Europe</option>
                            </select>
                        </div>
                        <p>We don't allow affiliate links, redirects, 3rd level domains, shorteners or any similar
                            services for trial accounts, use only paid one in this case.</p>
                        <label for=\"accept-term\">
                            <input type=\"checkbox\" id=\"accept-term\" name=\"accept_term\" checked>
                            I accept {product_name} Terms & Conditions
                        </label><br>
                        <input type=\"submit\" class=\"button color-button big\" value=\"Start free trial\">
                    </form>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.free-trial-->
    <section class=\"contact-us dark-version\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col\">
                    <div class=\"content-wrapper\">
                        <h2 class=\"section-title text-center\">Get Even More Traffic For Free</h2>
                        <div class=\"section-description text-center\">
                            <p>In case you have a blog on your website and would like to share our
                                service with your audience by publishing an article or guest post,
                                we are happy to offer you free credits for traffic in exchange.</p>
                            <p>Feel free to send us a proposal how you would like to collaborate
                                and we will get back to you with amount of traffic we will be
                                able to apply to your website. This amount will depend on the quality
                                of your website and the volume of your audience.</p>
                        </div>
                        <form id=\"contact-us-form\">
                            <div class=\"row\">
                                <div class=\"col\">
                                    <label for=\"visitor-name\">
                                        <span>Your name</span><br>
                                        <input type=\"text\" id=\"visitor-name\" name=\"visitor_name\">
                                    </label>
                                </div>
                                <div class=\"col\">
                                    <label for=\"visitor-email\">
                                        <span>Your E-mail</span><br>
                                        <input type=\"text\" id=\"visitor-email\" name=\"visitor_email\">
                                    </label>
                                </div>
                                <div class=\"w-100\"></div>
                                <div class=\"col\">
                                    <label for=\"visitor-message\">
                                        <span>Your message</span><br>
                                        <textarea id=\"visitor-message\" name=\"visitor_message\"
                                                  placeholder=\"Add as much information as you find needed, but don’t forget website url and screenshot from GA with monthly volume. \"></textarea>
                                    </label>
                                    <div class=\"contact-form-description text-center\">
                                        <input type=\"submit\" class=\"button color-button big\" value=\"Claim Free Traffic\">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!--.content-wrapper-->
                </div><!--.col-->
            </div><!--.row-->
        </div><!--.container-->
    </section><!--.contact-us-->
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "site/pricing.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 4,  44 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'site/layouts/main.html.twig' %}

{% block content %}
    <section class=\"economy-traffic-packages first-section\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col text-center\">
                    <h2 class=\"section-title\">Pricing & plans</h2>
                    <div class=\"section-description big gray\">
                        <p>You can have unlimited number of packages of different sizes linked to your account.<br>
                            * It is possible to purchase several packages and point them to the same URLs (10 x Large
                            will give you est. 6 Million hits per month).</p>
                    </div>
                </div>
            </div>
            <div class=\"row traffic-package-gallery\">
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Micro</div>
                        <div class=\"tp-price\">\$9.9</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 50k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Small</div>
                        <div class=\"tp-price\">\$29.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 200k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Medium</div>
                        <div class=\"tp-price\">\$59.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 500k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Large</div>
                        <div class=\"tp-price\">\$99.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 1M</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.economy-traffic-packages-->
    <section class=\"free-trial\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col text-center\">
                    <h1 class=\"page-title section-title\">Get 3000 Page Views For Free!</h1>
                    <div class=\"section-description big gray\">
                        <p>Feel short form below to get 3000 free page views to your website.</p>
                    </div>
                    <form id=\"free-trial-form\">
                        <label for=\"your-email\">
                            <input id=\"your-email\" type=\"email\" name=\"your_email\" placeholder=\"Your email here*\">
                        </label><br>
                        <label for=\"your-website\">
                            <input id=\"your-website\" type=\"url\" name=\"your_website\" placeholder=\"Your website URL*\">
                        </label><br>
                        <div class=\"modified-select\">
                            <select id=\"location-select\">
                                <option>Select something</option>
                                <option value=\"global\">Global(mixed)</option>
                                <option value=\"usa\">USA</option>
                                <option value=\"global\">Europe</option>
                            </select>
                        </div>
                        <p>We don't allow affiliate links, redirects, 3rd level domains, shorteners or any similar
                            services for trial accounts, use only paid one in this case.</p>
                        <label for=\"accept-term\">
                            <input type=\"checkbox\" id=\"accept-term\" name=\"accept_term\" checked>
                            I accept {product_name} Terms & Conditions
                        </label><br>
                        <input type=\"submit\" class=\"button color-button big\" value=\"Start free trial\">
                    </form>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.free-trial-->
    <section class=\"contact-us dark-version\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col\">
                    <div class=\"content-wrapper\">
                        <h2 class=\"section-title text-center\">Get Even More Traffic For Free</h2>
                        <div class=\"section-description text-center\">
                            <p>In case you have a blog on your website and would like to share our
                                service with your audience by publishing an article or guest post,
                                we are happy to offer you free credits for traffic in exchange.</p>
                            <p>Feel free to send us a proposal how you would like to collaborate
                                and we will get back to you with amount of traffic we will be
                                able to apply to your website. This amount will depend on the quality
                                of your website and the volume of your audience.</p>
                        </div>
                        <form id=\"contact-us-form\">
                            <div class=\"row\">
                                <div class=\"col\">
                                    <label for=\"visitor-name\">
                                        <span>Your name</span><br>
                                        <input type=\"text\" id=\"visitor-name\" name=\"visitor_name\">
                                    </label>
                                </div>
                                <div class=\"col\">
                                    <label for=\"visitor-email\">
                                        <span>Your E-mail</span><br>
                                        <input type=\"text\" id=\"visitor-email\" name=\"visitor_email\">
                                    </label>
                                </div>
                                <div class=\"w-100\"></div>
                                <div class=\"col\">
                                    <label for=\"visitor-message\">
                                        <span>Your message</span><br>
                                        <textarea id=\"visitor-message\" name=\"visitor_message\"
                                                  placeholder=\"Add as much information as you find needed, but don’t forget website url and screenshot from GA with monthly volume. \"></textarea>
                                    </label>
                                    <div class=\"contact-form-description text-center\">
                                        <input type=\"submit\" class=\"button color-button big\" value=\"Claim Free Traffic\">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!--.content-wrapper-->
                </div><!--.col-->
            </div><!--.row-->
        </div><!--.container-->
    </section><!--.contact-us-->
{% endblock %}", "site/pricing.html.twig", "/var/www/trafficbot.loc/templates/site/pricing.html.twig");
    }
}
