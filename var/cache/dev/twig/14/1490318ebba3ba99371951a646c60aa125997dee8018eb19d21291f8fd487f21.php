<?php

/* @OldSoundRabbitMq/Collector/collector.html.twig */
class __TwigTemplate_676a4b02fd2a5e36c3da89632c25d332048559e17f381861e6680310be545bee extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("WebProfilerBundle:Profiler:layout.html.twig", "@OldSoundRabbitMq/Collector/collector.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "WebProfilerBundle:Profiler:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@OldSoundRabbitMq/Collector/collector.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@OldSoundRabbitMq/Collector/collector.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        if (twig_get_attribute($this->env, $this->source, (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 4, $this->source); })()), "publishedMessagesCount", array())) {
            // line 5
            echo "        ";
            ob_start();
            // line 6
            echo "            <img width=\"28\" height=\"28\" alt=\"RabbitMQ\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAqlJREFUeNrsV01oE0EU/maz2U1tUFqtAY1QsaQVJKmHFjEF0R5E9CQIHhQF0UNBBL2IJ3tUevGiIOjFg0IpiErRXppD0VOpJCohQqWNYmzVtknaZtP9cWaTtKadDU3cmB76wUvCzJt5386+970JMQwDDMZQ3wmkf/bAIQKqAuz23yKHzkfAgRF/78Zo/9PlAZ3u4T95hbQEv6NMiOZnrL+bPO8dhKQBhACaDmzZ6kWL/xh2BGZWLyJDt+9g9M0pyEJuQKH+8bcDuBY6XC6B3A5fPnRiNg0sLALzC/Q7A/yYasfnsYPcVePhdtOH+Rb8J2IBVIDcCTicikmFFB4xT00QVe4qh5Q1ff72FyWlEgICaoxNApsENjgBqzIkgs4Z0+wjwOqahVica+DOZ1LuZQ0o+CvJxsqFiEeAUXt99ybi4QCVZx2GIdA+oSIRbcPvr3uKVrLf6V8C+o6OwBt4B21JtozI+oarPo0jl3ux06eIlo5OaonxLkzc7ypWQWp1HMJsPBIKYoxaKbDel6WmZWWcvXdDLOnozNt64VqnH20dmP3WXNsqEBxL1jnwLzA4r6fsJKwUyqraUvMRJA4x2wmwYL7OFwhefARRzpi6kEzswvCDq5ie7LDKJdG24PWNUVx6cgYeX7ZorsE7iYcXQtB1bsbZk4RMtOq2za0JzuBpjUKSLV+BfVVg6Py9dDZO/kMzYmrJLzc1d0TVJMBUMDXtQWTwwJq58MvTyGQsI4m2EVDmm/H43AD2H38FyUVzgXbM1FQTPtLru2itB/aVIdspNePDyLPrRSIklT5ne4WI16g27o3IcKycgEpbo4ZSyWovWCxNda4QaNr3CRLJXRZItR88f+57O4bNNCn8O0Ys1EavYNsLbbJ6BDQnZHcSrd2RYgI1wh8BBgAR2M5KdN1kRwAAAABJRU5ErkJggg==\" />
            <span class=\"sf-toolbar-status\">";
            // line 7
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 7, $this->source); })()), "publishedMessagesCount", array()), "html", null, true);
            echo "</span>
        ";
            $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
            // line 9
            echo "        ";
            ob_start();
            // line 10
            echo "            <div class=\"sf-toolbar-info-piece\">
                <b>Messages</b>
                <span>";
            // line 12
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 12, $this->source); })()), "publishedMessagesCount", array()), "html", null, true);
            echo "</span>
            </div>
        ";
            $context["text"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
            // line 15
            echo "        ";
            $this->loadTemplate("WebProfilerBundle:Profiler:toolbar_item.html.twig", "@OldSoundRabbitMq/Collector/collector.html.twig", 15)->display(array_merge($context, array("link" => (isset($context["profiler_url"]) || array_key_exists("profiler_url", $context) ? $context["profiler_url"] : (function () { throw new Twig_Error_Runtime('Variable "profiler_url" does not exist.', 15, $this->source); })()))));
            // line 16
            echo "    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 19
    public function block_menu($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 20
        echo "<span class=\"label\">
    <span class=\"icon\"><img alt=\"RabbitMQ\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAqlJREFUeNrsV01oE0EU/maz2U1tUFqtAY1QsaQVJKmHFjEF0R5E9CQIHhQF0UNBBL2IJ3tUevGiIOjFg0IpiErRXppD0VOpJCohQqWNYmzVtknaZtP9cWaTtKadDU3cmB76wUvCzJt5386+970JMQwDDMZQ3wmkf/bAIQKqAuz23yKHzkfAgRF/78Zo/9PlAZ3u4T95hbQEv6NMiOZnrL+bPO8dhKQBhACaDmzZ6kWL/xh2BGZWLyJDt+9g9M0pyEJuQKH+8bcDuBY6XC6B3A5fPnRiNg0sLALzC/Q7A/yYasfnsYPcVePhdtOH+Rb8J2IBVIDcCTicikmFFB4xT00QVe4qh5Q1ff72FyWlEgICaoxNApsENjgBqzIkgs4Z0+wjwOqahVica+DOZ1LuZQ0o+CvJxsqFiEeAUXt99ybi4QCVZx2GIdA+oSIRbcPvr3uKVrLf6V8C+o6OwBt4B21JtozI+oarPo0jl3ux06eIlo5OaonxLkzc7ypWQWp1HMJsPBIKYoxaKbDel6WmZWWcvXdDLOnozNt64VqnH20dmP3WXNsqEBxL1jnwLzA4r6fsJKwUyqraUvMRJA4x2wmwYL7OFwhefARRzpi6kEzswvCDq5ie7LDKJdG24PWNUVx6cgYeX7ZorsE7iYcXQtB1bsbZk4RMtOq2za0JzuBpjUKSLV+BfVVg6Py9dDZO/kMzYmrJLzc1d0TVJMBUMDXtQWTwwJq58MvTyGQsI4m2EVDmm/H43AD2H38FyUVzgXbM1FQTPtLru2itB/aVIdspNePDyLPrRSIklT5ne4WI16g27o3IcKycgEpbo4ZSyWovWCxNda4QaNr3CRLJXRZItR88f+57O4bNNCn8O0Ys1EavYNsLbbJ6BDQnZHcSrd2RYgI1wh8BBgAR2M5KdN1kRwAAAABJRU5ErkJggg==\" /></span>
    <strong>RabbitMQ</strong>
    <span class=\"count\">
        <span>";
        // line 24
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 24, $this->source); })()), "publishedMessagesCount", array()), "html", null, true);
        echo "</span>
    </span>
</span>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 29
    public function block_panel($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 30
        echo "    <h2>Messages</h2>
    ";
        // line 31
        if (twig_get_attribute($this->env, $this->source, (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 31, $this->source); })()), "publishedMessagesCount", array())) {
            // line 32
            echo "        <table>
            <thead>
                <tr>
                    <th scope=\"col\">Exchange</th>
                    <th scope=\"col\">Message body</th>
                </tr>
            </thead>
            <tbody>
                ";
            // line 40
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 40, $this->source); })()), "publishedMessagesLog", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["log"]) {
                // line 41
                echo "                <tr>
                    <td>";
                // line 42
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["log"], "exchange", array()), "html", null, true);
                echo "</td>
                    <td>";
                // line 43
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["log"], "msg", array()), "body", array()), "html", null, true);
                echo "</td>
                </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['log'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 46
            echo "            </tbody>
        </table>
    ";
        } else {
            // line 49
            echo "        <p>
            <em>No messages were sent.</em>
        </p>
    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@OldSoundRabbitMq/Collector/collector.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  174 => 49,  169 => 46,  160 => 43,  156 => 42,  153 => 41,  149 => 40,  139 => 32,  137 => 31,  134 => 30,  125 => 29,  111 => 24,  105 => 20,  96 => 19,  85 => 16,  82 => 15,  76 => 12,  72 => 10,  69 => 9,  64 => 7,  61 => 6,  58 => 5,  55 => 4,  46 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'WebProfilerBundle:Profiler:layout.html.twig' %}

{% block toolbar %}
    {% if collector.publishedMessagesCount %}
        {% set icon %}
            <img width=\"28\" height=\"28\" alt=\"RabbitMQ\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAqlJREFUeNrsV01oE0EU/maz2U1tUFqtAY1QsaQVJKmHFjEF0R5E9CQIHhQF0UNBBL2IJ3tUevGiIOjFg0IpiErRXppD0VOpJCohQqWNYmzVtknaZtP9cWaTtKadDU3cmB76wUvCzJt5386+970JMQwDDMZQ3wmkf/bAIQKqAuz23yKHzkfAgRF/78Zo/9PlAZ3u4T95hbQEv6NMiOZnrL+bPO8dhKQBhACaDmzZ6kWL/xh2BGZWLyJDt+9g9M0pyEJuQKH+8bcDuBY6XC6B3A5fPnRiNg0sLALzC/Q7A/yYasfnsYPcVePhdtOH+Rb8J2IBVIDcCTicikmFFB4xT00QVe4qh5Q1ff72FyWlEgICaoxNApsENjgBqzIkgs4Z0+wjwOqahVica+DOZ1LuZQ0o+CvJxsqFiEeAUXt99ybi4QCVZx2GIdA+oSIRbcPvr3uKVrLf6V8C+o6OwBt4B21JtozI+oarPo0jl3ux06eIlo5OaonxLkzc7ypWQWp1HMJsPBIKYoxaKbDel6WmZWWcvXdDLOnozNt64VqnH20dmP3WXNsqEBxL1jnwLzA4r6fsJKwUyqraUvMRJA4x2wmwYL7OFwhefARRzpi6kEzswvCDq5ie7LDKJdG24PWNUVx6cgYeX7ZorsE7iYcXQtB1bsbZk4RMtOq2za0JzuBpjUKSLV+BfVVg6Py9dDZO/kMzYmrJLzc1d0TVJMBUMDXtQWTwwJq58MvTyGQsI4m2EVDmm/H43AD2H38FyUVzgXbM1FQTPtLru2itB/aVIdspNePDyLPrRSIklT5ne4WI16g27o3IcKycgEpbo4ZSyWovWCxNda4QaNr3CRLJXRZItR88f+57O4bNNCn8O0Ys1EavYNsLbbJ6BDQnZHcSrd2RYgI1wh8BBgAR2M5KdN1kRwAAAABJRU5ErkJggg==\" />
            <span class=\"sf-toolbar-status\">{{ collector.publishedMessagesCount }}</span>
        {% endset %}
        {% set text %}
            <div class=\"sf-toolbar-info-piece\">
                <b>Messages</b>
                <span>{{ collector.publishedMessagesCount }}</span>
            </div>
        {% endset %}
        {% include 'WebProfilerBundle:Profiler:toolbar_item.html.twig' with { 'link': profiler_url } %}
    {% endif %}
{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\"><img alt=\"RabbitMQ\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAqlJREFUeNrsV01oE0EU/maz2U1tUFqtAY1QsaQVJKmHFjEF0R5E9CQIHhQF0UNBBL2IJ3tUevGiIOjFg0IpiErRXppD0VOpJCohQqWNYmzVtknaZtP9cWaTtKadDU3cmB76wUvCzJt5386+970JMQwDDMZQ3wmkf/bAIQKqAuz23yKHzkfAgRF/78Zo/9PlAZ3u4T95hbQEv6NMiOZnrL+bPO8dhKQBhACaDmzZ6kWL/xh2BGZWLyJDt+9g9M0pyEJuQKH+8bcDuBY6XC6B3A5fPnRiNg0sLALzC/Q7A/yYasfnsYPcVePhdtOH+Rb8J2IBVIDcCTicikmFFB4xT00QVe4qh5Q1ff72FyWlEgICaoxNApsENjgBqzIkgs4Z0+wjwOqahVica+DOZ1LuZQ0o+CvJxsqFiEeAUXt99ybi4QCVZx2GIdA+oSIRbcPvr3uKVrLf6V8C+o6OwBt4B21JtozI+oarPo0jl3ux06eIlo5OaonxLkzc7ypWQWp1HMJsPBIKYoxaKbDel6WmZWWcvXdDLOnozNt64VqnH20dmP3WXNsqEBxL1jnwLzA4r6fsJKwUyqraUvMRJA4x2wmwYL7OFwhefARRzpi6kEzswvCDq5ie7LDKJdG24PWNUVx6cgYeX7ZorsE7iYcXQtB1bsbZk4RMtOq2za0JzuBpjUKSLV+BfVVg6Py9dDZO/kMzYmrJLzc1d0TVJMBUMDXtQWTwwJq58MvTyGQsI4m2EVDmm/H43AD2H38FyUVzgXbM1FQTPtLru2itB/aVIdspNePDyLPrRSIklT5ne4WI16g27o3IcKycgEpbo4ZSyWovWCxNda4QaNr3CRLJXRZItR88f+57O4bNNCn8O0Ys1EavYNsLbbJ6BDQnZHcSrd2RYgI1wh8BBgAR2M5KdN1kRwAAAABJRU5ErkJggg==\" /></span>
    <strong>RabbitMQ</strong>
    <span class=\"count\">
        <span>{{ collector.publishedMessagesCount }}</span>
    </span>
</span>
{% endblock %}

{% block panel %}
    <h2>Messages</h2>
    {% if collector.publishedMessagesCount %}
        <table>
            <thead>
                <tr>
                    <th scope=\"col\">Exchange</th>
                    <th scope=\"col\">Message body</th>
                </tr>
            </thead>
            <tbody>
                {% for log in collector.publishedMessagesLog %}
                <tr>
                    <td>{{ log.exchange }}</td>
                    <td>{{ log.msg.body }}</td>
                </tr>
                {% endfor %}
            </tbody>
        </table>
    {% else %}
        <p>
            <em>No messages were sent.</em>
        </p>
    {% endif %}
{% endblock %}
", "@OldSoundRabbitMq/Collector/collector.html.twig", "/var/www/trafficbot.loc/vendor/php-amqplib/rabbitmq-bundle/Resources/views/Collector/collector.html.twig");
    }
}
