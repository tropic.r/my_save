<?php

/* site/how_it_works.html.twig */
class __TwigTemplate_aa9a6c954eb379b22e19be81392ce23912dd0d73df9f8115fe60ebf195b3bde8 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("site/layouts/main.html.twig", "site/how_it_works.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "site/layouts/main.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "site/how_it_works.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "site/how_it_works.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    <section class=\"how-it-works first-section\">
        <div class=\"container\">
            <h2 class=\"section-title text-center\">How it Works</h2>
            <div class=\"row\">
                <div class=\"col\">
                    <div class=\"section-description\">
                        <p>Our platform generates traffic, which is by behavior very similar to what real
                            users do - surfing pages, click links on your website, download tracking scripts,
                            submit forms, etc. We use real browsers, so any scripts on your site are executed
                            (you can also exclude some from downloading to comply with 3d party terms).
                            You are very flexible to setup visitor flows - what pages users should reach first,
                            what next, how traffic should be redistributed across the day, from what locations traffic
                            should go. You can customize a lot of metrics like bounce rate, time on page, returning
                            visitors,
                            traffic sources, % of mobile traffic and a lot more. It is possible to set up multiple flows
                            to get traffic
                            looks more naturally</p>
                    </div>
                </div>
                <div class=\"col\">
                    <img class=\"how-it-works-back position-absolute\" src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/how-it-works.png"), "html", null, true);
        echo "\"
                         alt=\"how-it-works\">
                </div>
            </div><!--.row-->
            <div class=\"text-center\">
                <a href=\"#read_more\" class=\"how-it-works-read button color-button big position-relative\">Read More</a>
            </div>
        </div>
    </section><!--.how-it-works-->
    <section class=\"metrics-list\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col text-center\">
                    <h2 class=\"section-title dark\">List of metrics to customize</h2>
                    <div class=\"section-description big dark\">
                        <p>Below you can find list of metrics you can customize to make traffic coming to your website
                            look
                            more natural. If you don’t find desired metric in the list, contact us and we will try to
                            meet your needs.</p>
                    </div>
                </div>
            </div>
            <div class=\"row metrics-list-gallery\">
                <div class=\"col\">
                    <div class=\"ml-gallery-item\">
                        <div class=\"ml-gallery-image\"></div>
                        <div class=\"ml-gallery-title\">keywords</div>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"ml-gallery-item\">
                        <div class=\"ml-gallery-image\"></div>
                        <div class=\"ml-gallery-title\">referrer</div>
                    </div>
                </div>
                <div class=\"w-100\"></div>
                <div class=\"col\">
                    <div class=\"ml-gallery-item\">
                        <div class=\"ml-gallery-image\"></div>
                        <div class=\"ml-gallery-title\">bounce rate</div>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"ml-gallery-item\">
                        <div class=\"ml-gallery-image\"></div>
                        <div class=\"ml-gallery-title\">daytime</div>
                    </div>
                </div>
                <div class=\"w-100\"></div>
                <div class=\"col\">
                    <div class=\"ml-gallery-item\">
                        <div class=\"ml-gallery-image\"></div>
                        <div class=\"ml-gallery-title\">session duration</div>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"ml-gallery-item\">
                        <div class=\"ml-gallery-image\"></div>
                        <div class=\"ml-gallery-title\">returning visitors</div>
                    </div>
                </div>
                <div class=\"w-100\"></div>
                <div class=\"col\">
                    <div class=\"ml-gallery-item\">
                        <div class=\"ml-gallery-image\"></div>
                        <div class=\"ml-gallery-title\">session duration</div>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"ml-gallery-item\">
                        <div class=\"ml-gallery-image\"></div>
                        <div class=\"ml-gallery-title\">returning visitors</div>
                    </div>
                </div>
                <div class=\"w-100\"></div>
                <div class=\"col other-metrics-list text-center\">
                    <span>cities & states</span>
                    <span>isp's</span>
                    <span>social traffic</span>
                    <span>visited pages</span>
                    <span>browsers</span>
                    <span>operating systems</span>
                    <span>screen resolutions</span>
                    <span>mobile devices</span>
                </div>
            </div><!--.metrics-list-gallery-->
        </div><!--.container-->
    </section><!--.metrics-list-->
    <section class=\"traffic-analytics-tiles\">
        <div class=\"row no-gutters\">
            <div class=\"col no-content\"></div>
            <div class=\"col orange-tile\">
                <div class=\"half-container float-left\">
                    <h4 class=\"ta-tile-title\">AdSense Safe</h4>
                    <div class=\"section-description\"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan
                            et viverra justo commodo. Proin sodales pulvinar sic tempor.
                            Sociis natoque penatibus et magnis dis parturient montes,
                            nascetur ridiculus mus.</p></div>
                    <a href=\"";
        // line 123
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pricing");
        echo "\" class=\"button white-button big\">Browse Packages</a>
                </div>
            </div>
            <div class=\"w-100\"></div>
            <div class=\"col blue-tile\">
                <div class=\"half-container float-right text-right\">
                    <h4 class=\"ta-tile-title\">Alexa Ranking Improve</h4>
                    <div class=\"section-description\"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan
                            et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque
                            penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            Nam fermentum, nulla luctus pharetra vulputate</p></div>
                    <a href=\"#learn_more\" class=\"button white-button big\">Learn More</a>
                </div>
            </div>
            <div class=\"col no-content\"></div>
            <div class=\"w-100\"></div>
            <div class=\"col no-content\">&nbsp;</div>
            <div class=\"col green-tile\">
                <div class=\"half-container float-left\">
                    <h4 class=\"ta-tile-title\">Supported Countries</h4>
                    <div class=\"section-description\"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan
                            et viverra justo commodo.</p></div>
                    <a href=\"";
        // line 147
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pricing");
        echo "\" class=\"button white-button big\">Browse Packages</a>
                </div>
            </div>
        </div>
    </section><!--.traffic-analytics-tiles-->
    <section class=\"economy-traffic-packages\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col text-center\">
                    <h2 class=\"section-title\">Economy Traffic Packages</h2>
                    <div class=\"section-description big gray\">
                        <p>You can have unlimited number of packages of different sizes linked to your account.<br>
                            * It is possible to purchase several packages and point them to the same URLs (10 x Large
                            will give you est. 6 Million hits per month).</p>
                    </div>
                </div>
            </div>
            <div class=\"row traffic-package-gallery\">
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Micro</div>
                        <div class=\"tp-price\">\$9.9</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 50k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Small</div>
                        <div class=\"tp-price\">\$29.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 200k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Medium</div>
                        <div class=\"tp-price\">\$59.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 500k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Large</div>
                        <div class=\"tp-price\">\$99.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 1M</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.economy-traffic-packages-->
    <section class=\"contact-us\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col\">
                    <h2 class=\"section-title text-center dark\">Contact Us</h2>
                    <form id=\"contact-us-form\">
                        <div class=\"row\">
                            <div class=\"col\">
                                <label for=\"visitor-name\">
                                    <span>Your name</span><br>
                                    <input type=\"text\" id=\"visitor-name\" name=\"visitor_name\">
                                </label>
                            </div>
                            <div class=\"col\">
                                <label for=\"visitor-email\">
                                    <span>Your E-mail</span><br>
                                    <input type=\"text\" id=\"visitor-email\" name=\"visitor_email\">
                                </label>
                            </div>
                            <div class=\"w-100\"></div>
                            <div class=\"col\">
                                <label for=\"visitor-message\">
                                    <span>Your message</span><br>
                                    <textarea id=\"visitor-message\" name=\"visitor_message\"
                                              placeholder=\"Write us something\"></textarea>
                                </label>
                                <div class=\"contact-form-description clerafix\">
                                    <p>You can always send us an email at <a href=\"mailto:support@produst_name.com\">support@{produst_name}.com</a>
                                    </p>
                                    <input type=\"submit\" class=\"button color-button big float-right\"
                                           value=\"Send Message\">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.contact-us-->

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "site/how_it_works.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  204 => 147,  177 => 123,  75 => 24,  53 => 4,  44 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'site/layouts/main.html.twig' %}

{% block content %}
    <section class=\"how-it-works first-section\">
        <div class=\"container\">
            <h2 class=\"section-title text-center\">How it Works</h2>
            <div class=\"row\">
                <div class=\"col\">
                    <div class=\"section-description\">
                        <p>Our platform generates traffic, which is by behavior very similar to what real
                            users do - surfing pages, click links on your website, download tracking scripts,
                            submit forms, etc. We use real browsers, so any scripts on your site are executed
                            (you can also exclude some from downloading to comply with 3d party terms).
                            You are very flexible to setup visitor flows - what pages users should reach first,
                            what next, how traffic should be redistributed across the day, from what locations traffic
                            should go. You can customize a lot of metrics like bounce rate, time on page, returning
                            visitors,
                            traffic sources, % of mobile traffic and a lot more. It is possible to set up multiple flows
                            to get traffic
                            looks more naturally</p>
                    </div>
                </div>
                <div class=\"col\">
                    <img class=\"how-it-works-back position-absolute\" src=\"{{ asset('s/build/site/img/how-it-works.png') }}\"
                         alt=\"how-it-works\">
                </div>
            </div><!--.row-->
            <div class=\"text-center\">
                <a href=\"#read_more\" class=\"how-it-works-read button color-button big position-relative\">Read More</a>
            </div>
        </div>
    </section><!--.how-it-works-->
    <section class=\"metrics-list\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col text-center\">
                    <h2 class=\"section-title dark\">List of metrics to customize</h2>
                    <div class=\"section-description big dark\">
                        <p>Below you can find list of metrics you can customize to make traffic coming to your website
                            look
                            more natural. If you don’t find desired metric in the list, contact us and we will try to
                            meet your needs.</p>
                    </div>
                </div>
            </div>
            <div class=\"row metrics-list-gallery\">
                <div class=\"col\">
                    <div class=\"ml-gallery-item\">
                        <div class=\"ml-gallery-image\"></div>
                        <div class=\"ml-gallery-title\">keywords</div>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"ml-gallery-item\">
                        <div class=\"ml-gallery-image\"></div>
                        <div class=\"ml-gallery-title\">referrer</div>
                    </div>
                </div>
                <div class=\"w-100\"></div>
                <div class=\"col\">
                    <div class=\"ml-gallery-item\">
                        <div class=\"ml-gallery-image\"></div>
                        <div class=\"ml-gallery-title\">bounce rate</div>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"ml-gallery-item\">
                        <div class=\"ml-gallery-image\"></div>
                        <div class=\"ml-gallery-title\">daytime</div>
                    </div>
                </div>
                <div class=\"w-100\"></div>
                <div class=\"col\">
                    <div class=\"ml-gallery-item\">
                        <div class=\"ml-gallery-image\"></div>
                        <div class=\"ml-gallery-title\">session duration</div>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"ml-gallery-item\">
                        <div class=\"ml-gallery-image\"></div>
                        <div class=\"ml-gallery-title\">returning visitors</div>
                    </div>
                </div>
                <div class=\"w-100\"></div>
                <div class=\"col\">
                    <div class=\"ml-gallery-item\">
                        <div class=\"ml-gallery-image\"></div>
                        <div class=\"ml-gallery-title\">session duration</div>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"ml-gallery-item\">
                        <div class=\"ml-gallery-image\"></div>
                        <div class=\"ml-gallery-title\">returning visitors</div>
                    </div>
                </div>
                <div class=\"w-100\"></div>
                <div class=\"col other-metrics-list text-center\">
                    <span>cities & states</span>
                    <span>isp's</span>
                    <span>social traffic</span>
                    <span>visited pages</span>
                    <span>browsers</span>
                    <span>operating systems</span>
                    <span>screen resolutions</span>
                    <span>mobile devices</span>
                </div>
            </div><!--.metrics-list-gallery-->
        </div><!--.container-->
    </section><!--.metrics-list-->
    <section class=\"traffic-analytics-tiles\">
        <div class=\"row no-gutters\">
            <div class=\"col no-content\"></div>
            <div class=\"col orange-tile\">
                <div class=\"half-container float-left\">
                    <h4 class=\"ta-tile-title\">AdSense Safe</h4>
                    <div class=\"section-description\"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan
                            et viverra justo commodo. Proin sodales pulvinar sic tempor.
                            Sociis natoque penatibus et magnis dis parturient montes,
                            nascetur ridiculus mus.</p></div>
                    <a href=\"{{ path('pricing') }}\" class=\"button white-button big\">Browse Packages</a>
                </div>
            </div>
            <div class=\"w-100\"></div>
            <div class=\"col blue-tile\">
                <div class=\"half-container float-right text-right\">
                    <h4 class=\"ta-tile-title\">Alexa Ranking Improve</h4>
                    <div class=\"section-description\"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan
                            et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque
                            penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            Nam fermentum, nulla luctus pharetra vulputate</p></div>
                    <a href=\"#learn_more\" class=\"button white-button big\">Learn More</a>
                </div>
            </div>
            <div class=\"col no-content\"></div>
            <div class=\"w-100\"></div>
            <div class=\"col no-content\">&nbsp;</div>
            <div class=\"col green-tile\">
                <div class=\"half-container float-left\">
                    <h4 class=\"ta-tile-title\">Supported Countries</h4>
                    <div class=\"section-description\"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan
                            et viverra justo commodo.</p></div>
                    <a href=\"{{ path('pricing') }}\" class=\"button white-button big\">Browse Packages</a>
                </div>
            </div>
        </div>
    </section><!--.traffic-analytics-tiles-->
    <section class=\"economy-traffic-packages\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col text-center\">
                    <h2 class=\"section-title\">Economy Traffic Packages</h2>
                    <div class=\"section-description big gray\">
                        <p>You can have unlimited number of packages of different sizes linked to your account.<br>
                            * It is possible to purchase several packages and point them to the same URLs (10 x Large
                            will give you est. 6 Million hits per month).</p>
                    </div>
                </div>
            </div>
            <div class=\"row traffic-package-gallery\">
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Micro</div>
                        <div class=\"tp-price\">\$9.9</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 50k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Small</div>
                        <div class=\"tp-price\">\$29.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 200k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Medium</div>
                        <div class=\"tp-price\">\$59.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 500k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Large</div>
                        <div class=\"tp-price\">\$99.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 1M</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.economy-traffic-packages-->
    <section class=\"contact-us\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col\">
                    <h2 class=\"section-title text-center dark\">Contact Us</h2>
                    <form id=\"contact-us-form\">
                        <div class=\"row\">
                            <div class=\"col\">
                                <label for=\"visitor-name\">
                                    <span>Your name</span><br>
                                    <input type=\"text\" id=\"visitor-name\" name=\"visitor_name\">
                                </label>
                            </div>
                            <div class=\"col\">
                                <label for=\"visitor-email\">
                                    <span>Your E-mail</span><br>
                                    <input type=\"text\" id=\"visitor-email\" name=\"visitor_email\">
                                </label>
                            </div>
                            <div class=\"w-100\"></div>
                            <div class=\"col\">
                                <label for=\"visitor-message\">
                                    <span>Your message</span><br>
                                    <textarea id=\"visitor-message\" name=\"visitor_message\"
                                              placeholder=\"Write us something\"></textarea>
                                </label>
                                <div class=\"contact-form-description clerafix\">
                                    <p>You can always send us an email at <a href=\"mailto:support@produst_name.com\">support@{produst_name}.com</a>
                                    </p>
                                    <input type=\"submit\" class=\"button color-button big float-right\"
                                           value=\"Send Message\">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.contact-us-->

{% endblock %}", "site/how_it_works.html.twig", "/var/www/trafficbot.loc/templates/site/how_it_works.html.twig");
    }
}
