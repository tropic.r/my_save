<?php

/* site/geo_targeting.html.twig */
class __TwigTemplate_6ae19853e313ba2e20b05032c8b2c4adf139ad97e5284b7fc35fa5f3da637b4f extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("site/layouts/main.html.twig", "site/geo_targeting.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "site/layouts/main.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "site/geo_targeting.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "site/geo_targeting.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    <section class=\"geo-targeting-header\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col text-center\">
                    <h1 class=\"section-title\">GEO Targeting</h1>
                </div>
            </div>
        </div>
    </section><!--.geo-targeting-header-->
    <section class=\"geo-features\">
        <div class=\"container\">
            <div class=\"row geo-feature-item left-image no-gutters\">
                <div class=\"col-5\">
                    <div class=\"geof-images\"></div>
                </div>
                <div class=\"col-7\">
                    <div class=\"geof-desc\">
                        <h2 class=\"geof-title\">Free Geo Targeting</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Ut pretium pretium tempor. Ut eget imperdiet neque.
                            In volutpat ante semper diam molestie, et aliquam erat laoreet.
                            Sed sit amet arcu aliquet,.</p>
                        <a href=\"#see_plans\" class=\"button color-button float-right\">See Plans</a>
                    </div>
                </div>
            </div>
            <div class=\"row geo-feature-item right-image no-gutters\">
                <div class=\"col-7\">
                    <div class=\"geof-desc\">
                        <h2 class=\"geof-title\">Local Ranking Improve</h2>
                        <p>Donec facilisis tortor ut augue lacinia,
                            at viverra est semper. Sed sapien metus,
                            scelerisque nec pharetra id, tempor a tortor.
                            Pellentesque non dignissim neque. Ut porta viverra est, ut dignissim elit.</p>
                        <a href=\"#learn_more\" class=\"button color-button\">Learn More</a>
                    </div>
                </div>
                <div class=\"col-5\">
                    <div class=\"geof-images float-right\"></div>
                </div>
            </div>
            <div class=\"row geo-feature-item left-image no-gutters\">
                <div class=\"col-5\">
                    <div class=\"geof-images\"></div>
                </div>
                <div class=\"col-7\">
                    <div class=\"geof-desc\">
                        <h2 class=\"geof-title\">Traffic from ISP’s</h2>
                        <p>Vestibulum rutrum quam vitae fringilla tincidunt.
                            Suspendisse nec tortor urna. Ut laoreet sodales nisi,
                            quis iaculis nulla iaculis vitae. Donec sagittis faucibus
                            lacus eget blandit. Mauris vitae ultricies me.</p>
                        <a href=\"#read_more\" class=\"button color-button float-right\">Read More</a>
                    </div>
                </div>
            </div>
            <div class=\"row geo-feature-item right-image no-gutters\">
                <div class=\"col-7\">
                    <div class=\"geof-desc\">
                        <h2 class=\"geof-title\">Cities Geo Targeting</h2v>
                            <p>Cras quis nulla commodo, aliquam lectus sed, blandit augue.
                                Cras ullamcorper bibendum bibendum. Duis tincidunt urna non pretium porta.
                                Nam condimentum vitae ligula vel ornare.</p>
                            <a href=\"#browse_packages\" class=\"button color-button\">Browse Packages</a>
                    </div>
                </div>
                <div class=\"col-5\">
                    <div class=\"geof-images float-right\"></div>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.geo-features-->
    <section class=\"geo-countries\">
        <div class=\"row no-gutters\">
            <div class=\"col\">
                <div class=\"map\"></div>
            </div>
            <div class=\"col\">
                <div class=\"map-desc\">
                    <h2 class=\"section-title dark\">Countries Supported</h2>
                    <div class=\"section-description dark\">
                        <p>Vestibulum rutrum quam vitae fringilla tincidunt.
                            Suspendisse nec tortor urna. Ut laoreet sodales nisi,
                            quis iaculis nulla iaculis vitae. Donec sagittis faucibus
                            lacus eget blandit. Mauris vitae ultricies metus, at condimentum nulla.</p>
                        <p>Donec quis ornare lacus. Etiam gravida mollis tortor quis porttitor.
                            Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui.
                            In malesuada enim in dolor euismod, id commodo mi consectetur. Curabitur.</p>
                    </div>
                    <a href=\"#register\" class=\"button color-button big\">Register</a>
                </div>
            </div>
        </div>
    </section><!--.geo-countries-->
    <section class=\"geo-results\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col\">
                    <div class=\"geor-block\">
                        <div class=\"geor-image-wrapper\">
                            <div class=\"geor-image\"></div>
                            <h3 class=\"section-subtitle text-center position-absolute\">Guaranteed results</h3>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Aenean euismod bibendum laoreet. Proin gravida dolor sit
                            amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar
                            sic tempor. Sociis natoque penatibus et magnis dis parturient montes,
                            nascetur ridiculus mus.</p>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"geor-block\">
                        <div class=\"geor-image-wrapper\">
                            <div class=\"geor-image\"></div>
                            <h3 class=\"section-subtitle text-center position-absolute\">Google Adsense safe</h3>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Aenean euismod bibendum laoreet. Proin gravida dolor sit
                            amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar
                            sic tempor. Sociis natoque penatibus et magnis dis parturient montes,
                            nascetur ridiculus mus.</p>
                    </div>
                </div>
            </div>
            <div class=\"text-center\">
                <a href=\"#browse_packages\" class=\"button color-button\">Browse Paskages</a>
            </div>
        </div>
    </section><!--.geo-results-->
    <section class=\"economy-traffic-packages\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col text-center\">
                    <h2 class=\"section-title\">Economy Traffic Packages</h2>
                    <div class=\"section-description big gray\">
                        <p>You can have unlimited number of packages of different sizes linked to your account.<br>
                            * It is possible to purchase several packages and point them to the same URLs (10 x Large
                            will give you est. 6 Million hits per month).</p>
                    </div>
                </div>
            </div>
            <div class=\"row traffic-package-gallery\">
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Micro</div>
                        <div class=\"tp-price\">\$9.9</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 50k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Small</div>
                        <div class=\"tp-price\">\$29.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 200k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Medium</div>
                        <div class=\"tp-price\">\$59.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 500k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Large</div>
                        <div class=\"tp-price\">\$99.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 1M</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.economy-traffic-packages-->
    <section class=\"contact-us\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col\">
                    <h2 class=\"section-title text-center dark\">Contact Us</h2>
                    <form id=\"contact-us-form\">
                        <div class=\"row\">
                            <div class=\"col\">
                                <label for=\"visitor-name\">
                                    <span>Your name</span><br>
                                    <input type=\"text\" id=\"visitor-name\" name=\"visitor_name\">
                                </label>
                            </div>
                            <div class=\"col\">
                                <label for=\"visitor-email\">
                                    <span>Your E-mail</span><br>
                                    <input type=\"text\" id=\"visitor-email\" name=\"visitor_email\">
                                </label>
                            </div>
                            <div class=\"w-100\"></div>
                            <div class=\"col\">
                                <label for=\"visitor-message\">
                                    <span>Your message</span><br>
                                    <textarea id=\"visitor-message\" name=\"visitor_message\"
                                              placeholder=\"Write us something\"></textarea>
                                </label>
                                <div class=\"contact-form-description clerafix\">
                                    <p>You can always send us an email at <a href=\"mailto:support@produst_name.com\">support@{produst_name}.com</a>
                                    </p>
                                    <input type=\"submit\" class=\"button color-button big float-right\"
                                           value=\"Send Message\">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.contact-us-->
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "site/geo_targeting.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 4,  44 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'site/layouts/main.html.twig' %}

{% block content %}
    <section class=\"geo-targeting-header\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col text-center\">
                    <h1 class=\"section-title\">GEO Targeting</h1>
                </div>
            </div>
        </div>
    </section><!--.geo-targeting-header-->
    <section class=\"geo-features\">
        <div class=\"container\">
            <div class=\"row geo-feature-item left-image no-gutters\">
                <div class=\"col-5\">
                    <div class=\"geof-images\"></div>
                </div>
                <div class=\"col-7\">
                    <div class=\"geof-desc\">
                        <h2 class=\"geof-title\">Free Geo Targeting</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Ut pretium pretium tempor. Ut eget imperdiet neque.
                            In volutpat ante semper diam molestie, et aliquam erat laoreet.
                            Sed sit amet arcu aliquet,.</p>
                        <a href=\"#see_plans\" class=\"button color-button float-right\">See Plans</a>
                    </div>
                </div>
            </div>
            <div class=\"row geo-feature-item right-image no-gutters\">
                <div class=\"col-7\">
                    <div class=\"geof-desc\">
                        <h2 class=\"geof-title\">Local Ranking Improve</h2>
                        <p>Donec facilisis tortor ut augue lacinia,
                            at viverra est semper. Sed sapien metus,
                            scelerisque nec pharetra id, tempor a tortor.
                            Pellentesque non dignissim neque. Ut porta viverra est, ut dignissim elit.</p>
                        <a href=\"#learn_more\" class=\"button color-button\">Learn More</a>
                    </div>
                </div>
                <div class=\"col-5\">
                    <div class=\"geof-images float-right\"></div>
                </div>
            </div>
            <div class=\"row geo-feature-item left-image no-gutters\">
                <div class=\"col-5\">
                    <div class=\"geof-images\"></div>
                </div>
                <div class=\"col-7\">
                    <div class=\"geof-desc\">
                        <h2 class=\"geof-title\">Traffic from ISP’s</h2>
                        <p>Vestibulum rutrum quam vitae fringilla tincidunt.
                            Suspendisse nec tortor urna. Ut laoreet sodales nisi,
                            quis iaculis nulla iaculis vitae. Donec sagittis faucibus
                            lacus eget blandit. Mauris vitae ultricies me.</p>
                        <a href=\"#read_more\" class=\"button color-button float-right\">Read More</a>
                    </div>
                </div>
            </div>
            <div class=\"row geo-feature-item right-image no-gutters\">
                <div class=\"col-7\">
                    <div class=\"geof-desc\">
                        <h2 class=\"geof-title\">Cities Geo Targeting</h2v>
                            <p>Cras quis nulla commodo, aliquam lectus sed, blandit augue.
                                Cras ullamcorper bibendum bibendum. Duis tincidunt urna non pretium porta.
                                Nam condimentum vitae ligula vel ornare.</p>
                            <a href=\"#browse_packages\" class=\"button color-button\">Browse Packages</a>
                    </div>
                </div>
                <div class=\"col-5\">
                    <div class=\"geof-images float-right\"></div>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.geo-features-->
    <section class=\"geo-countries\">
        <div class=\"row no-gutters\">
            <div class=\"col\">
                <div class=\"map\"></div>
            </div>
            <div class=\"col\">
                <div class=\"map-desc\">
                    <h2 class=\"section-title dark\">Countries Supported</h2>
                    <div class=\"section-description dark\">
                        <p>Vestibulum rutrum quam vitae fringilla tincidunt.
                            Suspendisse nec tortor urna. Ut laoreet sodales nisi,
                            quis iaculis nulla iaculis vitae. Donec sagittis faucibus
                            lacus eget blandit. Mauris vitae ultricies metus, at condimentum nulla.</p>
                        <p>Donec quis ornare lacus. Etiam gravida mollis tortor quis porttitor.
                            Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui.
                            In malesuada enim in dolor euismod, id commodo mi consectetur. Curabitur.</p>
                    </div>
                    <a href=\"#register\" class=\"button color-button big\">Register</a>
                </div>
            </div>
        </div>
    </section><!--.geo-countries-->
    <section class=\"geo-results\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col\">
                    <div class=\"geor-block\">
                        <div class=\"geor-image-wrapper\">
                            <div class=\"geor-image\"></div>
                            <h3 class=\"section-subtitle text-center position-absolute\">Guaranteed results</h3>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Aenean euismod bibendum laoreet. Proin gravida dolor sit
                            amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar
                            sic tempor. Sociis natoque penatibus et magnis dis parturient montes,
                            nascetur ridiculus mus.</p>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"geor-block\">
                        <div class=\"geor-image-wrapper\">
                            <div class=\"geor-image\"></div>
                            <h3 class=\"section-subtitle text-center position-absolute\">Google Adsense safe</h3>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Aenean euismod bibendum laoreet. Proin gravida dolor sit
                            amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar
                            sic tempor. Sociis natoque penatibus et magnis dis parturient montes,
                            nascetur ridiculus mus.</p>
                    </div>
                </div>
            </div>
            <div class=\"text-center\">
                <a href=\"#browse_packages\" class=\"button color-button\">Browse Paskages</a>
            </div>
        </div>
    </section><!--.geo-results-->
    <section class=\"economy-traffic-packages\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col text-center\">
                    <h2 class=\"section-title\">Economy Traffic Packages</h2>
                    <div class=\"section-description big gray\">
                        <p>You can have unlimited number of packages of different sizes linked to your account.<br>
                            * It is possible to purchase several packages and point them to the same URLs (10 x Large
                            will give you est. 6 Million hits per month).</p>
                    </div>
                </div>
            </div>
            <div class=\"row traffic-package-gallery\">
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Micro</div>
                        <div class=\"tp-price\">\$9.9</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 50k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Small</div>
                        <div class=\"tp-price\">\$29.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 200k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Medium</div>
                        <div class=\"tp-price\">\$59.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 500k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Large</div>
                        <div class=\"tp-price\">\$99.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 1M</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.economy-traffic-packages-->
    <section class=\"contact-us\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col\">
                    <h2 class=\"section-title text-center dark\">Contact Us</h2>
                    <form id=\"contact-us-form\">
                        <div class=\"row\">
                            <div class=\"col\">
                                <label for=\"visitor-name\">
                                    <span>Your name</span><br>
                                    <input type=\"text\" id=\"visitor-name\" name=\"visitor_name\">
                                </label>
                            </div>
                            <div class=\"col\">
                                <label for=\"visitor-email\">
                                    <span>Your E-mail</span><br>
                                    <input type=\"text\" id=\"visitor-email\" name=\"visitor_email\">
                                </label>
                            </div>
                            <div class=\"w-100\"></div>
                            <div class=\"col\">
                                <label for=\"visitor-message\">
                                    <span>Your message</span><br>
                                    <textarea id=\"visitor-message\" name=\"visitor_message\"
                                              placeholder=\"Write us something\"></textarea>
                                </label>
                                <div class=\"contact-form-description clerafix\">
                                    <p>You can always send us an email at <a href=\"mailto:support@produst_name.com\">support@{produst_name}.com</a>
                                    </p>
                                    <input type=\"submit\" class=\"button color-button big float-right\"
                                           value=\"Send Message\">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.contact-us-->
{% endblock %}", "site/geo_targeting.html.twig", "/var/www/trafficbot.loc/templates/site/geo_targeting.html.twig");
    }
}
