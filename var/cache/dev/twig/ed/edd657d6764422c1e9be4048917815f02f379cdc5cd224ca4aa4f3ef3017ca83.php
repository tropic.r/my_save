<?php

/* admin/project/edit.html.twig */
class __TwigTemplate_2be48ec79513feffea350a46b45d729b543566a38661ea51194703bcdbacbbf1 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("admin/layouts/admin_left_menu.html.twig", "admin/project/edit.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layouts/admin_left_menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/project/edit.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/project/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 3
        echo "
    ";
        // line 4
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 4, $this->source); })()), 'form_start');
        echo "

    <div class=\"ibox-content m-b-sm border-bottom\">
        <div class=\"row\">
            <div class=\"col-lg-3 col-md-4 col-sm-6 col-xs-12\">
                <div class=\"form-group\">
                    ";
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 10, $this->source); })()), "name", array()), 'label');
        echo "
                    ";
        // line 11
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 11, $this->source); })()), "name", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                    </br>
                    ";
        // line 13
        if (twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "state", array(), "any", true, true)) {
            // line 14
            echo "                        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 14, $this->source); })()), "state", array()), 'label');
            echo "
                        ";
            // line 15
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 15, $this->source); })()), "state", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
                        </br>
                        ";
            // line 17
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 17, $this->source); })()), "save", array()), 'widget', array("attr" => array("class" => "btn btn-primary btn-sm")));
            echo "
                    ";
        } else {
            // line 19
            echo "                        </br>
                        ";
            // line 20
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 20, $this->source); })()), "add", array()), 'widget', array("attr" => array("class" => "btn btn-primary btn-sm")));
            echo "
                    ";
        }
        // line 22
        echo "                </div>
            </div>
        </div>
    </div>

    ";
        // line 27
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 27, $this->source); })()), 'form_end');
        echo "

    ";
        // line 29
        if (array_key_exists("flows", $context)) {
            // line 30
            echo "    <div class=\"row\">
        <div class=\"col-lg-12\">
            <div class=\"ibox\">
                <div class=\"ibox-content\">

                        ";
            // line 35
            if (twig_test_empty((isset($context["flows"]) || array_key_exists("flows", $context) ? $context["flows"] : (function () { throw new Twig_Error_Runtime('Variable "flows" does not exist.', 35, $this->source); })()))) {
                // line 36
                echo "
                            ";
                // line 37
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("No flows defined for this project yet"), "html", null, true);
                echo "
                            <a class=\"btn btn-primary btn-sm\" href=\"";
                // line 38
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("flow_add", array("projectId" => twig_get_attribute($this->env, $this->source, (isset($context["project"]) || array_key_exists("project", $context) ? $context["project"] : (function () { throw new Twig_Error_Runtime('Variable "project" does not exist.', 38, $this->source); })()), "id", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Add flow"), "html", null, true);
                echo "</a>

                        ";
            } else {
                // line 41
                echo "
                            <table class=\"footable table table-stripped toggle-arrow-tiny footable-loaded tablet breakpoint\" data-page-size=\"15\">
                                <thead>
                                <tr>
                                    <th data-toggle=\"true\" class=\"footable-visible footable-sortable footable-first-column\">Flows<span class=\"footable-sort-indicator\"></span></th>
                                    <th class=\"text-right footable-visible footable-last-column\" data-sort-ignore=\"true\">Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                ";
                // line 51
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["flows"]) || array_key_exists("flows", $context) ? $context["flows"] : (function () { throw new Twig_Error_Runtime('Variable "flows" does not exist.', 51, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["flow"]) {
                    // line 52
                    echo "
                                <tr class=\"footable-even\" style=\"\">
                                    <td class=\"footable-visible footable-first-column\"><span class=\"footable-toggle\"></span>
                                        <a class=\"state-";
                    // line 55
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["flow"], "state", array()), "html", null, true);
                    echo "\" href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("flow_edit", array("projectId" => twig_get_attribute($this->env, $this->source, (isset($context["project"]) || array_key_exists("project", $context) ? $context["project"] : (function () { throw new Twig_Error_Runtime('Variable "project" does not exist.', 55, $this->source); })()), "id", array()), "flowId" => twig_get_attribute($this->env, $this->source, $context["flow"], "id", array()))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["flow"], "name", array()), "html", null, true);
                    echo "</a>
                                    </td>
                                    <td class=\"text-right footable-visible footable-last-column\">
                                        <div class=\"btn-group\">
                                            <a class=\"btn-white btn btn-sm\" href=\"";
                    // line 59
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("flow_edit", array("projectId" => twig_get_attribute($this->env, $this->source, (isset($context["project"]) || array_key_exists("project", $context) ? $context["project"] : (function () { throw new Twig_Error_Runtime('Variable "project" does not exist.', 59, $this->source); })()), "id", array()), "flowId" => twig_get_attribute($this->env, $this->source, $context["flow"], "id", array()))), "html", null, true);
                    echo "\">Edit flow</a>
                                        </div>
                                    </td>
                                </tr>

                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flow'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 65
                echo "
                                <tr class=\"footable-even\" style=\"\">
                                    <td></td>
                                    <td class=\"text-right footable-visible footable-last-column\">
                                        <a class=\"btn btn-primary btn-sm\" href=\"";
                // line 69
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("flow_add", array("projectId" => twig_get_attribute($this->env, $this->source, (isset($context["project"]) || array_key_exists("project", $context) ? $context["project"] : (function () { throw new Twig_Error_Runtime('Variable "project" does not exist.', 69, $this->source); })()), "id", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Add flow"), "html", null, true);
                echo "</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        ";
            }
            // line 76
            echo "
                </div>
            </div>
        </div>
    </div>
    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/project/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  199 => 76,  187 => 69,  181 => 65,  169 => 59,  158 => 55,  153 => 52,  149 => 51,  137 => 41,  129 => 38,  125 => 37,  122 => 36,  120 => 35,  113 => 30,  111 => 29,  106 => 27,  99 => 22,  94 => 20,  91 => 19,  86 => 17,  81 => 15,  76 => 14,  74 => 13,  69 => 11,  65 => 10,  56 => 4,  53 => 3,  44 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'admin/layouts/admin_left_menu.html.twig' %}
{% block content %}

    {{ form_start(form) }}

    <div class=\"ibox-content m-b-sm border-bottom\">
        <div class=\"row\">
            <div class=\"col-lg-3 col-md-4 col-sm-6 col-xs-12\">
                <div class=\"form-group\">
                    {{ form_label(form.name) }}
                    {{ form_widget(form.name, {'attr': {'class': 'form-control'}}) }}
                    </br>
                    {% if form.state is defined %}
                        {{ form_label(form.state) }}
                        {{ form_widget(form.state, {'attr': {'class': 'form-control'}}) }}
                        </br>
                        {{ form_widget(form.save, {'attr': {'class': 'btn btn-primary btn-sm'}}) }}
                    {% else %}
                        </br>
                        {{ form_widget(form.add, {'attr': {'class': 'btn btn-primary btn-sm'}}) }}
                    {% endif %}
                </div>
            </div>
        </div>
    </div>

    {{ form_end(form) }}

    {% if flows is defined %}
    <div class=\"row\">
        <div class=\"col-lg-12\">
            <div class=\"ibox\">
                <div class=\"ibox-content\">

                        {% if flows is empty %}

                            {{ 'No flows defined for this project yet'|trans }}
                            <a class=\"btn btn-primary btn-sm\" href=\"{{ path('flow_add', { 'projectId': project.id }) }}\">{{ 'Add flow'|trans }}</a>

                        {% else %}

                            <table class=\"footable table table-stripped toggle-arrow-tiny footable-loaded tablet breakpoint\" data-page-size=\"15\">
                                <thead>
                                <tr>
                                    <th data-toggle=\"true\" class=\"footable-visible footable-sortable footable-first-column\">Flows<span class=\"footable-sort-indicator\"></span></th>
                                    <th class=\"text-right footable-visible footable-last-column\" data-sort-ignore=\"true\">Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                {% for flow in flows %}

                                <tr class=\"footable-even\" style=\"\">
                                    <td class=\"footable-visible footable-first-column\"><span class=\"footable-toggle\"></span>
                                        <a class=\"state-{{ flow.state }}\" href=\"{{ path('flow_edit', { 'projectId': project.id, 'flowId': flow.id }) }}\">{{ flow.name }}</a>
                                    </td>
                                    <td class=\"text-right footable-visible footable-last-column\">
                                        <div class=\"btn-group\">
                                            <a class=\"btn-white btn btn-sm\" href=\"{{ path('flow_edit', { 'projectId': project.id, 'flowId': flow.id }) }}\">Edit flow</a>
                                        </div>
                                    </td>
                                </tr>

                                {% endfor %}

                                <tr class=\"footable-even\" style=\"\">
                                    <td></td>
                                    <td class=\"text-right footable-visible footable-last-column\">
                                        <a class=\"btn btn-primary btn-sm\" href=\"{{ path('flow_add', { 'projectId': project.id }) }}\">{{ 'Add flow'|trans }}</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        {% endif %}

                </div>
            </div>
        </div>
    </div>
    {% endif %}
{% endblock %}






", "admin/project/edit.html.twig", "/var/www/trafficbot.loc/templates/admin/project/edit.html.twig");
    }
}
