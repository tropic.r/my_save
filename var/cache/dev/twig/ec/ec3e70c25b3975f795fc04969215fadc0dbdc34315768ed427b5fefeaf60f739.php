<?php

/* admin/layouts/admin_left_menu.html.twig */
class __TwigTemplate_9ef56f666b56ba09af7821a6d7878aee3636b20862a8704dde659c0f20d412f1 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("admin/layouts/base.html.twig", "admin/layouts/admin_left_menu.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'page_heading' => array($this, 'block_page_heading'),
            'content' => array($this, 'block_content'),
            'footer_right' => array($this, 'block_footer_right'),
            'footer_left' => array($this, 'block_footer_left'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layouts/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/layouts/admin_left_menu.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/layouts/admin_left_menu.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div id=\"wrapper\">
        <nav class=\"navbar-default navbar-static-side\" role=\"navigation\">
            <div class=\"sidebar-collapse\">
                <ul class=\"nav metismenu\" id=\"side-menu\">
                    <li class=\"nav-header\">
                        <div class=\"profile-element\">
                            &lt;TrafficBot logo&gt;
                        </div>
                        <div class=\"logo-element\">
                            TB
                        </div>
                    </li>
                    <li class=\"active\">
                        <a href=\"#\">
                            <i class=\"fa fa-th-large\"></i>
                            <span class=\"nav-label\">Projects</span>
                            <span class=\"fa arrow\"></span>
                        </a>
                        <ul class=\"nav nav-second-level\">
                            <li class=\"active\">
                                <a href=\"";
        // line 24
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("projects_list");
        echo "\">List</a>
                            </li>
                            <li>
                                <a href=\"";
        // line 27
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("project_add");
        echo "\">New Project</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href=\"#\">
                            <i class=\"fa fa-diamond\"></i>
                            <span class=\"nav-label\">Layouts</span>
                        </a>
                    </li>
                </ul>

            </div>
        </nav>

        <div id=\"page-wrapper\" class=\"gray-bg dashbard-1\">
            <div class=\"row border-bottom\">
                <nav class=\"navbar navbar-static-top\" role=\"navigation\" style=\"margin-bottom: 0\">
                    <div class=\"navbar-header\">
                        <a class=\"navbar-minimalize minimalize-styl-2 btn btn-primary \" href=\"#\">
                            <i class=\"fa fa-bars\"></i>
                        </a>
                    </div>
                    <ul class=\"nav navbar-top-links navbar-right\">
                        <li>
                            <span class=\"m-r-sm text-muted welcome-message\">Welcome to TrafficBot Admin.</span>
                        </li>

                        <li>
                            <a href=\"";
        // line 56
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("logout");
        echo "\">
                                <i class=\"fa fa-sign-out\"></i> Log out
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>

            ";
        // line 64
        $this->displayBlock('page_heading', $context, $blocks);
        // line 88
        echo "
            <div class=\"row\">
                <div class=\"col-lg-12\">
                    <div class=\"wrapper wrapper-content\">
                        ";
        // line 92
        $this->displayBlock('content', $context, $blocks);
        // line 93
        echo "                    </div>
                    <div class=\"footer\">
                        <div class=\"pull-right\">
                            ";
        // line 96
        $this->displayBlock('footer_right', $context, $blocks);
        // line 97
        echo "                        </div>
                        <div>
                            ";
        // line 99
        $this->displayBlock('footer_left', $context, $blocks);
        // line 102
        echo "                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 64
    public function block_page_heading($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_heading"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_heading"));

        // line 65
        echo "                <div class=\"row wrapper border-bottom white-bg page-heading\">
                    <div class=\"col-lg-10\">
                        ";
        // line 67
        if (array_key_exists("title", $context)) {
            // line 68
            echo "                            <h2>";
            echo twig_escape_filter($this->env, (isset($context["title"]) || array_key_exists("title", $context) ? $context["title"] : (function () { throw new Twig_Error_Runtime('Variable "title" does not exist.', 68, $this->source); })()), "html", null, true);
            echo "</h2>
                        ";
        }
        // line 70
        echo "                        ";
        if (array_key_exists("breadcrumb", $context)) {
            // line 71
            echo "                            <ol class=\"breadcrumb\">
                                ";
            // line 72
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumb"]) || array_key_exists("breadcrumb", $context) ? $context["breadcrumb"] : (function () { throw new Twig_Error_Runtime('Variable "breadcrumb" does not exist.', 72, $this->source); })()));
            $context['_iterated'] = false;
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 73
                echo "                                    <li>
                                        ";
                // line 74
                if (twig_get_attribute($this->env, $this->source, $context["loop"], "last", array())) {
                    // line 75
                    echo "                                            <strong>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "title", array()), "html", null, true);
                    echo "</strong>
                                        ";
                } else {
                    // line 77
                    echo "                                            <a href=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "url", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "title", array()), "html", null, true);
                    echo "</a>
                                        ";
                }
                // line 79
                echo "                                    </li>
                                ";
                $context['_iterated'] = true;
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            if (!$context['_iterated']) {
                // line 81
                echo "                                    <li></li>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 83
            echo "                            </ol>
                        ";
        }
        // line 85
        echo "                    </div>
                </div>
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 92
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 96
    public function block_footer_right($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer_right"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer_right"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 99
    public function block_footer_left($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer_left"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer_left"));

        // line 100
        echo "                                <strong>Copyright</strong> TrafficBot
                            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/layouts/admin_left_menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  311 => 100,  302 => 99,  285 => 96,  268 => 92,  256 => 85,  252 => 83,  245 => 81,  231 => 79,  223 => 77,  217 => 75,  215 => 74,  212 => 73,  194 => 72,  191 => 71,  188 => 70,  182 => 68,  180 => 67,  176 => 65,  167 => 64,  151 => 102,  149 => 99,  145 => 97,  143 => 96,  138 => 93,  136 => 92,  130 => 88,  128 => 64,  117 => 56,  85 => 27,  79 => 24,  57 => 4,  48 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'admin/layouts/base.html.twig' %}

{% block body %}
    <div id=\"wrapper\">
        <nav class=\"navbar-default navbar-static-side\" role=\"navigation\">
            <div class=\"sidebar-collapse\">
                <ul class=\"nav metismenu\" id=\"side-menu\">
                    <li class=\"nav-header\">
                        <div class=\"profile-element\">
                            &lt;TrafficBot logo&gt;
                        </div>
                        <div class=\"logo-element\">
                            TB
                        </div>
                    </li>
                    <li class=\"active\">
                        <a href=\"#\">
                            <i class=\"fa fa-th-large\"></i>
                            <span class=\"nav-label\">Projects</span>
                            <span class=\"fa arrow\"></span>
                        </a>
                        <ul class=\"nav nav-second-level\">
                            <li class=\"active\">
                                <a href=\"{{ path('projects_list') }}\">List</a>
                            </li>
                            <li>
                                <a href=\"{{ path('project_add') }}\">New Project</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href=\"#\">
                            <i class=\"fa fa-diamond\"></i>
                            <span class=\"nav-label\">Layouts</span>
                        </a>
                    </li>
                </ul>

            </div>
        </nav>

        <div id=\"page-wrapper\" class=\"gray-bg dashbard-1\">
            <div class=\"row border-bottom\">
                <nav class=\"navbar navbar-static-top\" role=\"navigation\" style=\"margin-bottom: 0\">
                    <div class=\"navbar-header\">
                        <a class=\"navbar-minimalize minimalize-styl-2 btn btn-primary \" href=\"#\">
                            <i class=\"fa fa-bars\"></i>
                        </a>
                    </div>
                    <ul class=\"nav navbar-top-links navbar-right\">
                        <li>
                            <span class=\"m-r-sm text-muted welcome-message\">Welcome to TrafficBot Admin.</span>
                        </li>

                        <li>
                            <a href=\"{{ path(\"logout\") }}\">
                                <i class=\"fa fa-sign-out\"></i> Log out
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>

            {% block page_heading %}
                <div class=\"row wrapper border-bottom white-bg page-heading\">
                    <div class=\"col-lg-10\">
                        {% if title is defined %}
                            <h2>{{ title }}</h2>
                        {% endif %}
                        {% if breadcrumb is defined %}
                            <ol class=\"breadcrumb\">
                                {% for item in breadcrumb %}
                                    <li>
                                        {% if loop.last %}
                                            <strong>{{ item.title }}</strong>
                                        {% else %}
                                            <a href=\"{{ item.url }}\">{{ item.title }}</a>
                                        {% endif %}
                                    </li>
                                {% else %}
                                    <li></li>
                                {% endfor %}
                            </ol>
                        {% endif %}
                    </div>
                </div>
            {% endblock %}

            <div class=\"row\">
                <div class=\"col-lg-12\">
                    <div class=\"wrapper wrapper-content\">
                        {% block content %}{% endblock %}
                    </div>
                    <div class=\"footer\">
                        <div class=\"pull-right\">
                            {% block footer_right %}{% endblock %}
                        </div>
                        <div>
                            {% block footer_left %}
                                <strong>Copyright</strong> TrafficBot
                            {% endblock %}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}", "admin/layouts/admin_left_menu.html.twig", "/var/www/trafficbot.loc/templates/admin/layouts/admin_left_menu.html.twig");
    }
}
