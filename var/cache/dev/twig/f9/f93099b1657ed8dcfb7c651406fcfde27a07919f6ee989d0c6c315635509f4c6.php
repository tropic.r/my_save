<?php

/* site/blog/list.html.twig */
class __TwigTemplate_b01315d2e113e361b15ca064a4bd9ff1ffb2cb2d9962e7e7e09f132719cd25df extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("site/layouts/blog.html.twig", "site/blog/list.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "site/layouts/blog.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "site/blog/list.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "site/blog/list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    <div class=\"main-container\">
        <div class=\"container\">
            <header class=\"blog-header text-center\">
                <h1 class=\"section-title page-title dark\">Get 3000 Page Views For Free!</h1>
                <div class=\"section-description big dark\">
                    <p>Feel short form below to get 3000 free page views to your website.</p>
                </div>
            </header>
            <div class=\"row\">
                <section class=\"col-8 main-loop\">
                    <article class=\"full-view\">
                        <h2 class=\"article-title\">
                            <a href=\"#\">This unusual cryptocurrency company made \$32,000 in 30 days — now it's moving to
                                Canada to avoid American regulation</a>
                        </h2>
                        <div class=\"article-info clearfix\">
                            <div class=\"author-info-wrapper\">
                                <div class=\"avatar-wrapper\">
                                    <img src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/avatar.png"), "html", null, true);
        echo "\" alt=\"author_avatar\"
                                         class=\"avatar\">
                                </div>
                                <div class=\"author-info\">
                                    <h4 class=\"author-name\">Carolyn Jacobs</h4>
                                    <time class=\"time\">8 min ago</time>
                                </div>
                            </div>
                            <div class=\"tag-wrapper\">
                                <span class=\"tag\">Tag</span>
                            </div>
                            <div class=\"comments-number social-number\">
                                <div class=\"number\">45</div>
                                <div class=\"text\">Comments</div>
                            </div>
                            <div class=\"shares-number social-number\">
                                <div class=\"number\">324</div>
                                <div class=\"text\">Shares</div>
                            </div>
                        </div>
                        <div class=\"article-image-wrapper\">
                            <div class=\"article-image\"
                                 style=\"background-image: url(";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/article1.png"), "html", null, true);
        echo ");\"></div>
                        </div>
                    </article>
                    <article class=\"full-view\">
                        <h2 class=\"article-title\">
                            <a href=\"#\">Meet the Clear Cut, a couple who's selling thousands of dollars worth of
                                diamonds through their Instagram DMs</a>
                        </h2>
                        <div class=\"article-info clearfix\">
                            <div class=\"author-info-wrapper\">
                                <div class=\"avatar-wrapper\">
                                    <img src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/avatar.png"), "html", null, true);
        echo "\" alt=\"author_avatar\"
                                         class=\"avatar\">
                                </div>
                                <div class=\"author-info\">
                                    <h4 class=\"author-name\">Carolyn Jacobs</h4>
                                    <time class=\"time\">8 min ago</time>
                                </div>
                            </div>
                            <div class=\"tag-wrapper\">
                                <span class=\"tag\">Tag</span>
                            </div>
                            <div class=\"comments-number social-number\">
                                <div class=\"number\">45</div>
                                <div class=\"text\">Comments</div>
                            </div>
                            <div class=\"shares-number social-number\">
                                <div class=\"number\">324</div>
                                <div class=\"text\">Shares</div>
                            </div>
                        </div>
                        <div class=\"article-image-wrapper\">
                            <div class=\"article-image\" style=\"background-image: url(";
        // line 76
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/article2.png"), "html", null, true);
        echo ");\"></div>
                        </div>
                    </article>
                    <article class=\"full-view\">
                        <h2 class=\"article-title\">
                            <a href=\"#\">There's one crucial, major innovation in Facebook's new VR headset that will set
                                a new precedent</a>
                        </h2>
                        <div class=\"article-info clearfix\">
                            <div class=\"author-info-wrapper\">
                                <div class=\"avatar-wrapper\">
                                    <img src=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/avatar.png"), "html", null, true);
        echo "\" alt=\"author_avatar\" class=\"avatar\">
                                </div>
                                <div class=\"author-info\">
                                    <h4 class=\"author-name\">Carolyn Jacobs</h4>
                                    <time class=\"time\">8 min ago</time>
                                </div>
                            </div>
                            <div class=\"tag-wrapper\">
                                <span class=\"tag\">Tag</span>
                            </div>
                            <div class=\"comments-number social-number\">
                                <div class=\"number\">45</div>
                                <div class=\"text\">Comments</div>
                            </div>
                            <div class=\"shares-number social-number\">
                                <div class=\"number\">324</div>
                                <div class=\"text\">Shares</div>
                            </div>
                        </div>
                        <div class=\"article-image-wrapper\">
                            <div class=\"article-image\" style=\"background-image: url(";
        // line 107
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/article3.png"), "html", null, true);
        echo ");\"></div>
                        </div>
                    </article>
                    <article class=\"shifted-view clearfix\">
                        <div class=\"article-image-wrapper\">
                            <div class=\"article-image\" style=\"background-image: url(";
        // line 112
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/article4.png"), "html", null, true);
        echo ");\"></div>
                        </div>
                        <div class=\"article-content-wrapper\">
                            <h2 class=\"article-title\">
                                <a href=\"#\">This 32-year-old tech CEO lives in a San Francisco home with 9 roommates,
                                    and it speaks to the length millennials will go to live in cities instead of
                                    suburbs</a>
                            </h2>
                            <div class=\"article-info clearfix\">
                                <div class=\"author-wrapper\">
                                    <div class=\"author-info\">
                                        <h4 class=\"author-name\">Carolyn Jacobs</h4>
                                        <time class=\"time\">8 min ago</time>
                                    </div>
                                </div>
                                <div class=\"shares-number social-number\">
                                    <div class=\"number\">324</div>
                                    <div class=\"text\">Shares</div>
                                </div>
                                <div class=\"comments-number social-number\">
                                    <div class=\"number\">45</div>
                                    <div class=\"text\">Comments</div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class=\"shifted-view clearfix\">
                        <div class=\"article-image-wrapper\">
                            <div class=\"article-image\" style=\"background-image: url(";
        // line 140
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/article5.png"), "html", null, true);
        echo ");\"></div>
                        </div>
                        <div class=\"article-content-wrapper\">
                            <h2 class=\"article-title\">
                                <a href=\"#\">San Francisco is so expensive that people are spending \$1 million to live
                                    next to a former nuclear-testing site</a>
                            </h2>
                            <div class=\"article-info clearfix\">
                                <div class=\"author-wrapper\">
                                    <div class=\"author-info\">
                                        <h4 class=\"author-name\">Carolyn Jacobs</h4>
                                        <time class=\"time\">8 min ago</time>
                                    </div>
                                </div>
                                <div class=\"shares-number social-number\">
                                    <div class=\"number\">324</div>
                                    <div class=\"text\">Shares</div>
                                </div>
                                <div class=\"comments-number social-number\">
                                    <div class=\"number\">45</div>
                                    <div class=\"text\">Comments</div>
                                </div>
                            </div>
                        </div>
                    </article>
                </section><!--. main-loop-->
                <aside class=\"col-4 sidebar\">
                    <div class=\"widgets-container\">
                        <div class=\"recommended-widget widget\">
                            <div class=\"widget-title\">Recommended For You</div>
                            <div class=\"widget-loop\">
                                <div class=\"widget-article\">
                                    <div class=\"wa-image-wrapper\">
                                        <div class=\"wa-image\"
                                             style=\"background-image: url(";
        // line 174
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/wa-image.png"), "html", null, true);
        echo ");\"></div>
                                    </div>
                                    <h4 class=\"wa-title\">
                                        <a href=\"#\">9 reasons you should buy an iPhone 8 instead of an iPhone X</a>
                                    </h4>
                                </div>
                                <div class=\"widget-article\">
                                    <div class=\"wa-image-wrapper\">
                                        <div class=\"wa-image\"
                                             style=\"background-image: url(";
        // line 183
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/wa-image.png"), "html", null, true);
        echo ");\"></div>
                                    </div>
                                    <h4 class=\"wa-title\">
                                        <a href=\"#\">9 reasons you should buy an iPhone 8 instead of an iPhone X</a>
                                    </h4>
                                </div>
                                <div class=\"widget-article\">
                                    <div class=\"wa-image-wrapper\">
                                        <div class=\"wa-image\"
                                             style=\"background-image: url(";
        // line 192
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/wa-image.png"), "html", null, true);
        echo ");\"></div>
                                    </div>
                                    <h4 class=\"wa-title\">
                                        <a href=\"#\">9 reasons you should buy an iPhone 8 instead of an iPhone X</a>
                                    </h4>
                                </div>
                            </div>
                        </div><!--.recommended-widget-->
                        <div class=\"video-widget widget\">
                            <div class=\"widget-title\">Videos You May Like</div>
                            <div class=\"widget-loop-tiles\">
                                <div class=\"row\">
                                    <div class=\"col\">
                                        <div class=\"widget-article\">
                                            <div class=\"wa-image-wrapper\">
                                                <div class=\"wa-image\"
                                                     style=\"background-image: url(";
        // line 208
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/wa-image1.png"), "html", null, true);
        echo ");\"></div>
                                            </div>
                                            <h4 class=\"wa-title\">
                                                <a href=\"#\">These 3D printed homes can be constructed for \$4,000 — and
                                                    they</a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class=\"col\">
                                        <div class=\"widget-article\">
                                            <div class=\"wa-image-wrapper\">
                                                <div class=\"wa-image\"
                                                     style=\"background-image: url(";
        // line 220
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/wa-image2.png"), "html", null, true);
        echo ");\"></div>
                                            </div>
                                            <h4 class=\"wa-title\">
                                                <a href=\"#\">A self-made millionaire describes the financial mistakes to
                                                    avoid</a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class=\"w-100\"></div>
                                    <div class=\"col\">
                                        <div class=\"widget-article\">
                                            <div class=\"wa-image-wrapper\">
                                                <div class=\"wa-image\"
                                                     style=\"background-image: url(";
        // line 233
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/wa-image3.png"), "html", null, true);
        echo ");\"></div>
                                            </div>
                                            <h4 class=\"wa-title\">
                                                <a href=\"#\">These 3D printed homes can be constructed for \$4,000 — and
                                                    they</a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class=\"col\">
                                        <div class=\"widget-article\">
                                            <div class=\"wa-image-wrapper\">
                                                <div class=\"wa-image\"
                                                     style=\"background-image: url(";
        // line 245
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/wa-image4.png"), "html", null, true);
        echo ");\"></div>
                                            </div>
                                            <h4 class=\"wa-title\">
                                                <a href=\"#\">A self-made millionaire describes the financial mistakes to
                                                    avoid</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--.video-widget-->
                        <div class=\"emails-widget widget\">
                            <div class=\"widget-title\">{ProjectName} Emails & Alerts</div>
                            <div class=\"widget-text\"><p>Get the best of Business Insider delivered to your inbox every
                                    day.</p></div>
                            <a href=\"";
        // line 260
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_register");
        echo "\" class=\"button color-button big\">Signup</a>
                        </div><!--.emails-widget-->
                        <div class=\"featured-widget widget\">
                            <div class=\"widget-title\">Featured</div>
                            <div class=\"widget-loop\">
                                <div class=\"widget-article\">
                                    <div class=\"wa-image-wrapper\">
                                        <div class=\"wa-image\"
                                             style=\"background-image: url(";
        // line 268
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/wa-image.png"), "html", null, true);
        echo ");\"></div>
                                    </div>
                                    <h4 class=\"wa-title\">
                                        <a href=\"#\">9 reasons you should buy an iPhone 8 instead of an iPhone X</a>
                                    </h4>
                                </div>
                                <div class=\"widget-article\">
                                    <div class=\"wa-image-wrapper\">
                                        <div class=\"wa-image\"
                                             style=\"background-image: url(";
        // line 277
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/wa-image.png"), "html", null, true);
        echo ");\"></div>
                                    </div>
                                    <h4 class=\"wa-title\">
                                        <a href=\"#\">9 reasons you should buy an iPhone 8 instead of an iPhone X</a>
                                    </h4>
                                </div>
                                <div class=\"widget-article\">
                                    <div class=\"wa-image-wrapper\">
                                        <div class=\"wa-image\"
                                             style=\"background-image: url(";
        // line 286
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/wa-image.png"), "html", null, true);
        echo ");\"></div>
                                    </div>
                                    <h4 class=\"wa-title\">
                                        <a href=\"#\">9 reasons you should buy an iPhone 8 instead of an iPhone X</a>
                                    </h4>
                                </div>
                            </div>
                        </div><!--.featured-widget-->
                        <div class=\"image-widget widget\">
                            <div class=\"widget-title\">{ProjectName} Intelligence Exclusive on Artificial Intelligence
                            </div>
                            <div class=\"widget-image-container\">
                                <img class=\"widget-image\" src=\"";
        // line 298
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/widget-image.png"), "html", null, true);
        echo "\" alt=\"\">
                                <a class=\"overlay-link\" href=\"#\">&nbsp;</a>
                            </div>
                        </div>
                    </div>
                </aside><!--.sidebar-->
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "site/blog/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  406 => 298,  391 => 286,  379 => 277,  367 => 268,  356 => 260,  338 => 245,  323 => 233,  307 => 220,  292 => 208,  273 => 192,  261 => 183,  249 => 174,  212 => 140,  181 => 112,  173 => 107,  150 => 87,  136 => 76,  112 => 55,  98 => 44,  73 => 22,  53 => 4,  44 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'site/layouts/blog.html.twig' %}

{% block content %}
    <div class=\"main-container\">
        <div class=\"container\">
            <header class=\"blog-header text-center\">
                <h1 class=\"section-title page-title dark\">Get 3000 Page Views For Free!</h1>
                <div class=\"section-description big dark\">
                    <p>Feel short form below to get 3000 free page views to your website.</p>
                </div>
            </header>
            <div class=\"row\">
                <section class=\"col-8 main-loop\">
                    <article class=\"full-view\">
                        <h2 class=\"article-title\">
                            <a href=\"#\">This unusual cryptocurrency company made \$32,000 in 30 days — now it's moving to
                                Canada to avoid American regulation</a>
                        </h2>
                        <div class=\"article-info clearfix\">
                            <div class=\"author-info-wrapper\">
                                <div class=\"avatar-wrapper\">
                                    <img src=\"{{ asset('s/build/site/img/avatar.png') }}\" alt=\"author_avatar\"
                                         class=\"avatar\">
                                </div>
                                <div class=\"author-info\">
                                    <h4 class=\"author-name\">Carolyn Jacobs</h4>
                                    <time class=\"time\">8 min ago</time>
                                </div>
                            </div>
                            <div class=\"tag-wrapper\">
                                <span class=\"tag\">Tag</span>
                            </div>
                            <div class=\"comments-number social-number\">
                                <div class=\"number\">45</div>
                                <div class=\"text\">Comments</div>
                            </div>
                            <div class=\"shares-number social-number\">
                                <div class=\"number\">324</div>
                                <div class=\"text\">Shares</div>
                            </div>
                        </div>
                        <div class=\"article-image-wrapper\">
                            <div class=\"article-image\"
                                 style=\"background-image: url({{ asset('s/build/site/img/article1.png') }});\"></div>
                        </div>
                    </article>
                    <article class=\"full-view\">
                        <h2 class=\"article-title\">
                            <a href=\"#\">Meet the Clear Cut, a couple who's selling thousands of dollars worth of
                                diamonds through their Instagram DMs</a>
                        </h2>
                        <div class=\"article-info clearfix\">
                            <div class=\"author-info-wrapper\">
                                <div class=\"avatar-wrapper\">
                                    <img src=\"{{ asset('s/build/site/img/avatar.png') }}\" alt=\"author_avatar\"
                                         class=\"avatar\">
                                </div>
                                <div class=\"author-info\">
                                    <h4 class=\"author-name\">Carolyn Jacobs</h4>
                                    <time class=\"time\">8 min ago</time>
                                </div>
                            </div>
                            <div class=\"tag-wrapper\">
                                <span class=\"tag\">Tag</span>
                            </div>
                            <div class=\"comments-number social-number\">
                                <div class=\"number\">45</div>
                                <div class=\"text\">Comments</div>
                            </div>
                            <div class=\"shares-number social-number\">
                                <div class=\"number\">324</div>
                                <div class=\"text\">Shares</div>
                            </div>
                        </div>
                        <div class=\"article-image-wrapper\">
                            <div class=\"article-image\" style=\"background-image: url({{ asset('s/build/site/img/article2.png') }});\"></div>
                        </div>
                    </article>
                    <article class=\"full-view\">
                        <h2 class=\"article-title\">
                            <a href=\"#\">There's one crucial, major innovation in Facebook's new VR headset that will set
                                a new precedent</a>
                        </h2>
                        <div class=\"article-info clearfix\">
                            <div class=\"author-info-wrapper\">
                                <div class=\"avatar-wrapper\">
                                    <img src=\"{{ asset('s/build/site/img/avatar.png') }}\" alt=\"author_avatar\" class=\"avatar\">
                                </div>
                                <div class=\"author-info\">
                                    <h4 class=\"author-name\">Carolyn Jacobs</h4>
                                    <time class=\"time\">8 min ago</time>
                                </div>
                            </div>
                            <div class=\"tag-wrapper\">
                                <span class=\"tag\">Tag</span>
                            </div>
                            <div class=\"comments-number social-number\">
                                <div class=\"number\">45</div>
                                <div class=\"text\">Comments</div>
                            </div>
                            <div class=\"shares-number social-number\">
                                <div class=\"number\">324</div>
                                <div class=\"text\">Shares</div>
                            </div>
                        </div>
                        <div class=\"article-image-wrapper\">
                            <div class=\"article-image\" style=\"background-image: url({{ asset('s/build/site/img/article3.png') }});\"></div>
                        </div>
                    </article>
                    <article class=\"shifted-view clearfix\">
                        <div class=\"article-image-wrapper\">
                            <div class=\"article-image\" style=\"background-image: url({{ asset('s/build/site/img/article4.png') }});\"></div>
                        </div>
                        <div class=\"article-content-wrapper\">
                            <h2 class=\"article-title\">
                                <a href=\"#\">This 32-year-old tech CEO lives in a San Francisco home with 9 roommates,
                                    and it speaks to the length millennials will go to live in cities instead of
                                    suburbs</a>
                            </h2>
                            <div class=\"article-info clearfix\">
                                <div class=\"author-wrapper\">
                                    <div class=\"author-info\">
                                        <h4 class=\"author-name\">Carolyn Jacobs</h4>
                                        <time class=\"time\">8 min ago</time>
                                    </div>
                                </div>
                                <div class=\"shares-number social-number\">
                                    <div class=\"number\">324</div>
                                    <div class=\"text\">Shares</div>
                                </div>
                                <div class=\"comments-number social-number\">
                                    <div class=\"number\">45</div>
                                    <div class=\"text\">Comments</div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class=\"shifted-view clearfix\">
                        <div class=\"article-image-wrapper\">
                            <div class=\"article-image\" style=\"background-image: url({{ asset('s/build/site/img/article5.png') }});\"></div>
                        </div>
                        <div class=\"article-content-wrapper\">
                            <h2 class=\"article-title\">
                                <a href=\"#\">San Francisco is so expensive that people are spending \$1 million to live
                                    next to a former nuclear-testing site</a>
                            </h2>
                            <div class=\"article-info clearfix\">
                                <div class=\"author-wrapper\">
                                    <div class=\"author-info\">
                                        <h4 class=\"author-name\">Carolyn Jacobs</h4>
                                        <time class=\"time\">8 min ago</time>
                                    </div>
                                </div>
                                <div class=\"shares-number social-number\">
                                    <div class=\"number\">324</div>
                                    <div class=\"text\">Shares</div>
                                </div>
                                <div class=\"comments-number social-number\">
                                    <div class=\"number\">45</div>
                                    <div class=\"text\">Comments</div>
                                </div>
                            </div>
                        </div>
                    </article>
                </section><!--. main-loop-->
                <aside class=\"col-4 sidebar\">
                    <div class=\"widgets-container\">
                        <div class=\"recommended-widget widget\">
                            <div class=\"widget-title\">Recommended For You</div>
                            <div class=\"widget-loop\">
                                <div class=\"widget-article\">
                                    <div class=\"wa-image-wrapper\">
                                        <div class=\"wa-image\"
                                             style=\"background-image: url({{ asset('s/build/site/img/wa-image.png') }});\"></div>
                                    </div>
                                    <h4 class=\"wa-title\">
                                        <a href=\"#\">9 reasons you should buy an iPhone 8 instead of an iPhone X</a>
                                    </h4>
                                </div>
                                <div class=\"widget-article\">
                                    <div class=\"wa-image-wrapper\">
                                        <div class=\"wa-image\"
                                             style=\"background-image: url({{ asset('s/build/site/img/wa-image.png') }});\"></div>
                                    </div>
                                    <h4 class=\"wa-title\">
                                        <a href=\"#\">9 reasons you should buy an iPhone 8 instead of an iPhone X</a>
                                    </h4>
                                </div>
                                <div class=\"widget-article\">
                                    <div class=\"wa-image-wrapper\">
                                        <div class=\"wa-image\"
                                             style=\"background-image: url({{ asset('s/build/site/img/wa-image.png') }});\"></div>
                                    </div>
                                    <h4 class=\"wa-title\">
                                        <a href=\"#\">9 reasons you should buy an iPhone 8 instead of an iPhone X</a>
                                    </h4>
                                </div>
                            </div>
                        </div><!--.recommended-widget-->
                        <div class=\"video-widget widget\">
                            <div class=\"widget-title\">Videos You May Like</div>
                            <div class=\"widget-loop-tiles\">
                                <div class=\"row\">
                                    <div class=\"col\">
                                        <div class=\"widget-article\">
                                            <div class=\"wa-image-wrapper\">
                                                <div class=\"wa-image\"
                                                     style=\"background-image: url({{ asset('s/build/site/img/wa-image1.png') }});\"></div>
                                            </div>
                                            <h4 class=\"wa-title\">
                                                <a href=\"#\">These 3D printed homes can be constructed for \$4,000 — and
                                                    they</a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class=\"col\">
                                        <div class=\"widget-article\">
                                            <div class=\"wa-image-wrapper\">
                                                <div class=\"wa-image\"
                                                     style=\"background-image: url({{ asset('s/build/site/img/wa-image2.png') }});\"></div>
                                            </div>
                                            <h4 class=\"wa-title\">
                                                <a href=\"#\">A self-made millionaire describes the financial mistakes to
                                                    avoid</a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class=\"w-100\"></div>
                                    <div class=\"col\">
                                        <div class=\"widget-article\">
                                            <div class=\"wa-image-wrapper\">
                                                <div class=\"wa-image\"
                                                     style=\"background-image: url({{ asset('s/build/site/img/wa-image3.png') }});\"></div>
                                            </div>
                                            <h4 class=\"wa-title\">
                                                <a href=\"#\">These 3D printed homes can be constructed for \$4,000 — and
                                                    they</a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class=\"col\">
                                        <div class=\"widget-article\">
                                            <div class=\"wa-image-wrapper\">
                                                <div class=\"wa-image\"
                                                     style=\"background-image: url({{ asset('s/build/site/img/wa-image4.png') }});\"></div>
                                            </div>
                                            <h4 class=\"wa-title\">
                                                <a href=\"#\">A self-made millionaire describes the financial mistakes to
                                                    avoid</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--.video-widget-->
                        <div class=\"emails-widget widget\">
                            <div class=\"widget-title\">{ProjectName} Emails & Alerts</div>
                            <div class=\"widget-text\"><p>Get the best of Business Insider delivered to your inbox every
                                    day.</p></div>
                            <a href=\"{{ path('user_register') }}\" class=\"button color-button big\">Signup</a>
                        </div><!--.emails-widget-->
                        <div class=\"featured-widget widget\">
                            <div class=\"widget-title\">Featured</div>
                            <div class=\"widget-loop\">
                                <div class=\"widget-article\">
                                    <div class=\"wa-image-wrapper\">
                                        <div class=\"wa-image\"
                                             style=\"background-image: url({{ asset('s/build/site/img/wa-image.png') }});\"></div>
                                    </div>
                                    <h4 class=\"wa-title\">
                                        <a href=\"#\">9 reasons you should buy an iPhone 8 instead of an iPhone X</a>
                                    </h4>
                                </div>
                                <div class=\"widget-article\">
                                    <div class=\"wa-image-wrapper\">
                                        <div class=\"wa-image\"
                                             style=\"background-image: url({{ asset('s/build/site/img/wa-image.png') }});\"></div>
                                    </div>
                                    <h4 class=\"wa-title\">
                                        <a href=\"#\">9 reasons you should buy an iPhone 8 instead of an iPhone X</a>
                                    </h4>
                                </div>
                                <div class=\"widget-article\">
                                    <div class=\"wa-image-wrapper\">
                                        <div class=\"wa-image\"
                                             style=\"background-image: url({{ asset('s/build/site/img/wa-image.png') }});\"></div>
                                    </div>
                                    <h4 class=\"wa-title\">
                                        <a href=\"#\">9 reasons you should buy an iPhone 8 instead of an iPhone X</a>
                                    </h4>
                                </div>
                            </div>
                        </div><!--.featured-widget-->
                        <div class=\"image-widget widget\">
                            <div class=\"widget-title\">{ProjectName} Intelligence Exclusive on Artificial Intelligence
                            </div>
                            <div class=\"widget-image-container\">
                                <img class=\"widget-image\" src=\"{{ asset('s/build/site/img/widget-image.png') }}\" alt=\"\">
                                <a class=\"overlay-link\" href=\"#\">&nbsp;</a>
                            </div>
                        </div>
                    </div>
                </aside><!--.sidebar-->
            </div>
        </div>
    </div>
{% endblock %}", "site/blog/list.html.twig", "/var/www/trafficbot.loc/templates/site/blog/list.html.twig");
    }
}
