<?php

/* site/index.html.twig */
class __TwigTemplate_8d40da16c29df58ac62638f392a52a0227a49b64c06679457fc53cffec88c813 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("site/layouts/main.html.twig", "site/index.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "site/layouts/main.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "site/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "site/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    <section class=\"header-info\">
        <div class=\"container\">
            <h1 class=\"page-title section-title\">
                <p>
                    <span>Buy website traffic at the best prices on the Internet at <i>{product_name}</i></span>
                </p>
            </h1>
            <div class=\"clearfix\"></div>
            <div class=\"button-wrapper\">
                <a href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("free_trial");
        echo "\" class=\"button color-button big float-right\">Start free trial</a>
            </div>
            <div class=\"clearfix\"></div>
        </div>
    </section><!--.header-info-->
    <section class=\"how-it-works\">
        <div class=\"container\">
            <h2 class=\"section-title text-center\">How it Works</h2>
            <div class=\"row\">
                <div class=\"col\">
                    <div class=\"section-description\">
                        <p>Our platform generates traffic, which is by behavior very similar to what real
                            users do - surfing pages, click links on your website, download tracking scripts,
                            submit forms, etc. We use real browsers, so any scripts on your site are executed
                            (you can also exclude some from downloading to comply with 3d party terms).
                            You are very flexible to setup visitor flows - what pages users should reach first,
                            what next, how traffic should be redistributed across the day, from what locations traffic
                            should go. You can customize a lot of metrics like bounce rate, time on page, returning
                            visitors,
                            traffic sources, % of mobile traffic and a lot more. It is possible to set up multiple flows
                            to get traffic
                            looks more naturally</p>
                    </div>
                </div>
                <div class=\"col text-center\">
                    <img class=\"how-it-works-back\" src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/how-it-works.png"), "html", null, true);
        echo "\"
                         alt=\"how-it-works\">
                </div>
            </div><!--.row-->
            <div class=\"text-center\">
                <a href=\"";
        // line 43
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("how_it_works");
        echo "\"
                   class=\"how-it-works-read button color-button big position-relative\">Read More</a>
            </div>
        </div>
    </section><!--.how-it-works-->
    <section class=\"traffic\">
        <div class=\"container\">
            <div class=\"row traffic-gallery\">
                <div class=\"col\">
                    <div class=\"traffic-gallery-item gallery-item text-center\">
                        <div class=\"tsg-image\"
                             style=\"background-image: url(";
        // line 54
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/purchase-traffic.png"), "html", null, true);
        echo "); top: -18px;\"></div>
                        <h3 class=\"tsg-title section-subtitle\">Purchase Traffic</h3>
                        <p>We deliver high quality traffic to meet all your needs.
                            If you are after big numbers, we can deliver millions of visits per month.
                            Need conversions? We can guarantee real, 100% human visitors sent to your website.</p>
                        <div class=\"button-wrapper\">
                            <a href=\"#browse_packages\" class=\"button color-button\">Browse Packages</a>
                        </div>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"traffic-gallery-item gallery-item text-center\">
                        <div class=\"tsg-image\"
                             style=\"background-image: url(";
        // line 67
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/geo-targeting.png"), "html", null, true);
        echo "); top: -8px;\"></div>
                        <h3 class=\"tsg-title section-subtitle section-subtitle\">Geo-Targeting</h3>
                        <p>After creating a project you can choose what country's traffic you would like to receive.
                            Also there is an option to get North American, South American, Asian or European
                            traffic.</p>
                        <div class=\"button-wrapper\">
                            <a href=\"";
        // line 73
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("geo_targeting");
        echo "\" class=\"button color-button\">Learn More</a>
                        </div>
                    </div>
                </div>
                <div class=\"w-100\"></div>
                <div class=\"col\">
                    <div class=\"traffic-gallery-item gallery-item text-center\">
                        <div class=\"tsg-image\"
                             style=\"background-image: url(";
        // line 81
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/traffic-from-isps.png"), "html", null, true);
        echo "); top: 15px;\"></div>
                        <h3 class=\"tsg-title section-subtitle\">Traffic From ISP’s</h3>
                        <p>We deliver high quality traffic to meet all your needs. If you are after big numbers,
                            we can deliver millions of visits per month. Need conversions? We can guarantee real,
                            100% human visitors sent to your website.</p>
                        <div class=\"button-wrapper\">
                            <a href=\"#read_more\" class=\"button color-button\">Read More</a>
                        </div>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"traffic-gallery-item gallery-item text-center\">
                        <div class=\"tsg-image\"
                             style=\"background-image: url(";
        // line 94
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/alexa-ranking-traffic.png"), "html", null, true);
        echo "); top: 4px\"></div>
                        <h3 class=\"tsg-title section-subtitle\">Alexa Ranking Traffic</h3>
                        <p>We deliver high quality traffic to meet all your needs. If you are after big numbers,
                            we can deliver millions of visits per month. Need conversions? We can guarantee real,
                            100% human visitors sent to your website.</p>
                        <div class=\"button-wrapper\">
                            <a href=\"";
        // line 100
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pricing");
        echo "\" class=\"button color-button\">See plans</a>
                        </div>
                    </div>
                </div>
            </div><!--.row.traffic-gallery-->
        </div><!--.container-->
    </section><!--.traffic--->
    <section class=\"watch-in-action\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col\">
                    <h2 class=\"section-title text-center\">Watch <i>{productName}</i> in action</h2>
                    <div class=\"section-description text-center\">
                        <p>Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui.
                            In malesuada enim in dolor euismod, id commodo mi consectetur.
                            Curabitur at vestibulum nisi. Nullam vehicula nisi velit. Mauris turpis nisl,
                            molestie ut ipsum et, suscipit vehicula odio.
                            Vestibulum interdum vestibulum felis ac molestie.</p>
                    </div>
                </div>
            </div>
            <div class=\"watch-in-action-gallery\">
                <section class=\"mbr-gallery mbr-slider-carousel cid-qLs0svQVOC\" id=\"gallery1-2\">
                    <div>
                        <div class=\"mbr-gallery-row\">
                            <div class=\"mbr-gallery-layout-default\">
                                <div class=\"container\">
                                    <div class=\"row\">
                                        <div class=\"col-6 mbr-gallery-item mbr-gallery-item--p1\" data-video-url=\"false\"
                                             data-tags=\"Awesome\">
                                            <div href=\"#lb-gallery1-2\" data-slide-to=\"0\" data-toggle=\"modal\">
                                                <img src=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/gallery00.jpg"), "html", null, true);
        echo "\" alt=\"\"
                                                     title=\"\">
                                                <span class=\"icon-focus\"></span>
                                                <span class=\"mbr-gallery-title mbr-fonts-style display-7\">Test new caption</span>
                                            </div>
                                        </div>
                                        <!--there should not be spaces, \\n, EOH, in this place for, that the slider worked well -->
                                        <div class=\"col-6 mbr-gallery-item mbr-gallery-item--p1\" data-video-url=\"false\"
                                             data-tags=\"Responsive\">
                                            <div href=\"#lb-gallery1-2\" data-slide-to=\"1\" data-toggle=\"modal\">
                                                <img src=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/gallery01.jpg"), "html", null, true);
        echo "\" alt=\"\"
                                                     title=\"\">
                                                <span class=\"icon-focus\"></span>
                                                <span class=\"mbr-gallery-title mbr-fonts-style display-7\">Type caption here</span>
                                            </div>
                                        </div>
                                        <div class=\"col-6 mbr-gallery-item mbr-gallery-item--p1\" data-video-url=\"false\"
                                             data-tags=\"Creative\">
                                            <div href=\"#lb-gallery1-2\" data-slide-to=\"2\" data-toggle=\"modal\">
                                                <img src=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/gallery02.jpg"), "html", null, true);
        echo "\" alt=\"\"
                                                     title=\"\">
                                                <span class=\"icon-focus\"></span>
                                                <span class=\"mbr-gallery-title mbr-fonts-style display-7\">Type caption here</span>
                                            </div>
                                        </div>
                                        <div class=\"col-6 mbr-gallery-item mbr-gallery-item--p1\" data-video-url=\"false\"
                                             data-tags=\"Animated\">
                                            <div href=\"#lb-gallery1-2\" data-slide-to=\"3\" data-toggle=\"modal\">
                                                <img src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/gallery03.jpg"), "html", null, true);
        echo "\" alt=\"\"
                                                     title=\"\">
                                                <span class=\"icon-focus\"></span>
                                                <span class=\"mbr-gallery-title mbr-fonts-style display-7\">Type caption here</span>
                                            </div>
                                        </div>
                                        <div class=\"col-6 mbr-gallery-item mbr-gallery-item--p1\" data-video-url=\"false\"
                                             data-tags=\"Awesome\">
                                            <div href=\"#lb-gallery1-2\" data-slide-to=\"4\" data-toggle=\"modal\">
                                                <img src=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/gallery04.jpg"), "html", null, true);
        echo "\" alt=\"\"
                                                     title=\"\">
                                                <span class=\"icon-focus\"></span>
                                                <span class=\"mbr-gallery-title mbr-fonts-style display-7\">Type caption here</span>
                                            </div>
                                        </div>
                                        <div class=\"col-6 mbr-gallery-item mbr-gallery-item--p1\" data-video-url=\"false\"
                                             data-tags=\"Awesome\">
                                            <div href=\"#lb-gallery1-2\" data-slide-to=\"5\" data-toggle=\"modal\">
                                                <img src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/gallery05.jpg"), "html", null, true);
        echo "\" alt=\"\"
                                                     title=\"\">
                                                <span class=\"icon-focus\"></span>
                                                <span class=\"mbr-gallery-title mbr-fonts-style display-7\">Type caption here</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"clearfix\"></div>
                            </div>
                        </div><!-- Filter --><!-- Gallery -->
                        <div data-app-prevent-settings=\"\" class=\"mbr-slider modal fade carousel slide\" tabindex=\"-1\"
                             data-keyboard=\"true\" data-interval=\"false\" id=\"lb-gallery1-2\">
                            <div class=\"modal-dialog\">
                                <div class=\"modal-content\">
                                    <div class=\"modal-body\">
                                        <div class=\"carousel-inner\">
                                            <div class=\"carousel-item active\">
                                                <img src=\"";
        // line 195
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/gallery00.jpg"), "html", null, true);
        echo "\" alt=\"\"
                                                     title=\"\">
                                            </div>
                                            <div class=\"carousel-item\">
                                                <img src=\"";
        // line 199
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/gallery01.jpg"), "html", null, true);
        echo "\" alt=\"\"
                                                     title=\"\">
                                            </div>
                                            <div class=\"carousel-item\">
                                                <img src=\"";
        // line 203
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/gallery02.jpg"), "html", null, true);
        echo "\" alt=\"\"
                                                     title=\"\">
                                            </div>
                                            <div class=\"carousel-item\">
                                                <img src=\"";
        // line 207
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/gallery03.jpg"), "html", null, true);
        echo "\" alt=\"\"
                                                     title=\"\">
                                            </div>
                                            <div class=\"carousel-item\">
                                                <img src=\"";
        // line 211
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/gallery04.jpg"), "html", null, true);
        echo "\" alt=\"\"
                                                     title=\"\">
                                            </div>
                                            <div class=\"carousel-item\">
                                                <img src=\"";
        // line 215
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/gallery05.jpg"), "html", null, true);
        echo "\" alt=\"\"
                                                     title=\"\">
                                            </div>
                                        </div>
                                        <a class=\"carousel-control carousel-control-prev\" role=\"button\"
                                           data-slide=\"prev\" href=\"#lb-gallery1-2\">
                                            <span class=\"mbri-left mbr-iconfont\" aria-hidden=\"true\"></span>
                                            <span class=\"sr-only\">Previous</span>
                                        </a>
                                        <a class=\"carousel-control carousel-control-next\" role=\"button\"
                                           data-slide=\"next\" href=\"#lb-gallery1-2\">
                                            <span class=\"mbri-right mbr-iconfont\" aria-hidden=\"true\"></span>
                                            <span class=\"sr-only\">Next</span>
                                        </a>
                                        <a class=\"close\" href=\"#\" role=\"button\" data-dismiss=\"modal\">
                                            <span class=\"sr-only\">Close</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div><!-- Lightbox -->
                    </div><!--Gallery-->
                </section>
            </div><!--.container-->
        </div>
    </section><!--.watch-in-action-->
    <section class=\"community\">
        <div class=\"container\">
            <h2 class=\"section-title text-center dark\">Our community is already<br>over 60k members and growing</h2>
            <div class=\"row\">
                <div class=\"col\">
                    <div class=\"section-description\">Cras quis nulla commodo, aliquam lectus sed,
                        blandit augue. Cras ullamcorper bibendum bibendum. Duis tincidunt urna non pretium porta.
                        Nam condimentum vitae ligula vel ornare. Phasellus at semper turpis.
                        Nunc eu tellus tortor. Etiam at condimentum nisl, vitae sagittis orci.
                        Donec id dignissim nunc. Donec elit ante, eleifend a dolor et, venenatis facilisis dolor.
                        In feugiat orci odio, sed lacinia sem elementum quis. Aliquam consectetur, eros et vulputate
                        euismod,
                        nunc leo tempor lacus, ac rhoncus neque eros nec lacus. Cras lobortis molestie faucibus.
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"community-graph\">
                        <div class=\"graph-legend-y\">
                            <div>25k</div>
                            <div>20k</div>
                            <div>15k</div>
                            <div>10k</div>
                            <div>5k</div>
                        </div><!--.graph-legend-y-->
                        <div class=\"graph-container\">
                            <div class=\"graph-overlay\">
                                <div></div>
                                <div></div>
                                <div></div>
                                <div></div>
                                <div></div>
                            </div><!--.graph-overlay-->
                            <div class=\"graph-cols\">
                                <div class=\"graph-col\">
                                    <div class=\"col-back\"></div>
                                    <div class=\"col-transparent\"></div>
                                    <div class=\"col-hover\" style=\"height: 20%\"></div><!--change this height as needed-->
                                    <div class=\"grah-col-tooltip\" style=\"bottom: 24%;\">
                                        <div class=\"little-info\">March 2018</div>
                                        <div class=\"members-ads clearfix\">
                                            <div class=\"members-number\">
                                                <div class=\"big-info\">28.1K</div>
                                                <div class=\"little-info\">New members</div>
                                            </div>
                                            <div class=\"ads-number\">
                                                <div class=\"big-info\">10K</div>
                                                <div class=\"little-info\">Ads</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"graph-col\">
                                    <div class=\"col-back\"></div>
                                    <div class=\"col-transparent\"></div>
                                    <div class=\"col-hover\" style=\"height: 25%\"></div>
                                    <div class=\"grah-col-tooltip\" style=\"bottom: 29%;\">
                                        <div class=\"little-info\">March 2018</div>
                                        <div class=\"members-ads clearfix\">
                                            <div class=\"members-number\">
                                                <div class=\"big-info\">28.1K</div>
                                                <div class=\"little-info\">New members</div>
                                            </div>
                                            <div class=\"ads-number\">
                                                <div class=\"big-info\">10K</div>
                                                <div class=\"little-info\">Ads</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"graph-col\">
                                    <div class=\"col-back\"></div>
                                    <div class=\"col-transparent\"></div>
                                    <div class=\"col-hover\" style=\"height: 40%\"></div>
                                    <div class=\"grah-col-tooltip\" style=\"bottom: 44%;\">
                                        <div class=\"little-info\">March 2018</div>
                                        <div class=\"members-ads clearfix\">
                                            <div class=\"members-number\">
                                                <div class=\"big-info\">28.1K</div>
                                                <div class=\"little-info\">New members</div>
                                            </div>
                                            <div class=\"ads-number\">
                                                <div class=\"big-info\">10K</div>
                                                <div class=\"little-info\">Ads</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"graph-col\">
                                    <div class=\"col-back\"></div>
                                    <div class=\"col-transparent\"></div>
                                    <div class=\"col-hover\" style=\"height: 50%\"></div>
                                    <div class=\"grah-col-tooltip\" style=\"bottom: 54%;\">
                                        <div class=\"little-info\">March 2018</div>
                                        <div class=\"members-ads clearfix\">
                                            <div class=\"members-number\">
                                                <div class=\"big-info\">28.1K</div>
                                                <div class=\"little-info\">New members</div>
                                            </div>
                                            <div class=\"ads-number\">
                                                <div class=\"big-info\">10K</div>
                                                <div class=\"little-info\">Ads</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"graph-col\">
                                    <div class=\"col-back\"></div>
                                    <div class=\"col-transparent\"></div>
                                    <div class=\"col-hover\" style=\"height: 60%\"></div>
                                    <div class=\"grah-col-tooltip\" style=\"bottom: 64%;\">
                                        <div class=\"little-info\">March 2018</div>
                                        <div class=\"members-ads clearfix\">
                                            <div class=\"members-number\">
                                                <div class=\"big-info\">28.1K</div>
                                                <div class=\"little-info\">New members</div>
                                            </div>
                                            <div class=\"ads-number\">
                                                <div class=\"big-info\">10K</div>
                                                <div class=\"little-info\">Ads</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"graph-col\">
                                    <div class=\"col-back\"></div>
                                    <div class=\"col-transparent\"></div>
                                    <div class=\"col-hover\" style=\"height: 75%\"></div>
                                    <
                                    <div class=\"grah-col-tooltip\" style=\"bottom: 79%;\">
                                        <div class=\"little-info\">March 2018</div>
                                        <div class=\"members-ads clearfix\">
                                            <div class=\"members-number\">
                                                <div class=\"big-info\">28.1K</div>
                                                <div class=\"little-info\">New members</div>
                                            </div>
                                            <div class=\"ads-number\">
                                                <div class=\"big-info\">10K</div>
                                                <div class=\"little-info\">Ads</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--.graph-cols-->
                        </div><!--.graph-container-->
                        <div class=\"graph-legend-x\">
                            <div>Nov</div>
                            <div>Dec</div>
                            <div>Jan</div>
                            <div>Feb</div>
                            <div>Mar</div>
                            <div>Apr</div>
                        </div><!--.graph-legend-x-->
                    </div>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.community-->
    <section class=\"client-feedback\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col\">
                    <h2 class=\"section-title text-center dark\">Client feedback<br></h2>
                </div>
            </div>
            <div class=\"row client-feddback-gallery\">
                <div class=\"col\">
                    <div class=\"cf-item gallery-item\">
                        <div class=\"client-info clearfix\">
                            <figure class=\"clent-avatar\">
                                <img src=\"";
        // line 410
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/denise.png"), "html", null, true);
        echo "\" alt=\"client_avatar\">
                            </figure>
                            <div class=\"name-loc\">
                                <div class=\"client-name\">Denise Nichols</div>
                                <div class=\"client-location\">Zuid-Holland</div>
                            </div>
                        </div>
                        <div class=\"client-feedback-text\">
                            <p>{productName} takes the hassle out of going through
                                the feedback on a design, which ultimatel
                                speeds up the entire process, something everyone wants… read more</p>
                        </div>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"cf-item gallery-item\">
                        <div class=\"client-info clearfix\">
                            <figure class=\"clent-avatar\">
                                <img src=\"";
        // line 428
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/dorothy.png"), "html", null, true);
        echo "\" alt=\"client_avatar\">
                            </figure>
                            <div class=\"name-loc\">
                                <div class=\"client-name\">Dorothy Hamilton</div>
                                <div class=\"client-location\">Orléans</div>
                            </div>
                        </div>
                        <div class=\"client-feedback-text\">
                            <p>{productName} takes the hassle out of going through
                                the feedback on a design, which ultimatel
                                speeds up the entire process, something everyone wants… read more</p>
                        </div>
                    </div>
                </div>
                <div class=\"w-100\"></div>
                <div class=\"col\">
                    <div class=\"cf-item gallery-item\">
                        <div class=\"client-info clearfix\">
                            <figure class=\"clent-avatar\">
                                <img src=\"";
        // line 447
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/christopher.png"), "html", null, true);
        echo "\" alt=\"client_avatar\">
                            </figure>
                            <div class=\"name-loc\">
                                <div class=\"client-name\">Christopher Hughes</div>
                                <div class=\"client-location\">Brandenburg</div>
                            </div>
                        </div>
                        <div class=\"client-feedback-text\">
                            <p>{productName} has made massive improvements when communi- cating design at TMW.
                                There are usually many stakeholders involved in the process, both… read more</p>
                        </div>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"cf-item gallery-item\">
                        <div class=\"client-info clearfix\">
                            <figure class=\"clent-avatar\">
                                <img src=\"";
        // line 464
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/patrick.png"), "html", null, true);
        echo "\" alt=\"client_avatar\">
                            </figure>
                            <div class=\"name-loc\">
                                <div class=\"client-name\">Patrick Gilbert</div>
                                <div class=\"client-location\">Friesland</div>
                            </div>
                        </div>
                        <div class=\"client-feedback-text\">
                            <p>{productName} has made massive improvements when communi- cating design at TMW.
                                There are usually many stakeholders involved in the process, both… read more</p>
                        </div>
                    </div>
                </div>
                <div class=\"w-100\"></div>
                <div class=\"col\">
                    <div class=\"cf-item gallery-item\">
                        <div class=\"client-info clearfix\">
                            <figure class=\"clent-avatar\">
                                <img src=\"";
        // line 482
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/scott.png"), "html", null, true);
        echo "\" alt=\"client_avatar\">
                            </figure>
                            <div class=\"name-loc\">
                                <div class=\"client-name\">Scott Nguyen</div>
                                <div class=\"client-location\">Baden-Württemberg</div>
                            </div>
                        </div>
                        <div class=\"client-feedback-text\">
                            <p>Easy to use, works great, customizable, and by far, the best support I have
                                experienced.</p>
                        </div>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"cf-item gallery-item\">
                        <div class=\"client-info clearfix\">
                            <figure class=\"clent-avatar\">
                                <img src=\"";
        // line 499
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("s/build/site/img/johnny.png"), "html", null, true);
        echo "\" alt=\"client_avatar\">
                            </figure>
                            <div class=\"name-loc\">
                                <div class=\"client-name\">Johnny Young</div>
                                <div class=\"client-location\">Mulhouse</div>
                            </div>
                        </div>
                        <div class=\"client-feedback-text\">
                            <p>Easy to use, works great, customizable, and by far, the best support I have
                                experienced.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"text-center\">
                <a href=\"#read_all_reviews\" class=\"button color-button big\">Read all reviews</a>
            </div>
        </div><!--.container-->
    </section><!--.client-feedback-->
    <section class=\"register-free-acc\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col text-center\">
                    <h2 class=\"section-title\">Register a FREE Account</h2>
                    <div class=\"section-description big gray\">
                        <p>Create an account and benefit from multiple TraficBot advertising and money earning
                            opportunities. We only need your email address!</p>
                    </div>
                    <form id=\"register\">
                        <label for=\"register-email\">
                            <input id=\"register-email\" type=\"email\" name=\"register_email\" placeholder=\"Your email here\">
                        </label><br>
                        <label for=\"accept-term\">
                            <input type=\"checkbox\" id=\"accept-term\" name=\"accept_term\" checked>
                            I accept {product_name} Terms & Conditions
                        </label><br>
                        <input type=\"submit\" class=\"button color-button big\" value=\"Register\">
                    </form>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.register-free-acc-->
    <section class=\"economy-traffic-packages\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col text-center\">
                    <h2 class=\"section-title\">Economy Traffic Packages</h2>
                    <div class=\"section-description big gray\">
                        <p>You can have unlimited number of packages of different sizes linked to your account.<br>
                            * It is possible to purchase several packages and point them to the same URLs (10 x Large
                            will give you est. 6 Million hits per month).</p>
                    </div>
                </div>
            </div>
            <div class=\"row traffic-package-gallery\">
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Micro</div>
                        <div class=\"tp-price\">\$9.9</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 50k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Small</div>
                        <div class=\"tp-price\">\$29.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 200k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Medium</div>
                        <div class=\"tp-price\">\$59.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 500k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Large</div>
                        <div class=\"tp-price\">\$99.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 1M</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.economy-traffic-packages-->
    <section class=\"contact-us\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col\">
                    <h2 class=\"section-title text-center dark\">Contact Us</h2>
                    <form id=\"contact-us-form\">
                        <div class=\"row\">
                            <div class=\"col\">
                                <label for=\"visitor-name\">
                                    <span>Your name</span><br>
                                    <input type=\"text\" id=\"visitor-name\" name=\"visitor_name\">
                                </label>
                            </div>
                            <div class=\"col\">
                                <label for=\"visitor-email\">
                                    <span>Your E-mail</span><br>
                                    <input type=\"text\" id=\"visitor-email\" name=\"visitor_email\">
                                </label>
                            </div>
                            <div class=\"w-100\"></div>
                            <div class=\"col\">
                                <label for=\"visitor-message\">
                                    <span>Your message</span><br>
                                    <textarea id=\"visitor-message\" name=\"visitor_message\"
                                              placeholder=\"Write us something\"></textarea>
                                </label>
                                <div class=\"contact-form-description clerafix\">
                                    <p>You can always send us an email at
                                        <a href=\"mailto:support@produst_name.com\">support@{produst_name}.com</a>
                                    </p>
                                    <input type=\"submit\" class=\"button color-button big\" value=\"Send Message\">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.contact-us-->
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "site/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  628 => 499,  608 => 482,  587 => 464,  567 => 447,  545 => 428,  524 => 410,  326 => 215,  319 => 211,  312 => 207,  305 => 203,  298 => 199,  291 => 195,  270 => 177,  258 => 168,  246 => 159,  234 => 150,  222 => 141,  209 => 131,  175 => 100,  166 => 94,  150 => 81,  139 => 73,  130 => 67,  114 => 54,  100 => 43,  92 => 38,  64 => 13,  53 => 4,  44 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'site/layouts/main.html.twig' %}

{% block content %}
    <section class=\"header-info\">
        <div class=\"container\">
            <h1 class=\"page-title section-title\">
                <p>
                    <span>Buy website traffic at the best prices on the Internet at <i>{product_name}</i></span>
                </p>
            </h1>
            <div class=\"clearfix\"></div>
            <div class=\"button-wrapper\">
                <a href=\"{{ path('free_trial') }}\" class=\"button color-button big float-right\">Start free trial</a>
            </div>
            <div class=\"clearfix\"></div>
        </div>
    </section><!--.header-info-->
    <section class=\"how-it-works\">
        <div class=\"container\">
            <h2 class=\"section-title text-center\">How it Works</h2>
            <div class=\"row\">
                <div class=\"col\">
                    <div class=\"section-description\">
                        <p>Our platform generates traffic, which is by behavior very similar to what real
                            users do - surfing pages, click links on your website, download tracking scripts,
                            submit forms, etc. We use real browsers, so any scripts on your site are executed
                            (you can also exclude some from downloading to comply with 3d party terms).
                            You are very flexible to setup visitor flows - what pages users should reach first,
                            what next, how traffic should be redistributed across the day, from what locations traffic
                            should go. You can customize a lot of metrics like bounce rate, time on page, returning
                            visitors,
                            traffic sources, % of mobile traffic and a lot more. It is possible to set up multiple flows
                            to get traffic
                            looks more naturally</p>
                    </div>
                </div>
                <div class=\"col text-center\">
                    <img class=\"how-it-works-back\" src=\"{{ asset('s/build/site/img/how-it-works.png') }}\"
                         alt=\"how-it-works\">
                </div>
            </div><!--.row-->
            <div class=\"text-center\">
                <a href=\"{{ path('how_it_works') }}\"
                   class=\"how-it-works-read button color-button big position-relative\">Read More</a>
            </div>
        </div>
    </section><!--.how-it-works-->
    <section class=\"traffic\">
        <div class=\"container\">
            <div class=\"row traffic-gallery\">
                <div class=\"col\">
                    <div class=\"traffic-gallery-item gallery-item text-center\">
                        <div class=\"tsg-image\"
                             style=\"background-image: url({{ asset('s/build/site/img/purchase-traffic.png') }}); top: -18px;\"></div>
                        <h3 class=\"tsg-title section-subtitle\">Purchase Traffic</h3>
                        <p>We deliver high quality traffic to meet all your needs.
                            If you are after big numbers, we can deliver millions of visits per month.
                            Need conversions? We can guarantee real, 100% human visitors sent to your website.</p>
                        <div class=\"button-wrapper\">
                            <a href=\"#browse_packages\" class=\"button color-button\">Browse Packages</a>
                        </div>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"traffic-gallery-item gallery-item text-center\">
                        <div class=\"tsg-image\"
                             style=\"background-image: url({{ asset('s/build/site/img/geo-targeting.png') }}); top: -8px;\"></div>
                        <h3 class=\"tsg-title section-subtitle section-subtitle\">Geo-Targeting</h3>
                        <p>After creating a project you can choose what country's traffic you would like to receive.
                            Also there is an option to get North American, South American, Asian or European
                            traffic.</p>
                        <div class=\"button-wrapper\">
                            <a href=\"{{ path('geo_targeting') }}\" class=\"button color-button\">Learn More</a>
                        </div>
                    </div>
                </div>
                <div class=\"w-100\"></div>
                <div class=\"col\">
                    <div class=\"traffic-gallery-item gallery-item text-center\">
                        <div class=\"tsg-image\"
                             style=\"background-image: url({{ asset('s/build/site/img/traffic-from-isps.png') }}); top: 15px;\"></div>
                        <h3 class=\"tsg-title section-subtitle\">Traffic From ISP’s</h3>
                        <p>We deliver high quality traffic to meet all your needs. If you are after big numbers,
                            we can deliver millions of visits per month. Need conversions? We can guarantee real,
                            100% human visitors sent to your website.</p>
                        <div class=\"button-wrapper\">
                            <a href=\"#read_more\" class=\"button color-button\">Read More</a>
                        </div>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"traffic-gallery-item gallery-item text-center\">
                        <div class=\"tsg-image\"
                             style=\"background-image: url({{ asset('s/build/site/img/alexa-ranking-traffic.png') }}); top: 4px\"></div>
                        <h3 class=\"tsg-title section-subtitle\">Alexa Ranking Traffic</h3>
                        <p>We deliver high quality traffic to meet all your needs. If you are after big numbers,
                            we can deliver millions of visits per month. Need conversions? We can guarantee real,
                            100% human visitors sent to your website.</p>
                        <div class=\"button-wrapper\">
                            <a href=\"{{ path('pricing') }}\" class=\"button color-button\">See plans</a>
                        </div>
                    </div>
                </div>
            </div><!--.row.traffic-gallery-->
        </div><!--.container-->
    </section><!--.traffic--->
    <section class=\"watch-in-action\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col\">
                    <h2 class=\"section-title text-center\">Watch <i>{productName}</i> in action</h2>
                    <div class=\"section-description text-center\">
                        <p>Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui.
                            In malesuada enim in dolor euismod, id commodo mi consectetur.
                            Curabitur at vestibulum nisi. Nullam vehicula nisi velit. Mauris turpis nisl,
                            molestie ut ipsum et, suscipit vehicula odio.
                            Vestibulum interdum vestibulum felis ac molestie.</p>
                    </div>
                </div>
            </div>
            <div class=\"watch-in-action-gallery\">
                <section class=\"mbr-gallery mbr-slider-carousel cid-qLs0svQVOC\" id=\"gallery1-2\">
                    <div>
                        <div class=\"mbr-gallery-row\">
                            <div class=\"mbr-gallery-layout-default\">
                                <div class=\"container\">
                                    <div class=\"row\">
                                        <div class=\"col-6 mbr-gallery-item mbr-gallery-item--p1\" data-video-url=\"false\"
                                             data-tags=\"Awesome\">
                                            <div href=\"#lb-gallery1-2\" data-slide-to=\"0\" data-toggle=\"modal\">
                                                <img src=\"{{ asset('s/build/site/img/gallery00.jpg') }}\" alt=\"\"
                                                     title=\"\">
                                                <span class=\"icon-focus\"></span>
                                                <span class=\"mbr-gallery-title mbr-fonts-style display-7\">Test new caption</span>
                                            </div>
                                        </div>
                                        <!--there should not be spaces, \\n, EOH, in this place for, that the slider worked well -->
                                        <div class=\"col-6 mbr-gallery-item mbr-gallery-item--p1\" data-video-url=\"false\"
                                             data-tags=\"Responsive\">
                                            <div href=\"#lb-gallery1-2\" data-slide-to=\"1\" data-toggle=\"modal\">
                                                <img src=\"{{ asset('s/build/site/img/gallery01.jpg') }}\" alt=\"\"
                                                     title=\"\">
                                                <span class=\"icon-focus\"></span>
                                                <span class=\"mbr-gallery-title mbr-fonts-style display-7\">Type caption here</span>
                                            </div>
                                        </div>
                                        <div class=\"col-6 mbr-gallery-item mbr-gallery-item--p1\" data-video-url=\"false\"
                                             data-tags=\"Creative\">
                                            <div href=\"#lb-gallery1-2\" data-slide-to=\"2\" data-toggle=\"modal\">
                                                <img src=\"{{ asset('s/build/site/img/gallery02.jpg') }}\" alt=\"\"
                                                     title=\"\">
                                                <span class=\"icon-focus\"></span>
                                                <span class=\"mbr-gallery-title mbr-fonts-style display-7\">Type caption here</span>
                                            </div>
                                        </div>
                                        <div class=\"col-6 mbr-gallery-item mbr-gallery-item--p1\" data-video-url=\"false\"
                                             data-tags=\"Animated\">
                                            <div href=\"#lb-gallery1-2\" data-slide-to=\"3\" data-toggle=\"modal\">
                                                <img src=\"{{ asset('s/build/site/img/gallery03.jpg') }}\" alt=\"\"
                                                     title=\"\">
                                                <span class=\"icon-focus\"></span>
                                                <span class=\"mbr-gallery-title mbr-fonts-style display-7\">Type caption here</span>
                                            </div>
                                        </div>
                                        <div class=\"col-6 mbr-gallery-item mbr-gallery-item--p1\" data-video-url=\"false\"
                                             data-tags=\"Awesome\">
                                            <div href=\"#lb-gallery1-2\" data-slide-to=\"4\" data-toggle=\"modal\">
                                                <img src=\"{{ asset('s/build/site/img/gallery04.jpg') }}\" alt=\"\"
                                                     title=\"\">
                                                <span class=\"icon-focus\"></span>
                                                <span class=\"mbr-gallery-title mbr-fonts-style display-7\">Type caption here</span>
                                            </div>
                                        </div>
                                        <div class=\"col-6 mbr-gallery-item mbr-gallery-item--p1\" data-video-url=\"false\"
                                             data-tags=\"Awesome\">
                                            <div href=\"#lb-gallery1-2\" data-slide-to=\"5\" data-toggle=\"modal\">
                                                <img src=\"{{ asset('s/build/site/img/gallery05.jpg') }}\" alt=\"\"
                                                     title=\"\">
                                                <span class=\"icon-focus\"></span>
                                                <span class=\"mbr-gallery-title mbr-fonts-style display-7\">Type caption here</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"clearfix\"></div>
                            </div>
                        </div><!-- Filter --><!-- Gallery -->
                        <div data-app-prevent-settings=\"\" class=\"mbr-slider modal fade carousel slide\" tabindex=\"-1\"
                             data-keyboard=\"true\" data-interval=\"false\" id=\"lb-gallery1-2\">
                            <div class=\"modal-dialog\">
                                <div class=\"modal-content\">
                                    <div class=\"modal-body\">
                                        <div class=\"carousel-inner\">
                                            <div class=\"carousel-item active\">
                                                <img src=\"{{ asset('s/build/site/img/gallery00.jpg') }}\" alt=\"\"
                                                     title=\"\">
                                            </div>
                                            <div class=\"carousel-item\">
                                                <img src=\"{{ asset('s/build/site/img/gallery01.jpg') }}\" alt=\"\"
                                                     title=\"\">
                                            </div>
                                            <div class=\"carousel-item\">
                                                <img src=\"{{ asset('s/build/site/img/gallery02.jpg') }}\" alt=\"\"
                                                     title=\"\">
                                            </div>
                                            <div class=\"carousel-item\">
                                                <img src=\"{{ asset('s/build/site/img/gallery03.jpg') }}\" alt=\"\"
                                                     title=\"\">
                                            </div>
                                            <div class=\"carousel-item\">
                                                <img src=\"{{ asset('s/build/site/img/gallery04.jpg') }}\" alt=\"\"
                                                     title=\"\">
                                            </div>
                                            <div class=\"carousel-item\">
                                                <img src=\"{{ asset('s/build/site/img/gallery05.jpg') }}\" alt=\"\"
                                                     title=\"\">
                                            </div>
                                        </div>
                                        <a class=\"carousel-control carousel-control-prev\" role=\"button\"
                                           data-slide=\"prev\" href=\"#lb-gallery1-2\">
                                            <span class=\"mbri-left mbr-iconfont\" aria-hidden=\"true\"></span>
                                            <span class=\"sr-only\">Previous</span>
                                        </a>
                                        <a class=\"carousel-control carousel-control-next\" role=\"button\"
                                           data-slide=\"next\" href=\"#lb-gallery1-2\">
                                            <span class=\"mbri-right mbr-iconfont\" aria-hidden=\"true\"></span>
                                            <span class=\"sr-only\">Next</span>
                                        </a>
                                        <a class=\"close\" href=\"#\" role=\"button\" data-dismiss=\"modal\">
                                            <span class=\"sr-only\">Close</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div><!-- Lightbox -->
                    </div><!--Gallery-->
                </section>
            </div><!--.container-->
        </div>
    </section><!--.watch-in-action-->
    <section class=\"community\">
        <div class=\"container\">
            <h2 class=\"section-title text-center dark\">Our community is already<br>over 60k members and growing</h2>
            <div class=\"row\">
                <div class=\"col\">
                    <div class=\"section-description\">Cras quis nulla commodo, aliquam lectus sed,
                        blandit augue. Cras ullamcorper bibendum bibendum. Duis tincidunt urna non pretium porta.
                        Nam condimentum vitae ligula vel ornare. Phasellus at semper turpis.
                        Nunc eu tellus tortor. Etiam at condimentum nisl, vitae sagittis orci.
                        Donec id dignissim nunc. Donec elit ante, eleifend a dolor et, venenatis facilisis dolor.
                        In feugiat orci odio, sed lacinia sem elementum quis. Aliquam consectetur, eros et vulputate
                        euismod,
                        nunc leo tempor lacus, ac rhoncus neque eros nec lacus. Cras lobortis molestie faucibus.
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"community-graph\">
                        <div class=\"graph-legend-y\">
                            <div>25k</div>
                            <div>20k</div>
                            <div>15k</div>
                            <div>10k</div>
                            <div>5k</div>
                        </div><!--.graph-legend-y-->
                        <div class=\"graph-container\">
                            <div class=\"graph-overlay\">
                                <div></div>
                                <div></div>
                                <div></div>
                                <div></div>
                                <div></div>
                            </div><!--.graph-overlay-->
                            <div class=\"graph-cols\">
                                <div class=\"graph-col\">
                                    <div class=\"col-back\"></div>
                                    <div class=\"col-transparent\"></div>
                                    <div class=\"col-hover\" style=\"height: 20%\"></div><!--change this height as needed-->
                                    <div class=\"grah-col-tooltip\" style=\"bottom: 24%;\">
                                        <div class=\"little-info\">March 2018</div>
                                        <div class=\"members-ads clearfix\">
                                            <div class=\"members-number\">
                                                <div class=\"big-info\">28.1K</div>
                                                <div class=\"little-info\">New members</div>
                                            </div>
                                            <div class=\"ads-number\">
                                                <div class=\"big-info\">10K</div>
                                                <div class=\"little-info\">Ads</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"graph-col\">
                                    <div class=\"col-back\"></div>
                                    <div class=\"col-transparent\"></div>
                                    <div class=\"col-hover\" style=\"height: 25%\"></div>
                                    <div class=\"grah-col-tooltip\" style=\"bottom: 29%;\">
                                        <div class=\"little-info\">March 2018</div>
                                        <div class=\"members-ads clearfix\">
                                            <div class=\"members-number\">
                                                <div class=\"big-info\">28.1K</div>
                                                <div class=\"little-info\">New members</div>
                                            </div>
                                            <div class=\"ads-number\">
                                                <div class=\"big-info\">10K</div>
                                                <div class=\"little-info\">Ads</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"graph-col\">
                                    <div class=\"col-back\"></div>
                                    <div class=\"col-transparent\"></div>
                                    <div class=\"col-hover\" style=\"height: 40%\"></div>
                                    <div class=\"grah-col-tooltip\" style=\"bottom: 44%;\">
                                        <div class=\"little-info\">March 2018</div>
                                        <div class=\"members-ads clearfix\">
                                            <div class=\"members-number\">
                                                <div class=\"big-info\">28.1K</div>
                                                <div class=\"little-info\">New members</div>
                                            </div>
                                            <div class=\"ads-number\">
                                                <div class=\"big-info\">10K</div>
                                                <div class=\"little-info\">Ads</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"graph-col\">
                                    <div class=\"col-back\"></div>
                                    <div class=\"col-transparent\"></div>
                                    <div class=\"col-hover\" style=\"height: 50%\"></div>
                                    <div class=\"grah-col-tooltip\" style=\"bottom: 54%;\">
                                        <div class=\"little-info\">March 2018</div>
                                        <div class=\"members-ads clearfix\">
                                            <div class=\"members-number\">
                                                <div class=\"big-info\">28.1K</div>
                                                <div class=\"little-info\">New members</div>
                                            </div>
                                            <div class=\"ads-number\">
                                                <div class=\"big-info\">10K</div>
                                                <div class=\"little-info\">Ads</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"graph-col\">
                                    <div class=\"col-back\"></div>
                                    <div class=\"col-transparent\"></div>
                                    <div class=\"col-hover\" style=\"height: 60%\"></div>
                                    <div class=\"grah-col-tooltip\" style=\"bottom: 64%;\">
                                        <div class=\"little-info\">March 2018</div>
                                        <div class=\"members-ads clearfix\">
                                            <div class=\"members-number\">
                                                <div class=\"big-info\">28.1K</div>
                                                <div class=\"little-info\">New members</div>
                                            </div>
                                            <div class=\"ads-number\">
                                                <div class=\"big-info\">10K</div>
                                                <div class=\"little-info\">Ads</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"graph-col\">
                                    <div class=\"col-back\"></div>
                                    <div class=\"col-transparent\"></div>
                                    <div class=\"col-hover\" style=\"height: 75%\"></div>
                                    <
                                    <div class=\"grah-col-tooltip\" style=\"bottom: 79%;\">
                                        <div class=\"little-info\">March 2018</div>
                                        <div class=\"members-ads clearfix\">
                                            <div class=\"members-number\">
                                                <div class=\"big-info\">28.1K</div>
                                                <div class=\"little-info\">New members</div>
                                            </div>
                                            <div class=\"ads-number\">
                                                <div class=\"big-info\">10K</div>
                                                <div class=\"little-info\">Ads</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--.graph-cols-->
                        </div><!--.graph-container-->
                        <div class=\"graph-legend-x\">
                            <div>Nov</div>
                            <div>Dec</div>
                            <div>Jan</div>
                            <div>Feb</div>
                            <div>Mar</div>
                            <div>Apr</div>
                        </div><!--.graph-legend-x-->
                    </div>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.community-->
    <section class=\"client-feedback\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col\">
                    <h2 class=\"section-title text-center dark\">Client feedback<br></h2>
                </div>
            </div>
            <div class=\"row client-feddback-gallery\">
                <div class=\"col\">
                    <div class=\"cf-item gallery-item\">
                        <div class=\"client-info clearfix\">
                            <figure class=\"clent-avatar\">
                                <img src=\"{{ asset('s/build/site/img/denise.png') }}\" alt=\"client_avatar\">
                            </figure>
                            <div class=\"name-loc\">
                                <div class=\"client-name\">Denise Nichols</div>
                                <div class=\"client-location\">Zuid-Holland</div>
                            </div>
                        </div>
                        <div class=\"client-feedback-text\">
                            <p>{productName} takes the hassle out of going through
                                the feedback on a design, which ultimatel
                                speeds up the entire process, something everyone wants… read more</p>
                        </div>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"cf-item gallery-item\">
                        <div class=\"client-info clearfix\">
                            <figure class=\"clent-avatar\">
                                <img src=\"{{ asset('s/build/site/img/dorothy.png') }}\" alt=\"client_avatar\">
                            </figure>
                            <div class=\"name-loc\">
                                <div class=\"client-name\">Dorothy Hamilton</div>
                                <div class=\"client-location\">Orléans</div>
                            </div>
                        </div>
                        <div class=\"client-feedback-text\">
                            <p>{productName} takes the hassle out of going through
                                the feedback on a design, which ultimatel
                                speeds up the entire process, something everyone wants… read more</p>
                        </div>
                    </div>
                </div>
                <div class=\"w-100\"></div>
                <div class=\"col\">
                    <div class=\"cf-item gallery-item\">
                        <div class=\"client-info clearfix\">
                            <figure class=\"clent-avatar\">
                                <img src=\"{{ asset('s/build/site/img/christopher.png') }}\" alt=\"client_avatar\">
                            </figure>
                            <div class=\"name-loc\">
                                <div class=\"client-name\">Christopher Hughes</div>
                                <div class=\"client-location\">Brandenburg</div>
                            </div>
                        </div>
                        <div class=\"client-feedback-text\">
                            <p>{productName} has made massive improvements when communi- cating design at TMW.
                                There are usually many stakeholders involved in the process, both… read more</p>
                        </div>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"cf-item gallery-item\">
                        <div class=\"client-info clearfix\">
                            <figure class=\"clent-avatar\">
                                <img src=\"{{ asset('s/build/site/img/patrick.png') }}\" alt=\"client_avatar\">
                            </figure>
                            <div class=\"name-loc\">
                                <div class=\"client-name\">Patrick Gilbert</div>
                                <div class=\"client-location\">Friesland</div>
                            </div>
                        </div>
                        <div class=\"client-feedback-text\">
                            <p>{productName} has made massive improvements when communi- cating design at TMW.
                                There are usually many stakeholders involved in the process, both… read more</p>
                        </div>
                    </div>
                </div>
                <div class=\"w-100\"></div>
                <div class=\"col\">
                    <div class=\"cf-item gallery-item\">
                        <div class=\"client-info clearfix\">
                            <figure class=\"clent-avatar\">
                                <img src=\"{{ asset('s/build/site/img/scott.png') }}\" alt=\"client_avatar\">
                            </figure>
                            <div class=\"name-loc\">
                                <div class=\"client-name\">Scott Nguyen</div>
                                <div class=\"client-location\">Baden-Württemberg</div>
                            </div>
                        </div>
                        <div class=\"client-feedback-text\">
                            <p>Easy to use, works great, customizable, and by far, the best support I have
                                experienced.</p>
                        </div>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"cf-item gallery-item\">
                        <div class=\"client-info clearfix\">
                            <figure class=\"clent-avatar\">
                                <img src=\"{{ asset('s/build/site/img/johnny.png') }}\" alt=\"client_avatar\">
                            </figure>
                            <div class=\"name-loc\">
                                <div class=\"client-name\">Johnny Young</div>
                                <div class=\"client-location\">Mulhouse</div>
                            </div>
                        </div>
                        <div class=\"client-feedback-text\">
                            <p>Easy to use, works great, customizable, and by far, the best support I have
                                experienced.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"text-center\">
                <a href=\"#read_all_reviews\" class=\"button color-button big\">Read all reviews</a>
            </div>
        </div><!--.container-->
    </section><!--.client-feedback-->
    <section class=\"register-free-acc\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col text-center\">
                    <h2 class=\"section-title\">Register a FREE Account</h2>
                    <div class=\"section-description big gray\">
                        <p>Create an account and benefit from multiple TraficBot advertising and money earning
                            opportunities. We only need your email address!</p>
                    </div>
                    <form id=\"register\">
                        <label for=\"register-email\">
                            <input id=\"register-email\" type=\"email\" name=\"register_email\" placeholder=\"Your email here\">
                        </label><br>
                        <label for=\"accept-term\">
                            <input type=\"checkbox\" id=\"accept-term\" name=\"accept_term\" checked>
                            I accept {product_name} Terms & Conditions
                        </label><br>
                        <input type=\"submit\" class=\"button color-button big\" value=\"Register\">
                    </form>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.register-free-acc-->
    <section class=\"economy-traffic-packages\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col text-center\">
                    <h2 class=\"section-title\">Economy Traffic Packages</h2>
                    <div class=\"section-description big gray\">
                        <p>You can have unlimited number of packages of different sizes linked to your account.<br>
                            * It is possible to purchase several packages and point them to the same URLs (10 x Large
                            will give you est. 6 Million hits per month).</p>
                    </div>
                </div>
            </div>
            <div class=\"row traffic-package-gallery\">
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Micro</div>
                        <div class=\"tp-price\">\$9.9</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 50k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Small</div>
                        <div class=\"tp-price\">\$29.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 200k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Medium</div>
                        <div class=\"tp-price\">\$59.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 500k</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
                <div class=\"col\">
                    <div class=\"tp-item text-center\">
                        <div class=\"tp-title\">Large</div>
                        <div class=\"tp-price\">\$99.99</div>
                        <div class=\"price-details\">per months</div>
                        <div class=\"tp-hits\">Guaranteed Hits 1M</div>
                        <div class=\"tp-specs\">
                            <ul>
                                <li>Geo-Targeting</li>
                                <li>Bounce Rate Control</li>
                                <li>Session Duration Control</li>
                                <li>Language Control</li>
                                <li>Navigation Funnels</li>
                                <li>Google Analytics Safe</li>
                                <li>Traffic From ISP’s</li>
                                <li>Cities and States</li>
                            </ul>
                        </div>
                        <a href=\"buy\" class=\"button big\">Buy</a>
                    </div>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.economy-traffic-packages-->
    <section class=\"contact-us\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col\">
                    <h2 class=\"section-title text-center dark\">Contact Us</h2>
                    <form id=\"contact-us-form\">
                        <div class=\"row\">
                            <div class=\"col\">
                                <label for=\"visitor-name\">
                                    <span>Your name</span><br>
                                    <input type=\"text\" id=\"visitor-name\" name=\"visitor_name\">
                                </label>
                            </div>
                            <div class=\"col\">
                                <label for=\"visitor-email\">
                                    <span>Your E-mail</span><br>
                                    <input type=\"text\" id=\"visitor-email\" name=\"visitor_email\">
                                </label>
                            </div>
                            <div class=\"w-100\"></div>
                            <div class=\"col\">
                                <label for=\"visitor-message\">
                                    <span>Your message</span><br>
                                    <textarea id=\"visitor-message\" name=\"visitor_message\"
                                              placeholder=\"Write us something\"></textarea>
                                </label>
                                <div class=\"contact-form-description clerafix\">
                                    <p>You can always send us an email at
                                        <a href=\"mailto:support@produst_name.com\">support@{produst_name}.com</a>
                                    </p>
                                    <input type=\"submit\" class=\"button color-button big\" value=\"Send Message\">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!--.container-->
    </section><!--.contact-us-->
{% endblock %}
", "site/index.html.twig", "/var/www/trafficbot.loc/templates/site/index.html.twig");
    }
}
