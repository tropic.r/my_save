<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'service_locator.zgDKyvl' shared service.

return $this->privates['service_locator.zgDKyvl'] = new \Symfony\Component\DependencyInjection\ServiceLocator(array('translator' => function () {
    return ($this->services['translator'] ?? $this->getTranslatorService());
}));
