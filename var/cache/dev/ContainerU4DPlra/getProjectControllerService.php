<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'App\Controller\Admin\ProjectController' shared autowired service.

include_once $this->targetDirs[3].'/vendor/symfony/framework-bundle/Controller/ControllerTrait.php';
include_once $this->targetDirs[3].'/vendor/symfony/framework-bundle/Controller/AbstractController.php';
include_once $this->targetDirs[3].'/src/Controller/Admin/ProjectController.php';

$this->services['App\Controller\Admin\ProjectController'] = $instance = new \App\Controller\Admin\ProjectController();

$instance->setContainer(($this->privates['service_locator.aqxUcvZ'] ?? $this->load('getServiceLocator_AqxUcvZService.php'))->withContext('App\\Controller\\Admin\\ProjectController', $this));

return $instance;
