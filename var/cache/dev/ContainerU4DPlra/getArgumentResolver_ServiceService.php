<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'argument_resolver.service' shared service.

include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/Controller/ArgumentValueResolverInterface.php';
include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/Controller/ArgumentResolver/ServiceValueResolver.php';

return $this->privates['argument_resolver.service'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\ServiceValueResolver(new \Symfony\Component\DependencyInjection\ServiceLocator(array('App\\Controller\\Admin\\FlowController:add' => function () {
    return ($this->privates['service_locator.Yj2UZc1'] ?? $this->load('getServiceLocator_Yj2UZc1Service.php'));
}, 'App\\Controller\\Admin\\FlowController:edit' => function () {
    return ($this->privates['service_locator.Yj2UZc1'] ?? $this->load('getServiceLocator_Yj2UZc1Service.php'));
}, 'App\\Controller\\SecurityController:login' => function () {
    return ($this->privates['service_locator.qJjSKes'] ?? $this->load('getServiceLocator_QJjSKesService.php'));
}, 'App\\Controller\\TermsController:privacyPolicy' => function () {
    return ($this->privates['service_locator.zgDKyvl'] ?? $this->load('getServiceLocator_ZgDKyvlService.php'));
}, 'App\\Controller\\TermsController:refundPolicy' => function () {
    return ($this->privates['service_locator.zgDKyvl'] ?? $this->load('getServiceLocator_ZgDKyvlService.php'));
}, 'App\\Controller\\TermsController:termsAndConditions' => function () {
    return ($this->privates['service_locator.zgDKyvl'] ?? $this->load('getServiceLocator_ZgDKyvlService.php'));
}, 'App\\Controller\\UserController:register' => function () {
    return ($this->privates['service_locator.vvf7AP9'] ?? $this->load('getServiceLocator_Vvf7AP9Service.php'));
}, 'App\\Controller\\Admin\\FlowController::add' => function () {
    return ($this->privates['service_locator.Yj2UZc1'] ?? $this->load('getServiceLocator_Yj2UZc1Service.php'));
}, 'App\\Controller\\Admin\\FlowController::edit' => function () {
    return ($this->privates['service_locator.Yj2UZc1'] ?? $this->load('getServiceLocator_Yj2UZc1Service.php'));
}, 'App\\Controller\\SecurityController::login' => function () {
    return ($this->privates['service_locator.qJjSKes'] ?? $this->load('getServiceLocator_QJjSKesService.php'));
}, 'App\\Controller\\TermsController::privacyPolicy' => function () {
    return ($this->privates['service_locator.zgDKyvl'] ?? $this->load('getServiceLocator_ZgDKyvlService.php'));
}, 'App\\Controller\\TermsController::refundPolicy' => function () {
    return ($this->privates['service_locator.zgDKyvl'] ?? $this->load('getServiceLocator_ZgDKyvlService.php'));
}, 'App\\Controller\\TermsController::termsAndConditions' => function () {
    return ($this->privates['service_locator.zgDKyvl'] ?? $this->load('getServiceLocator_ZgDKyvlService.php'));
}, 'App\\Controller\\UserController::register' => function () {
    return ($this->privates['service_locator.vvf7AP9'] ?? $this->load('getServiceLocator_Vvf7AP9Service.php'));
})));
