let Encore = require('@symfony/webpack-encore');

let configs = [
    {
        outputPath: 'public/s/build/admin',
        publicPath: '/s/build/admin',
        entries: [
            {name: 'js/app', src: './assets/admin/js/app.js'},
            {name: 'js/user/register', src: './assets/admin/js/user/register.js'},
        ],
        styleEntries: [
            {name: 'css/app', src: './assets/admin/css/app.scss'},
            {name: 'css/user/register', src: './assets/admin/css/user/register.scss'},
        ],
        configureFilenames: {
            images: 'img/[name].[ext]',
            fonts: 'fonts/[name].[ext]',
        },
    },
    {
        outputPath: 'public/s/build/site',
        publicPath: '/s/build/site',
        entries: [
            {name: 'js/app', src: './assets/site/js/app.js'},
        ],
        styleEntries: [
            {name: 'css/app', src: './assets/site/css/app.scss'},
        ],
        configureFilenames: {
            images: 'img/[name].[ext]',
            fonts: 'fonts/[name].[ext]',
        },
    }
];

let encores = [];

for (let i in configs) {
    Encore
        .setOutputPath(configs[i].outputPath)
        .setPublicPath(configs[i].publicPath)
        .cleanupOutputBeforeBuild()
        .enableSourceMaps(!Encore.isProduction())
        .enableVersioning(Encore.isProduction())
        .enableSassLoader()
        .autoProvidejQuery()
        .enableBuildNotifications();

    for (let j in configs[i].entries) {
        let entry = configs[i].entries[j];
        Encore.addEntry(entry.name, entry.src);
    }
    for (let j in configs[i].styleEntries) {
        let entry = configs[i].styleEntries[j];
        Encore.addStyleEntry(entry.name, entry.src);
    }

    if (!Encore.isProduction()) {
        Encore.configureFilenames(configs[i].configureFilenames);
    }

    encores.push(Encore.getWebpackConfig());
    Encore.reset();
}

module.exports = encores;
