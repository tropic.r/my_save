require('bootstrap-sass');
require('metismenu');

$('#side-menu').metisMenu();

$('.navbar-minimalize').on('click', function (event) {
    event.preventDefault();
    $("body").toggleClass("mini-navbar");
    SmoothlyMenu();
});