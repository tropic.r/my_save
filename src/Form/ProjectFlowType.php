<?php

namespace App\Form;

use App\Entity\ProjectFlow;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectFlowType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('state', ChoiceType::class, [
                'choices' => [
                    'Active' => ProjectFlow::STATE_ACTIVE,
                    'Not active' => ProjectFlow::STATE_NOT_ACTIVE,
                ],
            ])
            ->add('trafficSpeed', IntegerType::class, [
                'empty_data' => 0,
                'required' => false,
            ])
            ->add('trafficDistribution', TrafficDistributionType::class, [
                'by_reference' => false,
            ])
            ->add('bounceRate', NumberType::class, [
                'by_reference' => false,
            ])
            ->add('timeOnPage', TimeOnPageType::class, [
                'by_reference' => false,
            ])
            ->add('returningVisitors', IntegerType::class, [
                'by_reference' => false,
            ])
            ->add('trafficType', TrafficTypeType::class, [
                'by_reference' => false,
            ])
            ->add('sitemapUrl', UrlType::class, [
                'empty_data' => '',
                'required' => false,
            ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var ProjectFlow $flow */
            $flow = $event->getData();
            $form = $event->getForm();

            if ($flow !== null && $flow->getId() !== null) {
                $form->add('save', SubmitType::class, [
                    'label' => "Save",
                ]);
            } else {
                $form->add('add', SubmitType::class, [
                    'label' => "Add",
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProjectFlow::class,
        ]);
    }
}
