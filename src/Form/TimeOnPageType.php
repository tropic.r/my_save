<?php

namespace App\Form;

use App\Entity\TimeOnPage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TimeOnPageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => array_combine(
                    TimeOnPage::getTypes(),
                    TimeOnPage::getTypes()
                ),
            ])
            ->add('value', IntegerType::class, [
                'empty_data' => 0,
                'required' => false,
            ])
            ->add('min', IntegerType::class, [
                'empty_data' => 0,
                'required' => false,
            ])
            ->add('max', IntegerType::class, [
                'empty_data' => 0,
                'required' => false,
            ])
            ->add('values', TextType::class, [
                'empty_data' => '',
                'required' => false,
            ]);

        $builder
            ->get('values')
            ->addModelTransformer(new CallbackTransformer(
                function (array $values) {
                    return join(", ", $values);
                },
                function (string $text) {
                    return array_filter(
                        array_map(
                            function ($value) {
                                return (int)trim($value);
                            },
                            explode(",", $text)
                        )
                    );
                }
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TimeOnPage::class,
        ]);
    }
}