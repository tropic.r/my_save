<?php

namespace App\Form;

use App\Entity\Project;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => "Name",
            ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var Project $project */
            $project = $event->getData();
            $form = $event->getForm();

            if ($project !== null && $project->getId() !== null) {
                $form->add('state', ChoiceType::class, [
                    'choices' => [
                        'Active' => Project::STATE_ACTIVE,
                        'Not active' => Project::STATE_NOT_ACTIVE,
                    ],
                ]);
                $form->add('save', SubmitType::class, [
                    'label' => "Save",
                ]);
            } else {
                $form->add('add', SubmitType::class, [
                    'label' => "Add",
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Project::class,
        ));
    }
}