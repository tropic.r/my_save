<?php

namespace App\Form;

use App\Entity\TrafficDistribution;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TimezoneType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TrafficDistributionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => array_combine(
                    TrafficDistribution::getTypes(),
                    TrafficDistribution::getTypes()
                ),
            ])
            ->add('time_zone', TimezoneType::class, [
                "input" => "datetimezone",
            ]);
        //->add('values');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TrafficDistribution::class,
        ]);
    }
}