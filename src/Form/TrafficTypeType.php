<?php

namespace App\Form;

use App\Entity\TrafficType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TrafficTypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => array_combine(
                    TrafficType::getTypes(),
                    TrafficType::getTypes()
                ),
            ])
            ->add('items', TextareaType::class, [
                'empty_data' => '',
                'required' => false,
            ]);

        $builder
            ->get('items')
            ->addModelTransformer(new CallbackTransformer(
                function (array $items) {
                    return join("\n", $items);
                },
                function (string $text) {
                    return array_filter(explode("\n", $text));
                }
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => TrafficType::class,
        ));
    }
}