<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\UserPackage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserPackage|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserPackage|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserPackage[]    findAll()
 * @method UserPackage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserPackageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserPackage::class);
    }

    /**
     * @param int $lockTime
     * @return UserPackage|null
     * @throws \Exception
     */
    public function lockNextPackage($lockTime)
    {
        $lockKey = base64_encode(random_bytes(16));

        $em = $this->getEntityManager();

        $affectedRows = $em->getConnection()->executeUpdate('
            UPDATE user_package SET
              lock_key = :lockKey,
              locked_at = NOW()
            WHERE
              (
                lock_key = ""
                OR locked_at < NOW() - INTERVAL :lockTime SECOND
              )
              AND start_date <= NOW()
              AND end_date >= NOW()
              AND remaining_visits > 0
            ORDER BY
              locked_at ASC
            LIMIT 1',
            [
                'lockKey' => $lockKey,
                'lockTime' => $lockTime,
            ]
        );

        if ($affectedRows === 0) {
            return null;
        }

        return $em->getRepository(UserPackage::class)->findOneBy([
            'lockKey' => $lockKey,
        ]);
    }

    /**
     * @param UserPackage $userPackage
     * @param int $tasksCount
     * @return int Returns amount of tasks NOT decreased from package (if package is empty)
     */
    public function decreaseRemainingVisits($userPackage, $tasksCount)
    {
        // Check how much visits are in user package
        $packageRemainingVisits = $userPackage->getRemainingVisits();

        // If remaining visits in package enough how passed tasks count, decrease all tasks
        // otherwise decrease all remaining visits
        if ($packageRemainingVisits > $tasksCount) {
            $visitsToDecrease = $tasksCount;
        } else {
            $visitsToDecrease = $packageRemainingVisits;
        }

        $userPackage->setRemainingVisits($packageRemainingVisits - $visitsToDecrease);

        return $tasksCount - $visitsToDecrease;
    }

    /**
     * @param User $user
     * @param \DateTimeInterface $date
     * @return UserPackage[]
     */
    public function findActivePackages(User $user, \DateTimeInterface $date)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.user = :user')
            ->andWhere('u.startDate <= :date')
            ->andWhere('u.endDate >= :date')
            ->setParameter('user', $user)
            ->setParameter('date', $date)
            ->orderBy('u.endDate', 'asc')
            ->getQuery()
            ->getResult();
    }

//    /**
//     * @return UserPackage[] Returns an array of UserPackage objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserPackage
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
