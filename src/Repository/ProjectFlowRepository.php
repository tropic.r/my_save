<?php

namespace App\Repository;

use App\Entity\ProjectFlow;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProjectFlow|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProjectFlow|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProjectFlow[]    findAll()
 * @method ProjectFlow[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectFlowRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProjectFlow::class);
    }

    public function lockFlow(int $id, int $lockTime)
    {
        $lockKey = base64_encode(random_bytes(16));

        $em = $this->getEntityManager();

        $affectedRows = $em->getConnection()->executeUpdate('
            UPDATE project_flow SET
              lock_key = :lockKey,
              locked_at = NOW()
            WHERE
              (
                lock_key = ""
                OR locked_at < NOW() - INTERVAL :lockTime SECOND
              )
              AND id = :id',
            [
                'id' => $id,
                'lockKey' => $lockKey,
                'lockTime' => $lockTime,
            ]
        );

        if ($affectedRows === 0) {
            return null;
        }

        return $em->getRepository(ProjectFlow::class)->findOneBy([
            'lockKey' => $lockKey,
        ]);
    }

    /**
     * @param ProjectFlow $flow
     * @param \DateTimeInterface $date
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function unlockFLow(ProjectFlow $flow, \DateTimeInterface $date)
    {
        $flow->setLockKey("");
        $flow->setLastProcessedAt($date);
        $em = $this->getEntityManager();
        $em->persist($flow);
        $em->flush();
    }

//    /**
//     * @return ProjectFlow[] Returns an array of ProjectFlow objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProjectFlow
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
