<?php

namespace App\Repository;

use App\Entity\Project;
use App\Entity\Visitor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Visitor|null find($id, $lockMode = null, $lockVersion = null)
 * @method Visitor|null findOneBy(array $criteria, array $orderBy = null)
 * @method Visitor[]    findAll()
 * @method Visitor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VisitorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Visitor::class);
    }

    /**
     * @param \DateTimeImmutable $maxLastVisitedAt
     * @param Project $project
     * @param string $countryCode
     * @return Visitor|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findVisitor(
        \DateTimeImmutable $maxLastVisitedAt,
        Project $project,
        string $countryCode)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.project = :project')
            ->andWhere('v.lastVisitedAt <= :maxLastVisitedAt')
            ->andWhere('v.countryCode = :countryCode')
            ->setParameter('project', $project)
            ->setParameter('maxLastVisitedAt', $maxLastVisitedAt)
            ->setParameter('countryCode', $countryCode)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

//    /**
//     * @return Visitor[] Returns an array of Visitor objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Visitor
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
