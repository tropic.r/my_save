<?php

namespace App\Consumer;

use App\Component\TaskDistributor\TaskDistributorFactory;
use App\Component\TaskModifier\TaskModifier;
use App\Component\TaskPublisher;
use App\Entity\Proxy;
use App\Entity\Task;
use App\Repository\ProjectFlowRepository;
use App\Repository\UserPackageRepository;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

class FlowConsumer implements ConsumerInterface
{
    /**
     * @var ProjectFlowRepository
     */
    private $flowRepository;
    /**
     * @var TaskPublisher
     */
    private $taskPublisher;
    /**
     * @var UserPackageRepository
     */
    private $userPackageRepository;
    /**
     * @var int
     */
    private $flowLockTime;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var TaskModifier[]
     */
    private $taskModifiers;
    /**
     * @var TaskDistributorFactory
     */
    private $taskDistributorFactory;

    /**
     * FlowConsumer constructor.
     * @param ProjectFlowRepository $flowRepository
     * @param UserPackageRepository $userPackageRepository
     * @param TaskPublisher $taskPublisher
     * @param LoggerInterface $logger
     * @param int $flowLockTime
     * @param TaskDistributorFactory $taskDistributorFactory
     */
    public function __construct(
        ProjectFlowRepository $flowRepository,
        UserPackageRepository $userPackageRepository,
        TaskPublisher $taskPublisher,
        LoggerInterface $logger,
        int $flowLockTime,
        TaskDistributorFactory $taskDistributorFactory
    )
    {
        $this->flowRepository = $flowRepository;
        $this->taskPublisher = $taskPublisher;
        $this->userPackageRepository = $userPackageRepository;
        $this->flowLockTime = $flowLockTime;
        $this->logger = $logger;
        $this->taskModifiers = [];
        $this->taskDistributorFactory = $taskDistributorFactory;
    }

    public function addTaskModifier(TaskModifier $taskModifier)
    {
        $this->taskModifiers[] = $taskModifier;
    }

    private function getUrl()
    {
        $urls = [
            "http://amazo.site/",
            "http://amazo.site/index.html",
            "http://amazo.site/services.html",
            "http://amazo.site/portfolio.html",
            "http://amazo.site/pricing.html",
            "http://amazo.site/contact.html",
        ];

        shuffle($urls);

        return $urls[0];
    }

    private function getReferer()
    {
        $referers = [
            "https://www.google.com/search?q=lava+lamp",
            "https://www.google.com/search?q=buy+lava+lamps",
            "https://www.google.com/search?q=google+lava+lamp",
            "https://www.google.com/search?q=lava+lamps+for+sale",
            "https://www.google.com/search?q=cool+lava+lamp",
            "https://www.google.com/search?q=green+lava+lamp",
            "https://www.google.com/search?q=red+lava+lamp",
            "https://www.google.com/search?q=blue+lava+lamp",
            "https://www.google.com/search?q=yellow+lava+lamp",
            "https://www.google.com/search?q=electric+lava+lamp",
            "https://www.google.com/search?q=best+lava+lamp",
            "https://www.google.com/search?q=office+lava+lamp",
            "https://www.elastic.co/webinars/elasticsearch-cyber-security-webinar-series-addressing-security-gaps?blade=facebook&hulk=cpc",
            "https://www.facebook.com/elastic.co/",
            "https://www.facebook.com/womenintechsnap/",
            "https://www.facebook.com/FIFAFootballAwards/",
        ];

        shuffle($referers);

        return $referers[0];
    }

    private function getProxy()
    {
        $proxies = [
            new Proxy("52.65.224.239:3128", "http", "traffic:traffic"),
            new Proxy("35.182.13.121:3128", "http", "traffic:traffic"),
            new Proxy("52.47.103.57:3128", "http", "traffic:traffic"),
            new Proxy("35.156.230.158:3128", "http", "traffic:traffic"),
            new Proxy("52.31.178.168:3128", "http", "traffic:traffic"),
            new Proxy("35.176.78.234:3128", "http", "traffic:traffic"),
            new Proxy("2607:fbe0:1:43:8000::6a30:7fba:3128", "http", "traffic:traffic"),
        ];

        shuffle($proxies);

        return $proxies[0];
    }

    public function decreaseRemainingVisits(array $userPackages, $tasksCount)
    {
        foreach ($userPackages as $userPackage) {
            $tasksCount = $this->userPackageRepository->decreaseRemainingVisits($userPackage, $tasksCount);
            if ($tasksCount === 0) {
                break;
            }
        }
    }

    /**
     * @param AMQPMessage $msg
     * @throws \Exception
     */
    protected function exec(AMQPMessage $msg)
    {
        $this->flowRepository->clear();
        $this->userPackageRepository->clear();

        $message = json_decode($msg->body, true);
        $flowId = $message["flow_id"];

        $flow = $this->flowRepository->lockFlow($flowId, $this->flowLockTime);
        if ($flow === null) {
            throw new \RuntimeException("Flow $flowId is busy");
        }
        $user = $flow->getProject()->getUser();

        $now = new \DateTimeImmutable('now');

        $userPackages = $this->userPackageRepository->findActivePackages($user, $now);

        $remainingVisits = 0;
        foreach ($userPackages as $userPackage) {
            $remainingVisits += $userPackage->getRemainingVisits();
        }

        $tasksCount = $this->taskDistributorFactory->getDistributorByName(
            $flow->getTrafficDistribution()->getType()
        )->getTasksCount($flow, $now);

        if ($tasksCount > $remainingVisits) {
            $tasksCount = $remainingVisits;
        }

        $this->logger->debug('Task to generate', [
            'count' => $tasksCount,
        ]);

        for ($i = 0; $i < $tasksCount; $i++) {
            $task = new Task();
            foreach ($this->taskModifiers as $taskModifier) {
                $task = $taskModifier->modify($task, $flow);
            }

            $this->taskPublisher->publish(
                $task
                    ->setStartUrl($this->getUrl())
                    ->setReferer($this->getReferer())
                    ->setProxy($this->getProxy())
            );
        }

        $this->decreaseRemainingVisits($userPackages, $tasksCount);

        $lastProcessedAt = $now;
        if ($tasksCount === 0) {
            $lastProcessedAt = $flow->getLastProcessedAt();
        }

        $this->flowRepository->unlockFLow($flow, $lastProcessedAt);
    }

    public function execute(AMQPMessage $msg)
    {
        while (true) {
            try {
                $this->exec($msg);
                return;
            } catch (\Exception $e) {
                $this->logger->error("Exception caught while processing flow", [
                    'message' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                ]);
                sleep(1);
            }
        }
    }
}