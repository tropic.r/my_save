<?php

namespace App\Consumer;

use App\Component\TaskSerializer;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Process\Process;

class TaskConsumer implements ConsumerInterface
{
    /**
     * @var TaskSerializer
     */
    private $taskSerializer;
    /**
     * @var string
     */
    private $phantomjsBinary;
    /**
     * @var string
     */
    private $crawlerjsPath;


    /**
     * TaskConsumer constructor.
     * @param TaskSerializer $taskSerializer
     * @param string $phantomjsBinary
     * @param string $crawlerjsPath
     */
    public function __construct(
        TaskSerializer $taskSerializer,
        string $phantomjsBinary,
        string $crawlerjsPath
    )
    {
        $this->taskSerializer = $taskSerializer;
        $this->phantomjsBinary = $phantomjsBinary;
        $this->crawlerjsPath = $crawlerjsPath;
    }

    public function execute(AMQPMessage $msg)
    {
        $task = $this->taskSerializer->unserialize($msg->body);

        $cookies = [];
        foreach ($task->getCookies() as $cookie) {
            $cookies[] = [
                "domain" => $cookie->getDomain(),
                "expiry" => $cookie->getExpires(),
                "httponly" => $cookie->isHttpOnly(),
                "name" => $cookie->getName(),
                "path" => $cookie->getPath(),
                "secure" => $cookie->isSecure(),
                "value" => $cookie->getValue(),
            ];
        }

        $cmd = $this->phantomjsBinary
            . " --proxy=" . escapeshellarg($task->getProxy()->getProxy())
            . " --proxy-type=" . escapeshellarg($task->getProxy()->getType())
            . " --proxy-auth=" . escapeshellarg($task->getProxy()->getAuth())
            . " " . $this->crawlerjsPath;

        $stdin = json_encode([
            "startUrl" => $task->getStartUrl(),
            "referer" => $task->getReferer(),
            "userAgent" => $task->getUserAgent(),
            "cookies" => $cookies,
        ]);

        echo "echo '$stdin' | " . $cmd . PHP_EOL;

        $process = new Process($cmd, null, null, null, 180);
        $process->setInput($stdin);
        $process->start();

        try {
            foreach ($process as $type => $data) {
                if ($process::OUT === $type) {
                    echo trim($data) . PHP_EOL;
                } else {
                    echo "<error>" . trim($data) . "</error>" . PHP_EOL;
                }
            }
        } catch (\Exception $e) {
            echo "<error>{$e->getMessage()}</error>" . PHP_EOL;
        }
    }
}