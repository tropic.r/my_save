<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180604194055 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $packages = [
            ["Mini", 1, 60000, "month", 1, 9.99, "USD"],
            ["Medium", 2, 300000, "month", 1, 29, "USD"],
            ["Large", 4, 600000, "month", 1, 59, "USD"],
            ["Ultimate", 8, 1000000, "month", 1, 99, "USD"],
        ];

        foreach ($packages as $package) {
            $this->addSql("INSERT INTO package (
              name, projects_count, visits_count, billing_term, billing_term_count, price_amount, price_currency
            ) VALUES (?, ?, ?, ?, ?, ?, ?)", $package);
        }
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project ADD site_domain VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE project_flow ADD url_set LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:object)\'');
    }
}
