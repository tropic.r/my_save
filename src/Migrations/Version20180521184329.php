<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180521184329 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE visitor ADD user_agent_id INT NOT NULL');
        $this->addSql('ALTER TABLE visitor ADD CONSTRAINT FK_CAE5E19FD499950B FOREIGN KEY (user_agent_id) REFERENCES user_agent (id)');
        $this->addSql('CREATE INDEX IDX_CAE5E19FD499950B ON visitor (user_agent_id)');
        $this->addSql('ALTER TABLE user_agent CHANGE active active TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_agent CHANGE active active TINYINT(1) DEFAULT \'1\' NOT NULL');
        $this->addSql('ALTER TABLE visitor DROP FOREIGN KEY FK_CAE5E19FD499950B');
        $this->addSql('DROP INDEX IDX_CAE5E19FD499950B ON visitor');
        $this->addSql('ALTER TABLE visitor DROP user_agent_id');
    }
}
