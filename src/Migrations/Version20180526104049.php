<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180526104049 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE _petrov_package_backup');
        $this->addSql('DROP TABLE _petrov_user_package_backup');
        $this->addSql('ALTER TABLE project ADD site_domain VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE _petrov_package_backup (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, projects_count INT NOT NULL, visits_count INT NOT NULL, billing_term VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, billing_term_count INT NOT NULL, price_amount NUMERIC(2, 0) NOT NULL, price_currency VARCHAR(3) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE _petrov_user_package_backup (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, package_id INT NOT NULL, start_date DATETIME NOT NULL, end_date DATETIME NOT NULL, remaining_visits INT NOT NULL, projects_count INT NOT NULL, lock_key VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, locked_at DATETIME NOT NULL, INDEX IDX_8665799FA76ED395 (user_id), INDEX IDX_8665799FF44CABFF (package_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE project DROP site_domain');
    }
}
