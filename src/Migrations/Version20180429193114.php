<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180429193114 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE project_flow (id INT AUTO_INCREMENT NOT NULL, project_id INT NOT NULL, traffic_speed INT NOT NULL, traffic_distribution VARCHAR(255) NOT NULL, bounce_rate NUMERIC(10, 0) NOT NULL, time_on_page LONGTEXT NOT NULL COMMENT \'(DC2Type:object)\', returning_visitors INT NOT NULL, traffic_type VARCHAR(255) NOT NULL, sitemap_url VARCHAR(255) NOT NULL, INDEX IDX_6B511E68166D1F9C (project_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE project_flow ADD CONSTRAINT FK_6B511E68166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE project_flow');
    }
}
