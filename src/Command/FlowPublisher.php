<?php

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use \App\Component\FlowPublisher as FlowPublisherComponent;

class FlowPublisher extends ContainerAwareCommand
{
    /**
     * @var \App\Component\FlowPublisher
     */
    private $publisher;

    /**
     * TaskPublisher constructor.
     * @param null|string $name
     * @param \App\Component\FlowPublisher $publisher
     */
    public function __construct(?string $name = null, FlowPublisherComponent $publisher)
    {
        parent::__construct($name);

        $this->publisher = $publisher;
    }

    protected function configure()
    {
        $this
            ->setName("tb:flow-publisher")
            ->setDescription("Publish flow tasks");
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Task publisher running");

        while (true) {
            try {
                $this->publisher->enqueueFlows($output);
                sleep(1);
            } catch (\Exception $e) {
                $output->writeln("<error>{$e->getMessage()}</error>");
                sleep(5);
            }
        }
    }
}