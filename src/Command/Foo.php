<?php

namespace App\Command;

use App\Component\WeightedRandom;
use App\Entity\Package;
use App\Entity\User;
use App\Entity\UserPackage;
use App\Entity\WeightedRandomItem;
use App\Producer\FlowProducer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Foo extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName("tb:foo")
            ->setDescription("Skel command for running something in app environment");
    }

    protected function packages()
    {
        $entityManager = $this->getContainer()->get('doctrine')->getManager();

        $user = $entityManager->getRepository(User::class)->find(1);

        $package = new Package();
        $package->setName("Large");
        $package->setBillingTerm(Package::TERM_MONTH);
        $package->setBillingTermCount(1);
        $package->setPriceAmount(59);
        $package->setPriceCurrency("USD");
        $package->setProjectsCount(1000);
        $package->setVisitsCount(1000000);

        $userPackage = new UserPackage();
        $userPackage->setUser($user);
        $userPackage->setPackage($package);
        $userPackage->setRemainingProjects(1000);
        $userPackage->setRemainingVisits(1000000);
        $userPackage->setStartDate(new \DateTimeImmutable("2018-05-13 00:00:00"));

        $entityManager->persist($package);
        $entityManager->persist($userPackage);

        $entityManager->flush();
    }

    protected function publishTest()
    {
        $flowProducer = $this->getContainer()->get('old_sound_rabbit_mq.flow_producer');

        $flowProducer->publish(json_encode([
            'message' => 'Hello World!',
        ]));
    }

    protected function testWeighedRandom()
    {
        $weighedRandom = new WeightedRandom([
            new WeightedRandomItem(10, "10%"),
            new WeightedRandomItem(40, "40%"),
            new WeightedRandomItem(20, "20%"),
            new WeightedRandomItem(30, "30%"),
        ]);

        $stats = [];
        for ($i = 0; $i < 1000; $i++) {
            $item = $weighedRandom->getRandomItem();
            isset($stats[$item]) or $stats[$item] = 0;
            $stats[$item]++;
        }

        print_r($stats);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->testWeighedRandom();
    }
}