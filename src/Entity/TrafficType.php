<?php

namespace App\Entity;

class TrafficType implements \Serializable
{
    const TYPE_DIRECT = "direct";
    const TYPE_SOCIAL = "social";
    const TYPE_REFERRAL = "referral";
    const TYPE_ORGANIC = "organic";

    static $types = [
        self::TYPE_DIRECT,
        self::TYPE_SOCIAL,
        self::TYPE_REFERRAL,
        self::TYPE_ORGANIC,
    ];

    /**
     * @var string
     */
    private $type = "";

    /**
     * @var string[]
     */
    private $items = [];

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        if (!in_array($type, self::$types, true)) {
            throw new \InvalidArgumentException("Invalid type");
        }
        $this->type = $type;
    }

    /**
     * @return array
     */
    public static function getTypes(): array
    {
        return self::$types;
    }

    /**
     * @return string[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param string[] $items
     */
    public function setItems($items): void
    {
        $this->items = $items;
    }

    public function serialize()
    {
        return json_encode([
            "type" => $this->type,
            "items" => $this->items,
        ]);
    }

    public function unserialize($serialized)
    {
        $data = json_decode($serialized, true);
        $this->setType((string)$data["type"]);
        $this->setItems($data["items"]);
    }
}