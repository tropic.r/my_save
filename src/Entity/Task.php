<?php

namespace App\Entity;

class Task implements TaskInterface
{
    /**
     * @var string
     */
    private $visitorId;
    /**
     * @var string
     */
    private $startUrl;
    /**
     * @var Cookie[]
     */
    private $cookies;
    /**
     * @var Proxy|null
     */
    private $proxy;
    /**
     * @var string
     */
    private $deliveryTag;
    /**
     * @var string
     */
    private $referer;
    /**
     * @var UserAgent|null
     */
    private $userAgent;

    /**
     * Task constructor.
     * @param string $visitorId
     * @param string $startUrl
     * @param string $referer
     * @param Proxy|null $proxy
     * @param UserAgent|null $userAgent
     * @param Cookie[] $cookies
     * @param string $deliveryTag
     */
    public function __construct(
        string $visitorId = "",
        string $startUrl = "",
        string $referer = "",
        ?Proxy $proxy = null,
        ?UserAgent $userAgent = null,
        array $cookies = [],
        string $deliveryTag = ""
    )
    {
        /* Array of instance validation. */
        foreach ($cookies as $key => $value) {
            if (!$value instanceof Cookie) {
                throw new \InvalidArgumentException(
                    sprintf(
                        'Invalid value of `%s["%s"]`. Must be instance of %s but %s given.',
                        '$cookies',
                        $key,
                        Cookie::class,
                        is_object($value) ? 'instance of ' . get_class($value) : gettype($value)
                    )
                );
            }
        }

        $this->visitorId = $visitorId;
        $this->startUrl = $startUrl;
        $this->cookies = $cookies;
        $this->proxy = $proxy;
        $this->deliveryTag = $deliveryTag;
        $this->referer = $referer;
        $this->userAgent = $userAgent;
    }

    /**
     * @return string
     */
    public function getVisitorId(): string
    {
        return $this->visitorId;
    }

    /**
     * @return string
     */
    public function getStartUrl(): string
    {
        return $this->startUrl;
    }

    /**
     * @return string
     */
    public function getReferer(): string
    {
        return $this->referer;
    }

    /**
     * @return Proxy|null
     */
    public function getProxy(): ?Proxy
    {
        return $this->proxy;
    }

    /**
     * @return UserAgent|null
     */
    public function getUserAgent(): ?UserAgent
    {
        return $this->userAgent;
    }

    /**
     * @return Cookie[]
     */
    public function getCookies(): array
    {
        return $this->cookies;
    }

    /**
     * @return string
     */
    public function getDeliveryTag(): string
    {
        return $this->deliveryTag;
    }

    /**
     * @param string $visitorId
     * @return TaskInterface
     */
    public function setVisitorId(string $visitorId): TaskInterface
    {
        $task = clone $this;
        $task->visitorId = $visitorId;

        return $task;
    }

    /**
     * @param string $startUrl
     * @return TaskInterface
     */
    public function setStartUrl(string $startUrl): TaskInterface
    {
        $task = clone $this;
        $task->startUrl = $startUrl;

        return $task;
    }

    /**
     * @param Cookie[] $cookies
     * @return TaskInterface
     */
    public function setCookies(array $cookies): TaskInterface
    {
        $task = clone $this;
        $task->cookies = $cookies;

        return $task;
    }

    /**
     * @param Proxy $proxy
     * @return TaskInterface
     */
    public function setProxy(Proxy $proxy): TaskInterface
    {
        $task = clone $this;
        $task->proxy = $proxy;

        return $task;
    }

    /**
     * @param string $deliveryTag
     * @return TaskInterface
     */
    public function setDeliveryTag(string $deliveryTag): TaskInterface
    {
        $task = clone $this;
        $task->deliveryTag = $deliveryTag;

        return $task;
    }

    /**
     * @param string $referer
     * @return TaskInterface
     */
    public function setReferer(string $referer): TaskInterface
    {
        $task = clone $this;
        $task->referer = $referer;

        return $task;
    }

    /**
     * @param UserAgent $userAgent
     * @return TaskInterface
     */
    public function setUserAgent(UserAgent $userAgent): TaskInterface
    {
        $task = clone $this;
        $task->userAgent = $userAgent;

        return $task;
    }
}