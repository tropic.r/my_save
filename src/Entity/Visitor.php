<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VisitorRepository")
 */
class Visitor
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $countryCode = "";

    /**
     * @ORM\Column(type="text")
     */
    private $cookies = "";

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserAgent")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userAgent = "";

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Project")
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTimeInterface
     */
    private $lastVisitedAt;

    public function __construct()
    {
        $this->lastVisitedAt = new \DateTimeImmutable("1000-01-01 00:00:00");
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    public function setCountryCode(string $countryCode): self
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    public function getCookies(): string
    {
        return $this->cookies;
    }

    public function setCookies(string $cookies): self
    {
        $this->cookies = $cookies;

        return $this;
    }

    public function getUserAgent(): ?UserAgent
    {
        return $this->userAgent;
    }

    public function setUserAgent(?UserAgent $userAgent): self
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getLastVisitedAt(): \DateTimeInterface
    {
        return $this->lastVisitedAt;
    }

    public function setLastVisitedAt(\DateTimeInterface $lastVisitedAt): self
    {
        $this->lastVisitedAt = $lastVisitedAt;

        return $this;
    }
}
