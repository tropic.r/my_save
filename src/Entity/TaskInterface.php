<?php

namespace App\Entity;

interface TaskInterface
{
    /**
     * @return string
     */
    public function getVisitorId(): string;

    /**
     * @return string
     */
    public function getStartUrl(): string;

    /**
     * @return string
     */
    public function getReferer(): string;

    /**
     * @return Proxy
     */
    public function getProxy(): ?Proxy;

    /**
     * @return UserAgent|null
     */
    public function getUserAgent(): ?UserAgent;

    /**
     * @return Cookie[]
     */
    public function getCookies(): array;

    /**
     * @return string
     */
    public function getDeliveryTag(): string;

    /**
     * @param string $visitorId
     * @return mixed
     */
    public function setVisitorId(string $visitorId): self;

    /**
     * @param string $startUrl
     * @return TaskInterface
     */
    public function setStartUrl(string $startUrl): self;

    /**
     * @param string $referer
     * @return TaskInterface
     */
    public function setReferer(string $referer): self;

    /**
     * @param Proxy $proxyProxy
     * @return TaskInterface
     */
    public function setProxy(Proxy $proxyProxy): self;

    /**
     * @param UserAgent $userAgent
     * @return TaskInterface
     */
    public function setUserAgent(UserAgent $userAgent): self;

    /**
     * @param array $cookies
     * @return TaskInterface
     */
    public function setCookies(array $cookies): self;

    /**
     * @param string $deliveryTag
     * @return TaskInterface
     */
    public function setDeliveryTag(string $deliveryTag): self;
}