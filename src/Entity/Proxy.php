<?php

namespace App\Entity;

class Proxy
{
    /**
     * @var string
     */
    private $proxy;
    /**
     * @var string
     */
    private $type;
    /**
     * @var string
     */
    private $auth;

    /**
     * Proxy constructor.
     * @param string $proxy
     * @param string $type
     * @param string $auth
     */
    public function __construct(string $proxy, string $type, string $auth)
    {
        $this->proxy = $proxy;
        $this->type = $type;
        $this->auth = $auth;
    }

    /**
     * @return string
     */
    public function getProxy(): string
    {
        return $this->proxy;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getAuth(): string
    {
        return $this->auth;
    }
}