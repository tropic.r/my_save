<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserAgentRepository")
 */
class UserAgent
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $userAgent;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $popularity;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * UserAgent constructor.
     * @param int $id
     * @param string $userAgent
     * @param float $popularity
     * @param bool $active
     */
    public function __construct(int $id, string $userAgent, float $popularity, bool $active)
    {
        $this->id = $id;
        $this->userAgent = $userAgent;
        $this->popularity = $popularity;
        $this->active = $active;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUserAgent(): string
    {
        return $this->userAgent;
    }

    public function setUserAgent(string $userAgent): self
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    public function getPopularity(): float
    {
        return $this->popularity;
    }

    public function setPopularity($popularity): self
    {
        $this->popularity = $popularity;

        return $this;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
