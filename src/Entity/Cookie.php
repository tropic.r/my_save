<?php

namespace App\Entity;

class Cookie
{
    /**
     * @var string
     */
    private $domain;
    /**
     * @var int
     */
    private $expires;
    /**
     * @var bool
     */
    private $httpOnly;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $path;
    /**
     * @var bool
     */
    private $secure;
    /**
     * @var string
     */
    private $value;

    /**
     * Cookie constructor.
     * @param string $domain
     * @param int $expires
     * @param bool $httpOnly
     * @param string $name
     * @param string $path
     * @param bool $secure
     * @param string $value
     */
    public function __construct(
        string $domain,
        int $expires,
        bool $httpOnly,
        string $name,
        string $path,
        bool $secure,
        string $value
    )
    {
        $this->domain = $domain;
        $this->expires = $expires;
        $this->httpOnly = $httpOnly;
        $this->name = $name;
        $this->path = $path;
        $this->secure = $secure;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * @return int
     */
    public function getExpires(): int
    {
        return $this->expires;
    }

    /**
     * @return bool
     */
    public function isHttpOnly(): bool
    {
        return $this->httpOnly;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return bool
     */
    public function isSecure(): bool
    {
        return $this->secure;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}