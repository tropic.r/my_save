<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    const STATE_ACTIVE = "active";
    const STATE_NOT_ACTIVE = "not_active";

    static $states = [
        self::STATE_ACTIVE,
        self::STATE_NOT_ACTIVE,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name = "";

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $state;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProjectFlow", mappedBy="project", orphanRemoval=true)
     */
    private $projectFlows;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="projects")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function __construct()
    {
        $this->projectFlows = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        if (!in_array($state, self::$states, true)) {
            throw new \InvalidArgumentException("Invalid state");
        }
        $this->state = $state;

        return $this;
    }

    /**
     * @return Collection|ProjectFlow[]
     */
    public function getProjectFlows(): Collection
    {
        return $this->projectFlows;
    }

    public function addProjectFlow(ProjectFlow $projectFlow): self
    {
        if (!$this->projectFlows->contains($projectFlow)) {
            $this->projectFlows[] = $projectFlow;
            $projectFlow->setProject($this);
        }

        return $this;
    }

    public function removeProjectFlow(ProjectFlow $projectFlow): self
    {
        if ($this->projectFlows->contains($projectFlow)) {
            $this->projectFlows->removeElement($projectFlow);
            // set the owning side to null (unless already changed)
            if ($projectFlow->getProject() === $this) {
                $projectFlow->setProject(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
