<?php

namespace App\Entity;

class TimeOnPage implements \Serializable
{
    const TYPE_FIXED = "fixed";
    const TYPE_RANDOM = "random";
    const TYPE_LOGARITHMIC = "logarithmic";
    const TYPE_CUSTOM = "custom";

    static $types = [
        self::TYPE_FIXED,
        self::TYPE_RANDOM,
        self::TYPE_LOGARITHMIC,
        self::TYPE_CUSTOM,
    ];

    /**
     * @var string
     */
    private $type = "";

    /**
     * @var int
     */
    private $value = 0;

    /**
     * @var int
     */
    private $min = 0;

    /**
     * @var int
     */
    private $max = 0;

    /**
     * @var int[]
     */
    private $values = [];

    public function __construct()
    {
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        if (!in_array($type, self::$types, true)) {
            throw new \InvalidArgumentException("Invalid type");
        }
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue(int $value): void
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getMin(): int
    {
        return $this->min;
    }

    /**
     * @param int $min
     */
    public function setMin(int $min): void
    {
        $this->min = $min;
    }

    /**
     * @return int
     */
    public function getMax(): int
    {
        return $this->max;
    }

    /**
     * @param int $max
     */
    public function setMax(int $max): void
    {
        $this->max = $max;
    }

    /**
     * @return int[]
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * @param int[] $values
     */
    public function setValues(array $values): void
    {
        $this->values = $values;
    }

    public function serialize()
    {
        return json_encode([
            "type" => $this->type,
            "value" => $this->value,
            "min" => $this->min,
            "max" => $this->max,
            "values" => $this->values,
        ]);
    }

    /**
     * @return string[]
     */
    public static function getTypes(): array
    {
        return self::$types;
    }

    public function unserialize($serialized)
    {
        $data = json_decode($serialized, true);
        $this->setType((string)$data["type"]);
        $this->setValue((int)$data["value"]);
        $this->setMin((int)$data["min"]);
        $this->setMax((int)$data["max"]);
        $this->setValues(array_map("intval", $data["values"]));
    }
}