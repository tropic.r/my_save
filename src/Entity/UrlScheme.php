<?php

namespace App\Entity;

abstract class UrlScheme implements \Serializable
{
    const TYPE_RANDOM = "random";
    const TYPE_SITEMAP = "sitemap";

    /**
     * @var string
     */
    private $type;

    /**
     * UrlSet constructor.
     */
    public function __construct()
    {
        $this->type = self::TYPE_RANDOM;
        $this->urls = [];
    }

    /**
     * @return string[]
     */
    public function getUrls(): array
    {
        return $this->urls;
    }

    /**
     * @param string[] $urls
     */
    public function setUrls(array $urls): void
    {
        $this->urls = $urls;
    }

    public function serialize()
    {
        return json_encode([
            "url_lists" => $this->urlLists,
        ]);
    }

    public function unserialize($serialized)
    {
        $data = json_decode($serialized, true);
        $this->setUrls(unserialize($data["url_lists"]));
    }
}