<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PackageRepository")
 */
class Package
{
    const TERM_DAY = 'day';
    const TERM_WEEK = 'week';
    const TERM_MONTH = 'month';
    const TERM_YEAR = 'year';

    static private $terms = [
        self::TERM_DAY,
        self::TERM_WEEK,
        self::TERM_MONTH,
        self::TERM_YEAR,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $projectsCount;

    /**
     * @ORM\Column(type="integer")
     */
    private $visitsCount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $billingTerm;

    /**
     * @ORM\Column(type="integer")
     */
    private $billingTermCount;

    /**
     * @ORM\Column(type="decimal", precision=2)
     */
    private $priceAmount;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $priceCurrency;

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getProjectsCount(): ?int
    {
        return $this->projectsCount;
    }

    public function setProjectsCount(int $projectsCount): self
    {
        $this->projectsCount = $projectsCount;

        return $this;
    }

    public function getVisitsCount(): ?int
    {
        return $this->visitsCount;
    }

    public function setVisitsCount(int $visitsCount): self
    {
        $this->visitsCount = $visitsCount;

        return $this;
    }

    public function getBillingTerm(): ?string
    {
        return $this->billingTerm;
    }

    public function setBillingTerm(string $billingTerm): self
    {
        if (!in_array($billingTerm, self::$terms, true)) {
            throw new \InvalidArgumentException("Invalid billingTerm");
        }
        $this->billingTerm = $billingTerm;

        return $this;
    }

    public function getBillingTermCount(): ?int
    {
        return $this->billingTermCount;
    }

    public function setBillingTermCount(int $billingTermCount): self
    {
        $this->billingTermCount = $billingTermCount;

        return $this;
    }

    public function getPriceAmount()
    {
        return $this->priceAmount;
    }

    public function setPriceAmount($priceAmount): self
    {
        $this->priceAmount = $priceAmount;

        return $this;
    }

    public function getPriceCurrency(): ?string
    {
        return $this->priceCurrency;
    }

    public function setPriceCurrency(string $priceCurrency): self
    {
        $this->priceCurrency = $priceCurrency;

        return $this;
    }
}
