<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectFlowRepository")
 */
class ProjectFlow
{
    const STATE_ACTIVE = "active";
    const STATE_NOT_ACTIVE = "not_active";

    static $states = [
        self::STATE_ACTIVE,
        self::STATE_NOT_ACTIVE,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name = "";

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $state = "";

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="projectFlows")
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;

    /**
     * Traffic speed visits per day
     * @ORM\Column(type="integer")
     */
    private $trafficSpeed = 0;

    /**
     * @ORM\Column(type="object")
     * @var TrafficDistribution
     */
    private $trafficDistribution;

    /**
     * @ORM\Column(type="decimal", precision=0)
     */
    private $bounceRate = 0;

    /**
     * @ORM\Column(type="object")
     * @var TimeOnPage
     */
    private $timeOnPage;

    /**
     * @ORM\Column(type="integer")
     */
    private $returningVisitors = 0;

    /**
     * @ORM\Column(type="object")
     * @var TrafficType
     */
    private $trafficType;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sitemapUrl = "";

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lockKey = "";

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTimeInterface
     */
    private $lockedAt;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTimeInterface
     */
    private $lastProcessedAt;

    public function __construct()
    {
        $this->lockedAt = new \DateTimeImmutable("1000-01-01 00:00:00");
        $this->lastProcessedAt = new \DateTimeImmutable("1000-01-01 00:00:00");
    }

    public function getId()
    {
        return $this->id;
    }

    public function getProject(): Project
    {
        return $this->project;
    }

    public function setProject(Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getTrafficSpeed(): int
    {
        return $this->trafficSpeed;
    }

    public function setTrafficSpeed(int $trafficSpeed): self
    {
        $this->trafficSpeed = $trafficSpeed;

        return $this;
    }

    public function getTrafficDistribution(): TrafficDistribution
    {
        return $this->trafficDistribution;
    }

    public function setTrafficDistribution(TrafficDistribution $trafficDistribution): self
    {
        $this->trafficDistribution = $trafficDistribution;

        return $this;
    }

    public function getBounceRate()
    {
        return $this->bounceRate;
    }

    public function setBounceRate($bounceRate): self
    {
        $this->bounceRate = $bounceRate;

        return $this;
    }

    public function getReturningVisitors(): int
    {
        return $this->returningVisitors;
    }

    public function setReturningVisitors(int $returningVisitors): self
    {
        $this->returningVisitors = $returningVisitors;

        return $this;
    }

    public function getTrafficType(): TrafficType
    {
        return $this->trafficType;
    }

    public function setTrafficType(TrafficType $trafficType): self
    {
        $this->trafficType = $trafficType;

        return $this;
    }

    public function getSitemapUrl(): string
    {
        return $this->sitemapUrl;
    }

    public function setSitemapUrl(string $sitemapUrl): self
    {
        $this->sitemapUrl = $sitemapUrl;

        return $this;
    }

    public function getTimeOnPage(): TimeOnPage
    {
        return $this->timeOnPage;
    }

    public function setTimeOnPage(TimeOnPage $timeOnPage): self
    {
        $this->timeOnPage = $timeOnPage;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        if (!in_array($state, self::$states, true)) {
            throw new \InvalidArgumentException("Invalid state");
        }
        $this->state = $state;

        return $this;
    }

    /**
     * @return string
     */
    public function getLockKey(): string
    {
        return $this->lockKey;
    }

    /**
     * @param string $lockKey
     * @return ProjectFlow
     */
    public function setLockKey(string $lockKey): self
    {
        $this->lockKey = $lockKey;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getLockedAt(): \DateTimeInterface
    {
        return $this->lockedAt;
    }

    /**
     * @param \DateTimeInterface $lockedAt
     * @return ProjectFlow
     */
    public function setLockedAt(\DateTimeInterface $lockedAt): self
    {
        $this->lockedAt = $lockedAt;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getLastProcessedAt(): \DateTimeInterface
    {
        return $this->lastProcessedAt;
    }

    /**
     * @param \DateTimeInterface $lastProcessedAt
     * @return ProjectFlow
     */
    public function setLastProcessedAt(\DateTimeInterface $lastProcessedAt): self
    {
        $this->lastProcessedAt = $lastProcessedAt;

        return $this;
    }
}
