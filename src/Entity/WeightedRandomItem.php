<?php

namespace App\Entity;

class WeightedRandomItem implements WeightedRandomItemInterface
{
    /**
     * @var float
     */
    private $weight;
    /**
     * @var mixed
     */
    private $item;

    /**
     * WeightedRandom constructor.
     * @param float $weight
     * @param mixed $item
     */
    public function __construct(float $weight, $item)
    {
        $this->weight = $weight;
        $this->item = $item;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }
}