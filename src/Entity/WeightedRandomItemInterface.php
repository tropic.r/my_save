<?php

namespace App\Entity;

interface WeightedRandomItemInterface
{
    /**
     * @return float
     */
    public function getWeight(): float;

    /**
     * @return mixed
     */
    public function getItem();
}