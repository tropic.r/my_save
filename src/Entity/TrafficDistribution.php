<?php

namespace App\Entity;

class TrafficDistribution implements \Serializable
{
    const TYPE_NORMAL = "normal";
    const TYPE_TIME_BASED = "time_based";

    static $types = [
        self::TYPE_NORMAL,
        self::TYPE_TIME_BASED,
    ];

    /**
     * @var string
     */
    private $type = "";

    /**
     * @var \DateTimeZone
     */
    private $timeZone;

    public function __construct()
    {
        $this->timeZone = new \DateTimeZone("UTC");
    }

    public function serialize()
    {
        return json_encode([
            "type" => $this->type,
            "time_zone" => $this->timeZone === null ? null : $this->timeZone->getName(),
        ]);
    }

    public function unserialize($serialized)
    {
        $data = json_decode($serialized, true);
        $this->setType((string)$data["type"]);
        $this->setTimeZone(new \DateTimeZone($data["time_zone"]));
    }

    /**
     * @param \DateTimeZone $timeZone
     */
    public function setTimeZone(\DateTimeZone $timeZone): void
    {
        $this->timeZone = $timeZone;
    }

    /**
     * @return array
     */
    public static function getTypes(): array
    {
        return self::$types;
    }

    /**
     * @return \DateTimeZone
     */
    public function getTimeZone(): \DateTimeZone
    {
        return $this->timeZone;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        if (!in_array($type, self::$types, true)) {
            throw new \InvalidArgumentException("Invalid type");
        }
        $this->type = $type;
    }
}