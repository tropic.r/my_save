<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserPackageRepository")
 */
class UserPackage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userPackages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Package")
     * @ORM\JoinColumn(nullable=false)
     */
    private $package;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $endDate;

    /**
     * @ORM\Column(type="integer")
     */
    private $remainingVisits;

    /**
     * @ORM\Column(type="integer")
     */
    private $projectsCount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lockKey = "";

    /**
     * @ORM\Column(type="datetime")
     */
    private $lockedAt;

    public function __construct()
    {
        $this->lockedAt = new \DateTimeImmutable("1000-01-01 00:00:00");
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPackage(): Package
    {
        return $this->package;
    }

    public function setPackage(Package $package): self
    {
        $this->package = $package;

        return $this;
    }

    public function getStartDate(): \DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getRemainingVisits(): int
    {
        return $this->remainingVisits;
    }

    public function setRemainingVisits(int $remainingVisits): self
    {
        $this->remainingVisits = $remainingVisits;

        return $this;
    }

    public function getProjectsCount(): int
    {
        return $this->projectsCount;
    }

    public function setProjectsCount(int $projectsCount): self
    {
        $this->projectsCount = $projectsCount;

        return $this;
    }

    public function getLockKey(): string
    {
        return $this->lockKey;
    }

    public function setLockKey(string $lockKey): self
    {
        $this->lockKey = $lockKey;

        return $this;
    }

    public function getLockedAt(): \DateTimeInterface
    {
        return $this->lockedAt;
    }

    public function setLockedAt(\DateTimeInterface $lockedAt): self
    {
        $this->lockedAt = $lockedAt;

        return $this;
    }

    public function getEndDate(): \DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }
}
