<?php

namespace App\Component;

use App\Entity\Project;
use App\Entity\ProjectFlow;
use App\Entity\User;
use App\Producer\FlowProducer;
use App\Repository\ProjectFlowRepository;
use App\Repository\ProjectRepository;
use App\Repository\UserPackageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FlowPublisher
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var FlowProducer
     */
    private $flowProducer;
    /**
     * @var UserPackageRepository
     */
    private $userPackageRepository;
    /**
     * @var ProjectRepository
     */
    private $projectRepository;
    /**
     * @var ProjectFlowRepository
     */
    private $projectFlowRepository;
    /**
     * @var int
     */
    private $userPackageLockTime;

    /**
     * TaskPublisher constructor.
     * @param EntityManagerInterface $entityManager
     * @param UserPackageRepository $userPackageRepository
     * @param ProjectRepository $projectRepository
     * @param ProjectFlowRepository $projectFlowRepository
     * @param ContainerInterface $container
     * @param int $userPackageLockTime
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserPackageRepository $userPackageRepository,
        ProjectRepository $projectRepository,
        ProjectFlowRepository $projectFlowRepository,
        ContainerInterface $container,
        int $userPackageLockTime
    )
    {
        $this->entityManager = $entityManager;
        $this->userPackageRepository = $userPackageRepository;
        $this->projectRepository = $projectRepository;
        $this->flowProducer = $container->get('old_sound_rabbit_mq.flow_producer');
        $this->projectFlowRepository = $projectFlowRepository;
        $this->userPackageLockTime = $userPackageLockTime;
    }

    /**
     * @param OutputInterface $output
     * @throws \Exception
     */
    public function enqueueFlows(OutputInterface $output)
    {
        $this->userPackageRepository->clear();
        $this->projectFlowRepository->clear();

        // First we gonna check for active user packages and get the least recently used package. This ensure that all
        // packages will be processed, even if we have lack of workers running.
        $userPackage = $this->userPackageRepository->lockNextPackage($this->userPackageLockTime);
        if ($userPackage === null) {
            return;
        }

        /** @var $user User */
        $user->getProjects();


        // Next we get allowed by package amount of projects owned by package user
        $projects = $this->projectRepository->findBy([
            'user' => $userPackage->getUser(),
            'state' => Project::STATE_ACTIVE,
        ], null, $userPackage->getProjectsCount());

        // And for each project get flows and enqueue separate tasks for each flow to process.
        foreach ($projects as $project) {
            $flows = $this->projectFlowRepository->findBy([
                'project' => $project,
                'state' => ProjectFlow::STATE_ACTIVE,
            ]);

            foreach ($flows as $flow) {
                $this->flowProducer->publish(json_encode([
                    'flow_id' => $flow->getId(),
                ]));
            }
        }
    }
}