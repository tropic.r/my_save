<?php

namespace App\Component;

use App\Entity\TaskInterface;
use App\Producer\TaskProducer;
use Psr\Container\ContainerInterface;

class TaskPublisher
{
    /**
     * @var TaskProducer
     */
    private $taskProducer;
    /**
     * @var TaskSerializer
     */
    private $taskSerializer;

    /**
     * TaskPublisher constructor.
     * @param ContainerInterface $container
     * @param TaskSerializer $taskSerializer
     */
    public function __construct(
        ContainerInterface $container,
        TaskSerializer $taskSerializer
    )
    {
        $this->taskProducer = $container->get('old_sound_rabbit_mq.task_producer');
        $this->taskSerializer = $taskSerializer;
    }

    /**
     * @param TaskInterface $task
     */
    public function publish(TaskInterface $task)
    {
        $this->taskProducer->publish(
            $this->taskSerializer->serialize($task)
        );
    }
}