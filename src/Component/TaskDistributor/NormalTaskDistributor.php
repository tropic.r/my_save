<?php

namespace App\Component\TaskDistributor;

use App\Entity\ProjectFlow;
use Psr\Log\LoggerInterface;

class NormalTaskDistributor implements TaskDistributor
{
    /**
     * @var int
     */
    private $maxTickInterval;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * FixedTaskDistributor constructor.
     * @param int $maxTickInterval
     * @param LoggerInterface $logger
     */
    public function __construct(int $maxTickInterval, LoggerInterface $logger)
    {
        $this->maxTickInterval = $maxTickInterval;
        $this->logger = $logger;
    }

    /**
     * @param ProjectFlow $flow
     * @param \DateTimeInterface $now
     * @return int
     */
    public function getTasksCount(ProjectFlow $flow, \DateTimeInterface $now): int
    {
        $day = 3600 * 24;
        $lastProcessedAt = $flow->getLastProcessedAt()->getTimestamp();
        if ($lastProcessedAt < 0) {
            return 0;
        }
        $secondsSinceLastProcess = ($now->getTimestamp() - $flow->getLastProcessedAt()->getTimestamp());
        if ($secondsSinceLastProcess > $this->maxTickInterval) {
            $secondsSinceLastProcess = $this->maxTickInterval;
        }

        $tasksCount = $flow->getTrafficSpeed() / $day * $secondsSinceLastProcess;

        $minTasksCount = (int)floor($tasksCount);
        $maxTasksCount = (int)ceil($tasksCount);

        $random = rand($minTasksCount * 100, $maxTasksCount * 100) / 100;
        $this->logger->debug("Calculate tasks count", [
            "trafficSpeed" => $flow->getTrafficSpeed(),
            "random" => $random,
            "tasksCount" => $tasksCount,
            "minTasksCount" => $minTasksCount,
            "maxTasksCount" => $maxTasksCount,
        ]);

        if ($random > $tasksCount) {
            $tasksCount = $minTasksCount;
        } else {
            $tasksCount = $maxTasksCount;
        }

        return $tasksCount;
    }
}