<?php

namespace App\Component\TaskDistributor;

use App\Entity\ProjectFlow;

interface TaskDistributor
{
    /**
     * @param ProjectFlow $flow
     * @param \DateTimeInterface $now
     * @return int
     */
    public function getTasksCount(ProjectFlow $flow, \DateTimeInterface $now): int;
}