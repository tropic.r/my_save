<?php

namespace App\Component\TaskDistributor;

class TaskDistributorFactory
{
    /**
     * @var TaskDistributor[]
     */
    private $distributors;

    /**
     * TaskDistributorFactory constructor.
     */
    public function __construct()
    {
        $this->distributors = [];
    }

    public function addDistributor(string $name, TaskDistributor $distributor)
    {
        $this->distributors[$name] = $distributor;
    }

    /**
     * @param string $name
     * @return TaskDistributor
     */
    public function getDistributorByName(string $name): TaskDistributor
    {
        if (!array_key_exists($name, $this->distributors)) {
            throw new \OutOfBoundsException(sprintf("Not distributed registered with name '%s'", $name));
        }

        return $this->distributors[$name];
    }
}