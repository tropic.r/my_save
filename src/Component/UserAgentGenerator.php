<?php

namespace App\Component;

use App\Entity\UserAgent;
use App\Entity\WeightedRandomItem;
use App\Repository\UserAgentRepository;

/**
 * Class UserAgentGenerator
 * @method UserAgent getRandomItem()
 * @package App\Component
 */
class UserAgentGenerator extends WeightedRandom
{
    /**
     * @var UserAgentRepository
     */
    private $userAgentRepository;

    /**
     * UserAgentGenerator constructor.
     * @param UserAgentRepository $userAgentRepository
     */
    public function __construct(UserAgentRepository $userAgentRepository)
    {
        parent::__construct([]);

        $this->userAgentRepository = $userAgentRepository;
        $this->init();
    }

    public function init()
    {
        $userAgents = $this->userAgentRepository->findAll();

        $this->setItems(array_map(function(UserAgent $userAgent) {
            return new WeightedRandomItem($userAgent->getPopularity(), $userAgent);
        }, $userAgents));
    }
}