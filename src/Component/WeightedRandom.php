<?php

namespace App\Component;

use App\Entity\WeightedRandomItem;
use App\Entity\WeightedRandomItemInterface;

class WeightedRandom
{
    /**
     * @var array
     */
    private $items;

    /**
     * WeightedRandom constructor.
     * @param WeightedRandomItemInterface[] $items
     */
    public function __construct(array $items = [])
    {
        $this->setItems($items);
    }

    /**
     * @param WeightedRandomItem[] $items
     */
    public function setItems(array $items)
    {
        $this->items = [];

        $totalWeight = 0;
        foreach ($items as $item) {
            $totalWeight += $item->getWeight();
        }

        foreach ($items as $item) {
            $this->items[] = [
                "weight" => $item->getWeight() / $totalWeight,
                "item" => $item->getItem(),
            ];
        }
    }

    /**
     * @return mixed
     */
    public function getRandomItem()
    {
        $random = mt_rand() / mt_getrandmax();
        $cumulativeWeight = 0;

        foreach ($this->items as $item) {
            $cumulativeWeight += $item["weight"];
            if ($cumulativeWeight >= $random) {
                return $item["item"];
            }
        }

        $item = end($this->items);
        reset($this->items);

        return $item["item"];
    }
}