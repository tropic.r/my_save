<?php

namespace App\Component\TaskModifier;

use App\Entity\Cookie;
use App\Entity\ProjectFlow;
use App\Entity\TaskInterface;
use App\Entity\Visitor;
use App\Repository\VisitorRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class VisitorModifier implements TaskModifier
{
    /**
     * @var VisitorRepository
     */
    private $visitorRepository;
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * VisitorModifier constructor.
     * @param VisitorRepository $visitorRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        VisitorRepository $visitorRepository,
        EntityManagerInterface $entityManager
    )
    {
        $this->visitorRepository = $visitorRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @return Cookie[]
     */
    private function getStubCookies()
    {
        return [
            new Cookie(
                ".amazo.site",
                1523730779,
                false,
                "_gat_gtag_UA_117218470_1",
                "/",
                false,
                "1"
            ),
            new Cookie(
                ".amazo.site",
                1523817119,
                false,
                "_gid",
                "/",
                false,
                "GA1.2.1042362586.1523730719"
            ),
            new Cookie(
                ".amazo.site",
                1586802719,
                false,
                "_ga",
                "/",
                false,
                "GA1.2.1163186073.1523730719"
            ),
            new Cookie(
                ".zewen.club",
                1577934245,
                false,
                "foo",
                "/",
                false,
                "bar"
            ),
        ];
    }

    /**
     * @param TaskInterface $task
     * @param ProjectFlow $flow
     * @return TaskInterface
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function modify(TaskInterface $task, ProjectFlow $flow): TaskInterface
    {
        $returningVisitors = $flow->getReturningVisitors();

        $project = $flow->getProject();

        $visitor = null;
        $random = mt_rand(0, 100);
        if ($random < $returningVisitors) {
            $visitor = $this->visitorRepository->findVisitor(
                new \DateTimeImmutable("-1 day"),
                $project,
                "USA"
            );
        }

        if ($visitor === null) {
            $visitor = new Visitor();
            $visitor->setProject($project);
            $visitor->setUserAgent($task->getUserAgent());
        }

        $visitor->setLastVisitedAt(new \DateTimeImmutable("now"));

        $this->entityManager->persist($visitor);
        $this->entityManager->flush();

        return $task
            ->setVisitorId($visitor->getId())
            ->setCookies($this->getStubCookies());
    }
}