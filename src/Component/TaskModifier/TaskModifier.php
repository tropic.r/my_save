<?php

namespace App\Component\TaskModifier;

use App\Entity\ProjectFlow;
use App\Entity\TaskInterface;

interface TaskModifier
{
    /**
     * @param TaskInterface $task
     * @param ProjectFlow $flow
     * @return TaskInterface
     */
    public function modify(
        TaskInterface $task,
        ProjectFlow $flow
    ): TaskInterface;
}