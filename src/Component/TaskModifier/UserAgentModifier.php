<?php

namespace App\Component\TaskModifier;

use App\Component\UserAgentGenerator;
use App\Entity\ProjectFlow;
use App\Entity\TaskInterface;

class UserAgentModifier implements TaskModifier
{
    /**
     * @var UserAgentGenerator
     */
    private $userAgentGenerator;

    /**
     * UserAgentModifier constructor.
     * @param UserAgentGenerator $userAgentGenerator
     */
    public function __construct(UserAgentGenerator $userAgentGenerator)
    {
        $this->userAgentGenerator = $userAgentGenerator;
    }

    /**
     * @param TaskInterface $task
     * @param ProjectFlow $flow
     * @return TaskInterface
     */
    public function modify(TaskInterface $task, ProjectFlow $flow): TaskInterface
    {
        $userAgent = $this->userAgentGenerator->getRandomItem();

        return $task->setUserAgent($userAgent);
    }
}