<?php

namespace App\Component;

use App\Entity\Cookie;
use App\Entity\Proxy;
use App\Entity\Task;
use App\Entity\TaskInterface;
use App\Entity\UserAgent;

class TaskSerializer
{
    /**
     * @param TaskInterface $task
     * @return string
     */
    public function serialize(TaskInterface $task): string
    {
        $cookies = [];
        foreach ($task->getCookies() as $cookie) {
            $cookies[] = [
                "domain" => $cookie->getDomain(),
                "expires" => $cookie->getExpires(),
                "httpOnly" => $cookie->isHttpOnly(),
                "name" => $cookie->getName(),
                "path" => $cookie->getPath(),
                "secure" => $cookie->isSecure(),
                "value" => $cookie->getValue(),
            ];
        }

        return json_encode([
            "visitorId" => $task->getVisitorId(),
            "startUrl" => $task->getStartUrl(),
            "referer" => $task->getReferer(),
            "proxy" => [
                "proxy" => $task->getProxy()->getProxy(),
                "type" => $task->getProxy()->getType(),
                "auth" => $task->getProxy()->getAuth(),
            ],
            "userAgent" => [
                "id" => $task->getUserAgent()->getId(),
                "popularity" => $task->getUserAgent()->getPopularity(),
                "userAgent" => $task->getUserAgent()->getUserAgent(),
                "active" => $task->getUserAgent()->getActive(),
            ],
            "cookies" => $cookies,
        ]);
    }

    /**
     * @param string $string
     * @return TaskInterface
     */
    public function unserialize(string $string): TaskInterface
    {
        $taskArray = json_decode($string, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \InvalidArgumentException(
                sprintf("Unable to unserialize task. Error: %s", json_last_error_msg())
            );
        }

        $proxy = $taskArray["proxy"];
        $cookies = [];
        foreach ($taskArray["cookies"] as $cookie) {
            $cookies[] = new Cookie(
                (string)$cookie["domain"],
                (int)$cookie["expires"],
                (bool)$cookie["httpOnly"],
                (string)$cookie["name"],
                (string)$cookie["path"],
                (bool)$cookie["secure"],
                (string)$cookie["value"]
            );
        }

        $userAgent = $taskArray["userAgent"];
        return new Task(
            $taskArray["visitorId"],
            $taskArray["startUrl"],
            $taskArray["referer"],
            new Proxy($proxy["proxy"], $proxy["type"], $proxy["auth"]),
            new UserAgent(
                (int)$userAgent["id"],
                (string)$userAgent["userAgent"],
                (float)$userAgent["popularity"],
                (bool)$userAgent["active"]
            ),
            $cookies,
            ""
        );
    }
}