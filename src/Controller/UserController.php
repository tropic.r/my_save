<?php

namespace App\Controller;

use App\Entity\ConfirmationToken;
use App\Entity\User;
use App\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/signup", name="user_register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, \Swift_Mailer $mailer)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $confirmationToken = new ConfirmationToken();
            $token = md5(random_bytes(32));
            $confirmationToken->setToken($token);

            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $user->setConfirmationToken($confirmationToken);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($confirmationToken);
            $entityManager->persist($user);
            $entityManager->flush();

            $message = (new \Swift_Message('Hello Email'))
                ->setFrom('eug.a.petrov@gmail.com')
                ->setTo($user->getEmail())
                ->setBody(
                    $this->renderView("admin/mails/confirmation.html.twig", [
                        'confirmationToken' => $confirmationToken,
                    ]),
                    'text/html'
                );

            $mailer->send($message);

            return $this->redirectToRoute('user_registration_success');
        }

        return $this->render("admin/user/register.html.twig", [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/confirm-registration/{token}", name="user_confirm")
     * @param string $token
     * @return Response
     * @throws \Exception
     */
    public function confirmRegistration(string $token)
    {
        $result = null;
        $repo = $this->getDoctrine()->getRepository(ConfirmationToken::class);
        $confirmationToken = $repo->findOneBy([
            'token' => $token,
        ]);

        if ($confirmationToken === null) {
            $result = "invalid_token";
        } else {
            $user = $confirmationToken->getUser();
            if ($user->isConfirmed()) {
                $result = "already_confirmed";
            } else {
                $user->setConfirmedAt(new \DateTimeImmutable());
                $user->setIsConfirmed(true);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                $result = "confirmed";
            }
        }
        return $this->render("admin/user/confirmation.html.twig", [
            "result" => $result,
        ]);
    }

    /**
     * @Route("/registered", name="user_registration_success")
     */
    public function registrationSuccess()
    {
        return $this->render("admin/user/registration_success.html.twig", []);
    }
}