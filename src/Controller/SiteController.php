<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SiteController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @Route("/index.html")
     */
    public function index()
    {
        return $this->render("site/index.html.twig");
    }

    /**
     * @Route("/pricing-and-plans.html", name="pricing")
     */
    public function pricing()
    {
        return $this->render("site/pricing.html.twig");
    }

    /**
     * @Route("/free-trial.html", name="free_trial")
     */
    public function freeTrial()
    {
        return $this->render("site/free_trial.html.twig");
    }

    /**
     * @Route("/geo-targeting.html", name="geo_targeting")
     */
    public function geoTargeting()
    {
        return $this->render('site/geo_targeting.html.twig');
    }

    /**
     * @Route("/how-it-works.html", name="how_it_works")
     */
    public function howItWorks()
    {
        return $this->render('site/how_it_works.html.twig');
    }

    /**
     * @Route("/traffic.html", name="traffic")
     */
    public function traffic()
    {
        return $this->render("site/traffic.html.twig");
    }
}