<?php

namespace App\Controller\Admin;

use App\Entity\Project;
use App\Entity\User;
use App\Form\ProjectType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProjectController
 * @method User getUser()
 * @package App\Controller\Admin
 */
class ProjectController extends AbstractController
{
    /**
     * @Route("/secure/projects", name="projects_list")
     */
    public function list()
    {
        $user = $this->getUser();

        $projects = $user->getProjects();

        return $this->render("admin/project/list.html.twig", [
            "title" => "Projects",
            "breadcrumb" => [
                [
                    "title" => "Home",
                    "url" => $this->generateUrl("dashboard"),
                ],
                [
                    "title" => "Projects",
                    "path" => $this->generateUrl("projects_list"),
                ],
            ],
            "projects" => $projects,
        ]);
    }

    /**
     * @Route("/secure/project/add", name="project_add")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function add(Request $request)
    {
        $project = new Project();
        $form = $this->createForm(ProjectType::class, $project);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $project->setUser($this->getUser());
            $project->setState(Project::STATE_NOT_ACTIVE);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($project);
            $entityManager->flush();

            return $this->redirectToRoute("project_edit", [
                "id" => $project->getId(),
            ]);
        }

        return $this->render("admin/project/edit.html.twig", [
            "form" => $form->createView(),
        ]);
    }

    /**
     * @Route("/secure/project/{id}", name="project_edit")
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request, int $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $project = $entityManager->getRepository(Project::class)->findOneBy([
            "id" => $id,
            "user" => $this->getUser(),
        ]);

        if ($project === null) {
            throw $this->createNotFoundException('Project does not exists');
        }

        $form = $this->createForm(ProjectType::class, $project);

        $flows = $project->getProjectFlows();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute("project_edit", [
                "id" => $project->getId(),
            ]);
        }

        return $this->render("admin/project/edit.html.twig", [
            "form" => $form->createView(),
            "project" => $project,
            "flows" => $flows,
            "title" => $project->getName(),
            "breadcrumb" => [
                [
                    "title" => "Home",
                    "url" => $this->generateUrl("dashboard")
                ],
                [
                    "title" => "Projects",
                    "url" => $this->generateUrl("projects_list")
                ],
                [
                    "title" => $project->getName(),
                    "url" => $this->generateUrl("project_edit", [
                        "id" => $project->getId(),
                    ])
                ],
            ],
        ]);
    }
}