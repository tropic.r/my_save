<?php

namespace App\Controller\Admin;

use App\Entity\Project;
use App\Entity\ProjectFlow;
use App\Entity\TimeOnPage;
use App\Entity\TrafficDistribution;
use App\Entity\TrafficType;
use App\Form\ProjectFlowType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;

class FlowController extends AbstractController
{
    /**
     * @Route("/secure/project/{projectId}/flow/add", name="flow_add")
     * @param Request $request
     * @param int $projectId
     * @param TranslatorInterface $translator
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function add(Request $request, int $projectId, TranslatorInterface $translator)
    {
        $project = $this->getDoctrine()->getRepository(Project::class)->findOneBy([
            "id" => $projectId,
            "user" => $this->getUser(),
        ]);

        if ($project === null) {
            throw $this->createNotFoundException('Project does not exists');
        }

        $timeOnPage = new TimeOnPage();
        $trafficDistribution = new TrafficDistribution();
        $trafficType = new TrafficType();

        $projectFlow = new ProjectFlow();
        $projectFlow->setTimeOnPage($timeOnPage);
        $projectFlow->setTrafficDistribution($trafficDistribution);
        $projectFlow->setTrafficType($trafficType);

        $form = $this->createForm(ProjectFlowType::class, $projectFlow);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $projectFlow->setProject($project);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($projectFlow);
            $entityManager->flush();

            return $this->redirectToRoute("flow_edit", [
                "projectId" => $project->getId(),
                "flowId" => $projectFlow->getId(),
            ]);
        }

        return $this->render("admin/project/flow/edit.html.twig", [
            "form" => $form->createView(),
            "title" => $translator->trans("Add flow to project %project_name%", [
                "%project_name%" => $project->getName(),
            ]),
            "breadcrumb" => [
                [
                    "title" => $translator->trans("Home"),
                    "url" => $this->generateUrl("dashboard")
                ],
                [
                    "title" => $translator->trans("Projects"),
                    "url" => $this->generateUrl("projects_list")
                ],
                [
                    "title" => $project->getName(),
                    "url" => $this->generateUrl("project_edit", [
                        "id" => $project->getId(),
                    ]),
                ],
                [
                    "title" => $translator->trans("Add flow"),
                    "url" => $this->generateUrl("flow_add", [
                        "projectId" => $project->getId(),
                    ]),
                ],
            ],
        ]);
    }

    /**
     * @Route("/secure/project/{projectId}/flow/{flowId}", name="flow_edit")
     * @param Request $request
     * @param int $projectId
     * @param int $flowId
     * @param TranslatorInterface $translator
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function edit(Request $request, int $projectId, int $flowId, TranslatorInterface $translator)
    {
        $project = $this->getDoctrine()->getRepository(Project::class)->findOneBy([
            "id" => $projectId,
            "user" => $this->getUser(),
        ]);

        if ($project === null) {
            throw $this->createNotFoundException('Project does not exists');
        }

        $entityManager = $this->getDoctrine()->getManager();

        $projectFlow = $entityManager->getRepository(ProjectFlow::class)->findOneBy([
            "id" => $flowId,
            "project" => $project,
        ]);

        if ($projectFlow === null) {
            throw $this->createNotFoundException('Project flow does not exists');
        }
        $form = $this->createForm(ProjectFlowType::class, $projectFlow);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute("flow_edit", [
                "projectId" => $project->getId(),
                "flowId" => $projectFlow->getId(),
            ]);
        }

        return $this->render("admin/project/flow/edit.html.twig", [
            "form" => $form->createView(),
            "title" => $translator->trans("Add flow to project %project_name%", [
                "%project_name%" => $project->getName(),
            ]),
            "breadcrumb" => [
                [
                    "title" => $translator->trans("Home"),
                    "url" => $this->generateUrl("dashboard")
                ],
                [
                    "title" => $translator->trans("Projects"),
                    "url" => $this->generateUrl("projects_list")
                ],
                [
                    "title" => $project->getName(),
                    "url" => $this->generateUrl("project_edit", [
                        "id" => $project->getId(),
                    ]),
                ],
                [
                    "title" => $projectFlow->getName(),
                    "url" => $this->generateUrl("flow_add", [
                        "projectId" => $project->getId(),
                    ]),
                ],
            ],
        ]);
    }
}