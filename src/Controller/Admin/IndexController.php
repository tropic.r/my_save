<?php

namespace App\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class IndexController extends AbstractController
{
    /**
     * @Route("/secure/", name="dashboard")
     */
    public function dashboard()
    {
        return $this->render("admin/index/dashboard.html.twig", [
            "title" => "Dashboard",
            "breadcrumb" => [
                ["path" => "dashboard", "title" => "Home"],
            ],
        ]);
    }
}