<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

class TermsController extends AbstractController
{
    /**
     * @Route("/terms-and-conditions.html", name="terms_and_condititons")
     */
    public function termsAndConditions(TranslatorInterface $translator)
    {
        return new Response(
            $translator->trans("Page under construction")
        );
    }

    /**
     * @Route("/privacy-policy.html", name="privacy_policy")
     */
    public function privacyPolicy(TranslatorInterface $translator)
    {
        return new Response(
            $translator->trans("Page under construction")
        );
    }
    /**
     * @Route("/refund-policy.html", name="refund_policy")
     */
    public function refundPolicy(TranslatorInterface $translator)
    {
        return new Response(
            $translator->trans("Page under construction")
        );
    }
}