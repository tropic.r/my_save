<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class SubscriptionController extends AbstractController
{
    /**
     * @Route("/unsubscribe/{token}", name="unsubscribe")
     */
    public function unsubscribe(string $token)
    {
        return new Response("Page under construction");
    }
}