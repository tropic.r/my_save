function setCookies(cookies) {
    phantom.clearCookies();
    for (var i in cookies) {
        cookie = cookies[i];
        if (cookie.hasOwnProperty("expiry")) {
            cookie.expires = (new Date(cookie.expiry * 1000)).getTime();
        }
        phantom.addCookie(cookie);
    }
}

function getExttension(url) {
    var link = document.createElement("a");
    link.href = url;
    var parts = link.pathname.toLowerCase().split(".");
    if (parts.length > 1) {
        return parts.pop();
    } else {
        return "";
    }
}

var sys = require('system');

var options = JSON.parse(sys.stdin.read());
var loadStatic = options.loadStatic || false;

var page = require('webpage').create();

page.settings.loadImages = true;
page.settings.loadPlugins = false;
if (options.userAgent) {
    page.settings.userAgent = options.userAgent;
}

// page.customHeaders = {
//     'Cache-Control': 'no-cache'
// };

page.onResourceRequested = function(requestData, request) {
    if (!loadStatic) {
        var ext = getExttension(requestData.url);
        if (ext === "css" || ext === "woff" || ext === "svg" || ext === "ttf" || ext === "jpg" || ext === "png" || ext === "gif") {
            request.abort();
        }
    }
};

setCookies(options.cookies);

var headers = {};
if (options.referer) {
    headers["Referer"] = options.referer;
}

page.open(options.startUrl, { headers: headers }, function () {
    var cookies = [];
    phantom.cookies.forEach(function (v) {
        cookies.push(v);
    });

    console.log(JSON.stringify({
        cookies: cookies
    }));

    phantom.exit();
});